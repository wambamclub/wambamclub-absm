<?php
/**
 * Generic utility functions
 * May be re-used in other projects
 */

/* BEGIN: Remove the WordPress Generator Meta Tag */
function update_generator_filter() {
	return '<meta name="generator" content="Absolute Media - absolutemedia.co.uk" />';
}

if (function_exists('add_filter')) {
	$types = array('html', 'xhtml', 'atom', 'rss2', /*'rdf',*/ 'comment', 'export');
	foreach ($types as $type)
	add_filter('get_the_generator_'.$type, 'update_generator_filter');
}
/* END: Remove the WordPress Generator Meta Tag */