<?php
/**
 * Custom functions specific to this project
 */

// BEGIN: Menu Walker
// custom menu walker to integrate menu separator
// Create a menu item with the title '|' and it will be treated as a seperator. the link won't be rendered
class absolute_walker_nav_menu extends Walker_Nav_Menu {

    // add main/sub classes to li's and links
     function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes
        $classes      = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names  = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
        if($item->title == '|') {
            $class_names .= ' nav__seperator';
        }
        // build html
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        // If it is not a separator
        $item_output = '';
        if($item->title != '|') {
            $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
                $args->before,
                $attributes,
                $args->link_before,
                apply_filters( 'the_title', $item->title, $item->ID ),
                $args->link_after,
                $args->after
            );
        }

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}
// END: Menu Walker

// BEGIN: Retrieve page children
/**
 * Retrieves all the children or siblings of the $_parent_id passed from home.php
 * @param  boolean $no_tree [set to true if only the current post should be returned. If false all the siblings or children will be returned]
 */
function get_page_children_or_current($no_tree = false) {
    global $_parent_id, $post;

    if (isAjax()) {
        // If it is AJAX, just render the page
        $no_tree = true;
    }

    $_parent_id = isset($_parent_id) ? $_parent_id : $post->ID;

    $my_wp_query    = new WP_Query();
    $pages = array();

    if($no_tree) {
        $id = $_parent_id;
    } else {
        // Find the parents of the current page, if any
        $parents = get_post_ancestors( $_parent_id );
        $id = ($parents) ? $parents[0]: $_parent_id;
        // Get all the pages under the current parent page
        $pages   = $my_wp_query->query(array('post_type' => 'page', 'post_parent' => $id, 'orderby' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => -1));
    }

    $pages = ($pages) ? $pages : get_pages(array('include' => array($_parent_id), 'sort_order' => 'ASC', 'sort_column' => 'menu_order') ) ;
    return $pages;
}
// END: Retrieve page children

function numberToImage($n = "00") {
    $_str = str_split($n);
    $_imgs = '';
    for($i = 0; $i < count($_str); $i++ ) {
        $_imgs .= '<img src="' . get_template_directory_uri() . '/images/assets/show/events/dates/' . $_str[$i] . '.png" alt="'. $_str[$i] .'" />';

    }
    return $_imgs;
}



class AbsoluteSocialWall {

    private static $_twitter;
    private static $_instagram;

    private static $_next_tweet = 0;
    private static $_next_photo = 0;

    /**
     * Intialise and fetch photos from instagram and tweets from twitter. id is used in the id for the html div tag.
     * @param string  $_inst_user [description]
     * @param string  $_id        [description]
     * @param integer $_count     [description]
     */
    public function __construct( $_inst_user = "absoluteagency", $_id = "100", $_count = 12 ) {
        // Set parameters for instagram
        $atts = array(
                        "id"         => $_id,
                        "user"       => $_inst_user,
                        "src"        => "user_recent",
                        "imgl"       => "none",
                        "style"      => "gallery",
                        "col"        => $_count,
                        "size"       => "L",
                        "grwidth"    => "800",
                        "grheight"   => "800",
                        "num"        => $_count,
                        "align"      => "center",
                        "max"        => "100",
                        "shuffle"    => "1",
                        "nocredit"   => "1"
                    );

        self::$_instagram   = self::get_pictures_from_instagram_using_alpine_photo_tile_for_instagram_plugin($atts);
        self::$_twitter     = self::get_tweets_using_display_tweets_plugin();
            }

    /**
     * Refer: \wp-content\plugins\alpine-photo-tile-for-instagram\gears\plugin-shortcode.php
     * This has to be called from within a template file, as the class is not initilised when loading this custom.php file
     * Example original implementation by the plugin developers: [alpine-phototile-for-instagram id=480 user="wambamclub" src="user_recent" imgl="none" style="gallery" row="12" grwidth="800" grheight="800" size="L" num="12" align="center" max="100"]
     * Example actual implementation: get_pictures_from_instagram_using_alpine_photo_tile_for_instagram_plugin( $atts )
     * @param  [array] $atts [array of (shortcode) options ]
     * @return [array]       [array of photos pulled from instagram]
     */
    private static function get_pictures_from_instagram_using_alpine_photo_tile_for_instagram_plugin( $atts ) {

        $_photos = array();

        if ( class_exists( 'PhotoTileForInstagramBot' ) ) {

            $bot = new PhotoTileForInstagramBot();

            // ID is used to retreive from transient.
            // Ideally, a unique id is assigned to each shortcode
            $array_string = implode ( $atts );
            $id = strlen( $array_string );
            if( !empty($atts['id']) ) {
                $id = $id."_".$atts['id'];
            }

            if( isset( $atts['testmode'] ) && isset( $atts['id'] ) ) {
                // Load shortcode in test mode (i.e. echo messages )
                $bot->set_private('testmode',$atts['testmode']);
                $bot->set_private('cacheid',$atts['id']);
                $id = $id."_".$atts['testmode'];
            } else {
                $bot->set_private('cacheid',$id);
            }

            $bot->echo_point("Start plugin.");

            $optiondetails = $bot->option_defaults();
            $options = array();

            foreach( $optiondetails as $opt=>$details ) {
                $options[$opt] = $details['default'];
                if( isset($details['short']) && isset($atts[ $details['short'] ]) ){
                    $options[$opt] = $atts[ $details['short'] ];
                }
            }

            $bot->set_private('wid','id-'.$id);
            $bot->set_private('options',$options);
            $bot->do_alpine_method( 'update_global_options' );
            $bot->echo_point("Enqueue styles and scripts");
            $bot->do_alpine_method( 'enqueue_style_and_script' );
            // Do the photo search
            $bot->echo_point("Retrieve Photos");
            $bot->do_alpine_method( 'photo_retrieval' );

            $bot->echo_point("Check Results");
            if( $bot->check_active_result('success') ) {
                $_photos_obj = $bot->get_private('results');
                if(array_key_exists('photos', $_photos_obj) && count($_photos_obj['photos'])) {
                    $_photos = $_photos_obj['photos'];
                }
            }
        }
        return $_photos;
    }

    /**
     * Use the Display Tweets plugin to get the tweets
     * @return [array] [Array of tweets as per the configuration options]
     */
    private static function get_tweets_using_display_tweets_plugin() {
        $_tweets = array();
        if ( class_exists( 'DisplayTweets' ) ) {
            $displaytweets = DisplayTweets::get_instance();
            $_tweets = $displaytweets->get();
        }
        // format the tweet text and add links etc
        foreach ($_tweets as $key => $_tweet) {
            $_tweet_time           = self::time_ago($_tweet->created_at, 1);
            $_tweet_link           = "https://twitter.com/". $_tweet->user->screen_name ."/statuses/" . $_tweet->id_str;
            $_link                 = '<a href="'. $_tweet_link .'" target="_blank">'. $_tweet_time .'</a>';
            //$_link               = $_tweet_time;

            $_html                 = '<p>' . $displaytweets->format_tweet($_tweet->text) . '</p>';
            $_html               .= '<p><small>' . $_link . '</small></p>';
            $_tweets[$key]->html  = $_html;
        }
        return $_tweets;
    }

    public function show_next_tweet() {
        $_ret_html = '';
        if(count(self::$_twitter) > self::$_next_tweet) {
            $_tweet    = self::$_twitter[self::$_next_tweet];
            $_ret_html = $_tweet->html;
            self::$_next_tweet++;
        }

        $_ret_html =   '<div class="social__block twitter">
                            <div class="social__block-inner">
                                <div class="social__block-content">'
                                . $_ret_html .
                                '</div>
                            </div>
                        </div>';

        return $_ret_html;
    }

    public function show_next_photo() {
        $_ret_html = '';
        if(count(self::$_instagram) > self::$_next_photo) {
            $_photo = self::$_instagram[self::$_next_photo];
            if( array_key_exists('image_original', $_photo) ) {
                $_image     = '<img src="'. $_photo['image_original'] .'" alt="' . ( array_key_exists('image_title', $_photo) ? $_photo['image_title'] : '' ) . '">';
                $_link      = '<a href="'. $_photo['image_link'] .'" target="_blank">'. $_image .'</a>';
                $_ret_html .= $_link;
                //$_ret_html .= $_image;
            }
            self::$_next_photo++;
        }
        $_ret_html =   '<div class="social__block instagram">
                            <div class="social__block-inner">
                                <div class="social__block-content">'
                                . $_ret_html .
                                '<div class="instagram__overlay"></div>
                            </div>
                        </div>
                    </div>';

        return $_ret_html;
    }

    // DISPLAYS COMMENT POST TIME AS "1 year, 1 week ago" or "5 minutes, 7 seconds ago", etc...
    private function time_ago($date, $granularity=2) {
        $date = strtotime($date);

        $difference = time() - $date;
        $periods = array(
                'decade'    => 315360000,
                'year'      => 31536000,
                'month'     => 2628000,
                'week'      => 604800,
                'day'       => 86400,
                'hour'      => 3600,
                'minute'    => 60,
                'second'    => 1
            );
        $retval = "";

        if ($difference < 5) { // less than 5 seconds ago, let's say "just now"
            $retval = "Posted just now";
        } else {
            foreach ($periods as $key => $value) {
                if ($difference >= $value) {
                    $time = floor($difference/$value);
                    $difference %= $value;
                    $retval .= ($retval ? ' ' : '').$time.' ';
                    $retval .= (($time > 1) ? $key.'s' : $key);
                    $granularity--;
                }
                if ($granularity == '0') { break; }
            }
            $retval = 'Posted '.$retval.' ago';
        }

        return $retval;
    }

}

// Shortcode to embed Aweber Newsletter. COpied from old website
function aweber_newsletter_form_func(){

    $_html = '';
    $_html .= '<form method="post" action="http://www.aweber.com/scripts/addlead.pl" id="form" target="_blank">';
    $_html .= '<label for="name">';
        $_html .= '<span class="form-label">Name</span>';
        $_html .= '<input type="text" maxlength="60" name="name" id="name" class="textbox" value="" placeholder="Name" />';
    $_html .= '</label>';
    $_html .= '<label for="from">';
        $_html .= '<span class="form-label">Name</span>';
        $_html .= '<input type="text" maxlength="64" name="from" id="from" class="textbox" value="" placeholder="Email" />';
    $_html .= '</label>';
    $_html .= '<input type="hidden" name="meta_split_id" value="" />';
    $_html .= '<input type="hidden" name="unit" value="wambamclub" />';
    $_html .= '<input type="hidden" name="redirect" value="http://www.wambamclub.com/please-confirm.html?cid=" />';
    $_html .= '<input type="hidden" name="meta_redirect_onlist" value="http://www.wambamclub.com/already-subscribed.html" />';
    $_html .= '<input type="hidden" name="meta_adtracking" value="" />';
    $_html .= '<input type="hidden" name="meta_message" value="1" />';
    $_html .= '<input type="hidden" name="meta_required" value="from" />';
    $_html .= '<input type="hidden" name="meta_forward_vars" value="1" />';
    $_html .= '<div class="form__submit">';
        $_html .= '<input type="submit" class="submit cta-button" name="submit" value="Submit" />';
    $_html .= '</div>';
    $_html .= '</form>';

    return $_html;

}
add_shortcode( 'aweber-newsletter', 'aweber_newsletter_form_func' );


function get_tab_links($append_before="", $_preload = true) {
    global $_parent_id;

    if (!isAjax()) {
        if(isset($_parent_id) && $_parent_id > 0 ) : ?>
            <div class="tab__buttons">
                <div class="tab__buttons-wrap">
                    <?php
                        $_children = get_pages(array( 'child_of' => $_parent_id, 'sort_column' => 'menu_order', 'sort_order' => 'asc' ) );

                        foreach( $_children as $_idx => $page ) {
                        ?><div class="tab__button <?php echo ($_idx==0 || $_preload) ? 'loaded' : ''?>" id="tab__button--<?php echo $append_before . $page->post_name ?>" data-postid=<?php echo $page->ID ?> >
                                <div class="tab__button-inner">
                                    <?php
                                        $_subtitle =  get_post_meta( $page->ID, 'wpcf-sub-title', true );
                                        echo ($_subtitle > '') ? '<small>' . $_subtitle . '</small>' : '';
                                    ?>
                                    <a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a>
                                </div>
                            </div><?php }
                    ?>
                </div>
                <div class="tab__overflow-indicator"></div>
            </div>
        <?php endif;
    }
}


function get_event_details($_start, $_end, $_embed_height, $_evnturl, $_dt_id, $_content, $_hidelocation = false, $_pdfmenu = null, $_hidepdfmenu = false) {
    ?>
    <?php $_html = ''; ?>
    <?php $_html .= '<section id="dates__' . $_dt_id .'"  title="' . date('F', $_start) . ' ' . date('d', $_start) . '">'; ?>
        <?php $_html .= '<img src="' . get_template_directory_uri() . '/images/assets/show/events/dates/dates-bg.jpg" alt="" />'; ?>
        <?php $_html .= '<div class="section__content">'; ?>
            <?php $_html .= '<div class="section__content-inner">';?>
                <?php $_html .= '<p class="dates__month">' . date('F', $_start) . '</p>'; ?>
                <?php $_html .= '<p class="dates__day">' . date('l', $_start) . '</p>'; ?>
                <?php $_html .= '<p class="dates__date">' . numberToImage(date('d', $_start)) . '</p>'; ?>
                <?php
                    // Fix the issue with p tags on the content. Sometimes there are p tags and some times there is no p tags
                    // Use regular expression to find the strong tag
                    // Remove all the p tags and then wrap the strong with a p tag
                    // Then wrap p tag to the rest of the content
                    // 29.12: Fixed so it doesn't nuke the header tags as well.
                    $regex = '/<strong>(.*)<\/strong>/si';
                    preg_match_all($regex, $_content, $matched_data, PREG_SET_ORDER);
                    $_content = strip_tags($_content, '<strong><a><h1><h2><h3>');
                    $_content = str_replace($matched_data[0][0], '<p>' . $matched_data[0][0] . '</p><p>', $_content);
                    $_content .= '</p>';
                    $_html .=  $_content;
                ?>
                <?php
                    $_pos = strpos($_evnturl, "-tickets-");
                    if ($_pos !== false) {
                        $_eventid   = substr($_evnturl, $_pos + 9);
                        /* Add back class "book-event" to trigger EventBrite AJAX popup on right */
                        $_html .= '<p><a href="'. $_evnturl .'" data-eventid="'. $_eventid .'" data-height="'. $_embed_height .'" class="cta-button">Buy Tickets</a></p>';
                    } else {
                        /* For alternative non-Eventbrite venues */
                        $_html .= '<p><a href="'. $_evnturl .'" data-height="'. $_embed_height .'" class="cta-button">Buy Tickets</a></p>';
                    }
                ?>
                <?php $_html .= '<ul class="iconlist">'; ?>
                    <?php if(!$_hidepdfmenu) : ?>
                        <?php $_pdfmenu = $_pdfmenu > '' ? $_pdfmenu :  "wp-content/uploads/2014/06/wam-bam-spring-summer-menu-20141.pdf"; ?>
                        <?php $_html .= '<li class="menu"><a href="' . $_pdfmenu . '" target="_blank">Menu</a></li>'; ?>
                    <?php endif; ?>
                    <?php if(!$_hidelocation) : ?>
                        <?php $_html .= '<li class="location"><a href="#location">Location</a></li>'; ?>
                    <?php endif; ?>
                <?php $_html .= '</ul>'; ?>
            <?php $_html .= '</div>'; ?>
        <?php $_html .= '</div>'; ?>
    <?php $_html .= '</section>'; ?>
    <?php
    return $_html;
}

function get_act_details($child_posts, $_pid = 0) {
    ?>
        <?php
        $_html = '';
        $_html .= '<div class="actgroup actgroup__' . $_pid .'">';
        foreach ($child_posts as $_oneact) : ?>
            <?php
            $act_id     = wpcf_pr_post_get_belongs($_oneact->ID, 'acts');
            $act        = get_post($act_id);
            $terms      = get_the_terms( $act_id,  'types');
            ?>
            <?php $_html .= '<article class="act__' . $_pid .' actid__'. $act_id .'">'; ?>
            <?php
                    $_responsive_shortcode = '[responsive imageid="'. get_post_thumbnail_id( $act_id ) .'" postid="'. $act_id .'"]';
                    $_html .= do_shortcode($_responsive_shortcode);
                ?>
                <?php $_html .= '<div class="section__overlay"></div>'; ?>
                <?php $_html .= '<div class="section__content">'; ?>
                    <?php $_html .= '<a href="" class="act__toggle"></a>'; ?>
                    <?php $_html .= '<div class="section__content-inner">'; ?>
                        <?php $_html .= '<h3>' . $act->post_title . '</h3>'; ?>
                        <?php $_html .= do_shortcode($act->post_content); ?>
                        <?php
                            $_trms = array();
                            if ( $terms && ! is_wp_error( $terms ) ) {
                                foreach ($terms as $key => $term) {
                                    $_trms[] = $term->name;
                                }
                            }
                        ?>
                        <?php $_html .= '<h4>' . implode(", ", $_trms) . '</h4>'; ?>
                    <?php $_html .= '</div>'; ?>
                <?php $_html .= '</div>'; ?>
            <?php $_html .= '</article>' ?>
        <?php endforeach; ?>
        <?php $_html .= '</div>'; ?>
<?php
    return $_html;
}


// Add the ajax scripts
function absolute_custom_scripts() {
    // embed the javascript file that makes the AJAX request
    wp_enqueue_style( 'absolute-custom-styles', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css', false, '20140624', $media = 'all' );
    wp_enqueue_script( 'absolute-custom-scripts', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.pack.js', array( 'jquery' ), '20140624', true );
}
add_action( 'wp_enqueue_scripts', 'absolute_custom_scripts' );

