<?php
/**
 * Script to support the Ajax functionality
 * May be re-used in other projects
 */

// Add the ajax scripts
function absolute_ajax_scripts() {
	// embed the javascript file that makes the AJAX request
	wp_enqueue_script( 'absolute-ajax-request', get_template_directory_uri() . '/js/custom/ajax.js', array( 'jquery' ), '20140623', true );

	// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
	wp_localize_script( 'absolute-ajax-request', 'AbsoluteAjax',
						array(
								// URL to wp-admin/admin-ajax.php to process the request
								'ajax_url' 		=> admin_url( 'admin-ajax.php' ),

								// generate a nonce with a unique ID "myajax-post-comment-nonce"
								// so that you can check it later when an AJAX request is sent
								'ajax_nonce' 	=> wp_create_nonce( 'absolute-ajax-nonce' ),
							)
	);
}
add_action( 'wp_enqueue_scripts', 'absolute_ajax_scripts' );


// Setup actions for ajax requests
add_action( 'wp_ajax_generic_action', 'generic_action_callback' );
add_action( 'wp_ajax_nopriv_generic_action', 'generic_action_callback' );

function generic_action_callback() {
	//global $wpdb; // this is how you get access to the database
	// $whatever = intval( $_POST['whatever'] );
	// $whatever += 10;
	// echo $whatever;

	$_nonce = $_POST['ajax_nonce'];

    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    // if ( ! wp_verify_nonce( $_nonce, 'absolute-ajax-nonce' ) ) {
    //     die ( 'Un-authorised call!');
    // }

	echo 'Returned from Ajax call';

	die(); // this is required to return a proper result
}


// Setup actions for ajax requests - for "info" tabs
add_action( 'wp_ajax_infotabs_action', 'infotabs_action_callback' );
add_action( 'wp_ajax_nopriv_infotabs_action', 'infotabs_action_callback' );

function infotabs_action_callback() {
	global $post, $_parent_id;

	//global $wpdb; // this is how you get access to the database
	// $whatever = intval( $_POST['whatever'] );
	// $whatever += 10;
	// echo $whatever;

	$_nonce = $_POST['ajax_nonce'];

    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    // if ( ! wp_verify_nonce( $_nonce, 'absolute-ajax-nonce' ) ) {
    //     die ( 'Un-authorised call!');
    // }

	$_parent_id = isset($_POST['postid']) ? $_POST['postid'] : 0;

	if(!$_parent_id > 0) {
        die ( 'Invalid id' );
	}

	$post 		= get_post($_parent_id);
	include(locate_template('page-templates/partials/content-info.php'));
	die(); // this is required to return a proper result

}


/**
 *  Check if it's an AJAX call
 */
function isAjax() {
	return defined('DOING_AJAX') && DOING_AJAX;
}


// Setup actions for ajax requests - for "events" tabs
add_action( 'wp_ajax_eventtabs_action', 'eventtabs_action_callback' );
add_action( 'wp_ajax_nopriv_eventtabs_action', 'eventtabs_action_callback' );

function eventtabs_action_callback() {
	$_nonce = $_POST['ajax_nonce'];

    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    // if ( ! wp_verify_nonce( $_nonce, 'absolute-ajax-nonce' ) ) {
    //     die ( 'Un-authorised call!');
    // }

	$_parent_id = isset($_POST['postid']) ? $_POST['postid'] : 0;
	$_post_type = isset($_POST['type'])   ? $_POST['type'] : '';

	if(!$_parent_id > 0) {
        die ( 'Invalid id' );
	}

	$post 			= get_post($_parent_id);

	$_start 		= get_post_meta( $_parent_id, 'wpcf-start-date', true );
	$_end 			= get_post_meta( $_parent_id, 'wpcf-end-date', true );
	$_embed_height 	= get_post_meta( $_parent_id, 'wpcf-embed-height', true );
	$_evnturl 		= get_post_meta( $_parent_id, 'wpcf-url-to-the-event', true );
	$_dt_id 		= date('d', $_start) . '-' . date('m', $_start);

	$_content = $post->post_content;
	$_content = apply_filters('the_content', $_content);
	$_content = str_replace(']]>', ']]&gt;', $_content);


	$_hidelocation 	= get_post_meta( $_parent_id, 'wpcf-hide-location-link', true );
	$_hidepdfmenu 	= get_post_meta( $_parent_id, 'wpcf-hide-menu-link', true );
	$_pdfmenu 	 	= get_post_meta( $_parent_id, 'wpcf-menu-for-this-show', true );

	$_event = get_event_details($_start, $_end, $_embed_height, $_evnturl, $_dt_id, $_content, $_hidelocation, $_pdfmenu, $_hidepdfmenu);

	$child_posts 	= null;
    $child_posts = types_child_posts('show-act', array('post_id' => $_parent_id));
	$_act =get_act_details($child_posts, $_parent_id);

	$_ret = array('event_details' => $_event, 'act_details' => $_act);
	echo json_encode($_ret);

	die(); // this is required to return a proper result

}

// Setup actions for ajax requests - to load social wall on page load using Ajax request. The number of items loaded will be depending on the device
add_action( 'wp_ajax_socialwall_action', 'socialwall_action_callback' );
add_action( 'wp_ajax_nopriv_socialwall_action', 'socialwall_action_callback' );

function socialwall_action_callback() {
	$_nonce = $_POST['ajax_nonce'];

    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    // if ( ! wp_verify_nonce( $_nonce, 'absolute-ajax-nonce' ) ) {
    //     die ( 'Un-authorised call!');
    // }

// The count will be retrieved in the socialwall template
//	$_count = isset($_POST['count']) ? $_POST['count'] : 12;

	get_template_part( 'page-templates/partials/content', 'socialwall' );

	die(); // this is required to return a proper result

}


// Setup actions for ajax requests - for "event brite - book ticket" popup
add_action( 'wp_ajax_eventbrite_action', 'eventbrite_action_callback' );
add_action( 'wp_ajax_nopriv_eventbrite_action', 'eventbrite_action_callback' );

function eventbrite_action_callback() {
  /*
  29.12: Attempting to get the tickethub widget to work (unsuccessfully so far)
	$_eventid  = isset($_POST['eventid']) ? $_POST['eventid'] : 0;
	$_eventurl = isset($_POST['eventurl']) ? $_POST['eventurl'] : '';

	if(!$_eventid > 0) {
        die ( 'Invalid event id' );
	}

	$_html = "<div class='booknow-popup booknow-popup-" . $_eventid . " active'>";  
    $_html .= '<script src="https://widget.tickethub.io/external.js"';
    $_html .= 'data-widget="924429e1-b78f-4e9e-9ef0-c008daaf93f8"';
    $_html .= 'data-layout="embed">';
    $_html .= '</script>';
    $_html .= '</div>';

    print $_html;
  die();
  */
  
	$_nonce = $_POST['ajax_nonce'];
    // check to see if the submitted nonce matches with the
    // generated nonce we created earlier
    // if ( ! wp_verify_nonce( $_nonce, 'absolute-ajax-nonce' ) ) {
    //     die ( 'Un-authorised call!');
    // }

	$_eventid  = isset($_POST['eventid']) ? $_POST['eventid'] : 0;
	$_eventurl = isset($_POST['eventurl']) ? $_POST['eventurl'] : '';

	if(!$_eventid > 0) {
        die ( 'Invalid event id' );
	}
	$_OAuth_token = '3IEI6SFF3USYHVKLF7OD';

    $eventbrite_url = "https://www.eventbriteapi.com/v3/events/". $_eventid ."/?token=" . $_OAuth_token;

	//$eventbrite_url = "https://www.eventbriteapi.com/v3/events/10817644867/?token=" . $_OAuth_token;  // Test URL

    //$events = json_decode(file_get_contents($eventBriteURL));
    $events = file_get_contents($eventbrite_url);

	/* Refer: http://stackoverflow.com/questions/16760617/use-eventbrite-api-to-create-ticket-order-on-3rd-party-website */

	$_html = "<div class='booknow-popup booknow-popup-" . $_eventid . " active'>";
    $_html.= '<form action="http://www.eventbrite.co.uk/orderstart?ebtv=C" method="post" target="_blank">';
    $_html.= '	<input type="hidden" name="eid" value="'. $_eventid .'">';
    $_html.= '	<input type="hidden" name="has_javascript" value="1">';

    $_html .= "<table>
				<thead>
					<tr>
						<th class=\"type\">Ticket Type</th>
						<th class=\"price\">Price</th>
						<th class=\"quantity\">Quantity</th>
					</tr>
				</thead>
				<tbody>";

    $events_array = json_decode($events);
    foreach ($events_array->ticket_classes as $key => $ticket) {
    	if(!$ticket->hidden) {
			$_html .= "<tr>";
			$_html .= "<td>". $ticket->name . "<br />" . "<span>" . $ticket->description . "</span> <a href=\"\" class=\"more-info\"></a>" ."</td>";
			$_html .= "<td>". $ticket->cost->display . "<br />" . "<span>Fee: ". $ticket->fee->display ."</span>" ."</td>";
			$_html .= "<td>". '<input type="text" name="quant_'. $ticket->id .'_None" value="" placeholder="0">' ."</td>";
			$_html .= "</tr>";
    	}
    }
    $_html .= "  </tbody>
			  </table>";

	$_html .= '  <div class="eventbrite__options">';
    if($_eventurl > '') {
    	$_html .= '    <ul class="iconlist"><li class="noicon"><a href="'.$_eventurl.'?discount=yes" target="_blank">Enter promo code</a></li></ul>';
    }
    $_html .= '    <img src="/wp-content/themes/absolute/images/icons/eventbrite.png" alt="Eventbrite tickets" />';
    $_html .= '  </div>';
    $_html .= '  <input type="hidden" name="discount" value="" />';
    $_html .= '  <input type="submit" class="submit cta-button" value="Order now" />';
    $_html .= '  </form></div>';

    echo $_html;

//	echo $events;

	die(); // this is required to return a proper result

}