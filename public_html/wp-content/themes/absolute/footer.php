<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
		</main>

		<footer role="contentinfo">
			<div id="footer__inner">
				<div id="footer__logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri() ?>/images/assets/wam-bam_logo-small.png" alt="Wam Bam Logo" />
					</a>
				</div>
				<div id="footer__links">
					<a href="http://www.tripadvisor.co.uk/Attraction_Review-g186338-d3786705-Reviews-Wam_Bam_Club-London_England.html" target="_blank">
						<img src="<?php echo get_template_directory_uri() ?>/images/assets/trip-advisor-logo.png" alt="Trip Advisor" class="trip-advisor" />
					</a>
					<?php if ( has_nav_menu( 'secondary' ) ) : ?>
						<?php wp_nav_menu( array(
													'theme_location' => 'secondary',
													'container' => false 
												)
						); ?>
					<?php endif; ?>
					<p>&copy; 2006-<?php echo date('Y') ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved.</p>
				</div>
				<div id="footer__credit">
					<p>Web Design Bolton by <a href="<?php echo esc_url( __( 'http://absolutemedia.co.uk', 'absolute' ) ); ?>" target="_blank">Absolute</a></p>
				</div>
			</div>
		</footer>

	<?php wp_footer(); ?>
</body>
</html>