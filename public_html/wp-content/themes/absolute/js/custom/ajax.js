(function($) {
	$(document).ready(function() {

		show_eventbrite_popup();

		setup_ajax_call('#info', '.tab__buttons .tab__button:not(.loaded)', 'infotabs_action', '#info section', 'info_section');
		setup_ajax_call('#event__dates', '.tab__buttons .tab__button:not(.loaded)', 'eventtabs_action', '#event__dates section', 'event_section');
		setup_ajax_call('body main', 'a.book-event:not(.loaded)', 'eventbrite_action', '#event__bookticket .section__content-inner', 'eventbrite_section');

		call_ajax_onload('socialwall_action', '#social .section__content .section__content-inner');

	});

	function show_eventbrite_popup() {

		$('body main').on( "click", "a.book-event.loaded" , function( event ) {
		    var eventid 	= $(this).data('eventid');
			var event_el  	= '#event__bookticket .section__content-inner .booknow-popup' ;
			$(event_el).removeClass('active');
			$(event_el + "-" + eventid).addClass('active');

		    event.preventDefault();
		});

	}

	function setup_ajax_call(aj_wrapper, aj_element, aj_action, aj_addafter, ajax_section) {
		if(ajax_section == 'eventbrite_section') {
			setup_ajax_loader('#bookticket__content');
		} else {
			setup_ajax_loader(aj_wrapper);
			if(ajax_section == 'event_section') {
				setup_ajax_loader('#event__acts');
			}
		}

		// variable to hold request
		var request;

		$(aj_wrapper).on( "click", aj_element, function( event ) {

			if(ajax_section != 'eventbrite_section') {
				$('.ajaxloader', $(aj_wrapper)).addClass('loading');
				if(ajax_section == 'event_section') {
					$('.ajaxloader', $('#event__acts')).addClass('loading');
				}
			} else {
				$('.ajaxloader', $('#bookticket__content')).addClass('loading');
			}

		    // abort any pending request
		    if (request) {
		        request.abort();
		    }
		    // setup some local variables
		    var $element = $(this);
			if(ajax_section == 'eventbrite_section') {
				var data = {
					'action'	: aj_action,  // Ajax action
					'ajax_nonce': AbsoluteAjax.ajax_nonce,
					'eventid'	: $element.data('eventid'),
					'eventurl'	: $element.attr('href')
				};
			} else {
				var data = {
					'action'	: aj_action,  // Ajax action
					'ajax_nonce': AbsoluteAjax.ajax_nonce,
					'postid'	: $element.data('postid'),
					'type'		: $element.data('type')
				};
			}

		    // fire off the request
		    request = $.ajax({
		        url: AbsoluteAjax.ajax_url,
		        type: "post",
		        cache: false,
		        data: data
		    });

		    // callback handler that will be called on success
		    request.done(function (response, textStatus, jqXHR){

		    	var fnSetState = function(w, e) {
					$('.active-tab', w).removeClass('active-tab');
					e.last().addClass('active-tab');
					/*
					$('.active-tab', w).fadeOut('fast', function() {
						$(this).removeClass('active-tab');
						e.last().hide().fadeIn('slow', function() {
							$(this).addClass('active-tab');
						});
					});
					*/
					$('.ajaxloader', w).removeClass('loading');
					if(ajax_section == 'event_section') {
						$('.ajaxloader', $('#event__acts')).removeClass('loading');
					}
		    	};

				switch (ajax_section) {
					case 'info_section':

						var fnSetInfoHTML = function (el) {
						    var promise = el.after(response);
						    return promise.promise();
						};
						/**
						 *  When adding the ajax response to the target, there may be a delay.
						 *  Using "promise", wait for the reponse to load, then apply the Fancybox to the gallery images
						 *  Then enable the right content section
						 *  Remove the loader
						 */
						$.when( fnSetInfoHTML($(aj_addafter).last()))
							.done(function() {
								if($element.attr('id') == 'tab__button--info__gallery') {
									$(".ngg-gallery-thumbnail .ngg-fancybox", $(aj_wrapper)).fancybox( {
										'overlayColor': '#000',
										'overlayOpacity': 0.6,
										'transitionIn': 'elastic',
										'transitionOut': 'elastic',
										'centerOnScroll': true,
										'titlePosition': 'over',
										'easingIn': 'easeOutBack',
										'easingOut': 'easeInBack'
									});
								}
								fnSetState($(aj_wrapper), $(aj_addafter));
						});
						break;
					case 'event_section':
						/* If it already loaded through ajax, the click event will be handled in global.js */
						var IS_JSON = true;
						try {
						    var json = $.parseJSON(response);

							var fnSetEventHTML = function (el, resp) {
							    var promise = el.after(resp);
							    return promise.promise();
							};

							$.when(
									fnSetEventHTML( $(aj_addafter).last(), json.event_details ),
									fnSetEventHTML( $('#event__acts .actgroup').last(), json.act_details ),
									$('#event__acts .actgroup').endNerveSlider(),
									$('#event__acts .actgroup:not(.actgroup__' +  $element.data('postid') +')').hide()
								)
							.done(function() {
								if (typeof(init_slideshow) === "function") {
									$.when(
											init_slideshow('#event__acts .actgroup__' +  $element.data('postid'), true, true)
										)
										.done(function() {
											fnSetState($(aj_wrapper), $(aj_addafter));
									});
								}
							});
						}
						catch(err) {
						    IS_JSON = false;
						}

						break;
					case 'eventbrite_section':
						    //console.log(response);
							wrapper  = '#event__bookticket .section__content-inner';
						    $(wrapper).append(response);
							$('.ajaxloader', $('#bookticket__content')).removeClass('loading');
						break;
					//default:
				}


				window.picturefill();
				$element.addClass('loaded');
		    });

		    // callback handler that will be called on failure
		    request.fail(function (jqXHR, textStatus, errorThrown) {
		        // log the error to the console
		        console.error(
		            "The following error occured: "+
		            textStatus, errorThrown
		        );
		    });

		    event.preventDefault();
		});

	}

	function call_ajax_onload(aj_action, aj_addafter) {
		// variable to hold request
		var request;

	    // abort any pending request
	    if (request) {
	        request.abort();
	    }

	    // setup some local variables
		var vpwidth = $( window ).width();

		item_count = 20;
		if(vpwidth <= 768) {
			// Mobile 				<= 384
			// Tablet portrait  	<= 768
			item_count = 4;
		} else if(vpwidth < 1600) {
			// Tablet landscape 	<= 1024
			// Desktop 		   		< 1600
			item_count = 5;
		} else {
			// Desktop with width >= 1600
			item_count = 20;
		}

		var data = {
			'action'	: aj_action,  // Ajax action
			'ajax_nonce': AbsoluteAjax.ajax_nonce,
			'count'		: item_count
		};

	    // fire off the request
	    request = $.ajax({
	        url: AbsoluteAjax.ajax_url,
	        type: "post",
	        data: data
	    });

	    // callback handler that will be called on success
	    request.done(function (response, textStatus, jqXHR){
			$(aj_addafter).last().after(response);
			$('#social .social__grid').addClass('blocks--' + item_count.toString());
			//console.log(response);
	    });

	    // callback handler that will be called on failure
	    request.fail(function (jqXHR, textStatus, errorThrown) {
	        // log the error to the console
	        console.error(
	            "The following error occured: " +
	            textStatus, errorThrown
	        );
	    });

	}

	function setup_ajax_loader(el) {
		$(el).prepend('<div class="ajaxloader"></div>');
	}

})(jQuery);
