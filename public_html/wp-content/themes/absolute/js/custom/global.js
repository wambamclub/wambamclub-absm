(function($) {
	$(window).load(function() {
		if ($('section > section').length > 0) {
			tabbed_content(false);
		}
	});
	$(document).ready(function() {

		$('#overlay__close').click(function(e) {
			e.preventDefault();
			close_overlay();
		});

		nav_mount();
		scroll_to_anchor();
		booknow_popup();

		toggle_content('.faq h3', '.faq', 'active');
		toggle_content('.act__toggle', '.actgroup article', 'open');
		toggle_content('.booknow-popup .more-info', '.booknow-popup tbody tr', 'open', true, 'span')
		toggler('#nav__toggle', 'header, #header__nav, #nav__toggle');

		init_slideshow('#banner', true);
		init_slideshow('.testimonial', true, false, 7500);
		init_slideshow('#event__acts .actgroup', true, true);

		$('.main-menu-link').click(function(e) {
			if ($(window).outerWidth() <= 960) {
				$('header, #header__nav, #nav__toggle').removeClass('open');

			}
		});

		$('body').on('click', '.tab__overflow-indicator', function(e) {
			e.preventDefault();
			var tabs;

			if ($(this).hasClass('leftscroll')) {
				tabs = $(this).next();
				scrolldirection = '-='; // left
			} else {
				tabs = $(this).prev();
				scrolldirection = '+='; // right
			}

			var tabs_width = $(tabs).outerWidth();
			var scrolldirection;
			$(tabs).animate({
				scrollLeft: scrolldirection + tabs_width
			}, 500);
		});

		aweber_validation();

	});

	$(document).ajaxStop(function() {
		square_blocks('.social__block');
	});

	$(window).load(function() {
		// centre_content('.section__content');
	});

	window.init_slideshow = function init_slideshow(container, autoplay, arrows, delay) {
		if (delay === undefined) {
			delay = 5000;
		}
		if ($(container).length > 0) {
			$(container).nerveSlider({
				allowKeyboardEvents: false,
				showArrows: arrows,
				showDots: true,
				showPause: false,
				showTimer: false,
				timerStartWidth: '0%',
				timerEndWidth: '0%',
				slideTransition: 'slide',
				slideTransitionDelay: delay,
				slideTransitionSpeed: 500,
				sliderAutoPlay: autoplay,
				sliderWidth: '100%',
				sliderHeightAdaptable: true,
				sliderResizable: true,
				sliderKeepAspectRatio: true,
				slidesDraggable: false
				// slidesDraggableMobileOnly: true,
				// slidesDragLimitFactor: 4
			});
		}
	}

	function booknow_popup() {
		$('body').on('click', '.book-event', function(e) {
			e.preventDefault();
			$('#event__bookticket').addClass('open');
			$('#bookticket__close').addClass('active');
		});
		$('body').on('click', '#bookticket__close', function(e) {
			e.preventDefault();
			close_booknow_popup();
		});
	}

	function close_booknow_popup() {
		$('#event__bookticket').removeClass('open');
		$('#bookticket__close').removeClass('active');
		$('#event__bookticket .booknow-popup.active').removeClass('active');
	}

	function close_overlay() {

		$('#overlay--contact').each(function() {
			$(this).removeClass('open');
		});
		$('#overlay__close').removeClass('active');
		if ($(window).scrollTop() < 158) {
			$('body').removeClass('nav-minified');
		}

	}

	function scroll_to_anchor() {

		$('body').on('click', 'a[href*=\\#], .book-event', function(e) {
			e.preventDefault();
			close_overlay(); // close overlay if open

			if ($(this).hasClass('book-event')) {
				var that_id = '#event';
			} else {
				/* Element variables */
				var that_id     = $(this).attr('href');
				var that_title  = that_id.substring(1, that_id.length);
			}

			if ($('#tab__button--' + that_title).length === 1) {
				$('#tab__button--' + that_title).trigger('click');
			}

			if ($(that_id).length === 0) {
				that_id = that_id.substr(0, that_id.indexOf('_'));
			}

			that_title  = that_id.substring(1, that_id.length); // update in case of any change to that_id

			/* Animation varialbles */
			var this_offset = $(this).offset();
			var that_offset = $(that_id).offset();
			var offset_diff = Math.abs(that_offset.top - this_offset.top);
			var base_speed  = 300; // Time in ms per 1,000 pixels
			var speed       = (offset_diff * base_speed) / 1000;
			var delay       = 0;

			// Get fixed nav height for calculation
			var nav_height;
			if ($(window).outerWidth() <= 400) {
				nav_height = 75 + (3 * 10);
			} else if ($(window).outerWidth() <= 768) {
				nav_height = 75 + (3 * 12);
			} else if ($(window).outerWidth() <= 1023) {
				nav_height = 75 + (3 * 14);
			} else {
				nav_height = 130 + (3 * 16);
			}

			// Alterations if anchor element is in an overlay
			var overlay = false;
			if ($(that_id).parents('#overlay--contact').length === 1) {
				$('#overlay--contact').addClass('open');
				$('#overlay__close').addClass('active');
				$('body').addClass('nav-minified')
				if ($(window).outerWidth() > 1365 && $(window).outerHeight() > 959) {
					var overlay = true;
				}
			}

			if ($(window).outerWidth() <= 960) {
				delay = 300;
			}

			if (!overlay) {
				$('html,body').delay(delay).animate({
					scrollTop: that_offset.top - nav_height
				}, speed);
			}
		});

	}

	function nav_mount() {
	/*
	 *	This function controls the mounting and unmounting of the
	 *	nav bar based upon the scroll position of the window.
	 */
		$(document).scroll(function(e){
			if ($(window).scrollTop() < 158) {
				$('body').removeClass('nav-minified');
			} else {
				$('body').addClass('nav-minified');
			}
		});
	}


	function toggle_content(toggler, container, activeClass, animate, animateEl) {
		if (animate === undefined) {
			animate = false;
		}

		var reset = function(current) {
			$(container).each(function(index) {
				if (index !== current) {
					if (animate) {
						if ($(this).closest(container).hasClass(activeClass)) {
							$(this).find(animateEl).animate({ height: 0 }, 300, function() {
								$(this).hide();
							});
						}
					}
					$(this).removeClass(activeClass);
				}
			});
		}
		$('body').on('click', toggler, function(e) {
			e.preventDefault();
			var clicked = $(this);
			var current;
			$(toggler).each(function(index) {
				if ($(this).is(clicked)) {
					current = index;
				}
			});
			reset(current);
			$(this).closest(container).toggleClass(activeClass);
			if (animate) {
				if ($(this).closest(container).hasClass(activeClass)) {
					$(this).closest(container).show();
					$(this).closest(container).find(animateEl).animate({ height: 'auto' }, 300);
				} else {
					$(this).closest(container).find(animateEl).animate({ height: '0' }, 300, function() {
						$(this).hide();
					});
				}
			}
		});

	}

	function toggler(toggler, container) {
		$(toggler).click(function(e) {
			e.preventDefault();
			$(container).toggleClass('open');
		});
	}

	function centre_content(to_center) {

		$(to_center).each(function() {
			var height = $(this).outerHeight();
			var margintop = Math.round(height / 2);
			$(this).css({
				'margin-top': -margintop + 'px'
			});
		});

	}

	function square_blocks(blocks) {

		var maxsize = 0;

		var get_max_size = function(block, index) {
			var width = $(block).outerWidth();
			if ($(block).is(':visible')) {
				if ((index === 0) || (width < maxsize)) {
					maxsize = width;
				}
			}
		}

		var set_height = function(block) {
			var height = maxsize;
			if (!($(block).parent().hasClass('w2xh2'))) {
				height = (maxsize * 2);
			}
			$(block).css('height', height) + 'px';
		}

		$(blocks).each(function(index) {
			get_max_size(this, index);
		});

		$(blocks).each(function() {
			set_height(this);
		});
	}

	var get_max_size = function(gb) {
		var width = $(gb).outerWidth() / colspan;
		if ((maxsize === 0) || (width < maxsize)) {
			maxsize = width;
		}
	}

	function tabbed_content(loaded) {

		var container; // Use: Store the current tab group which actions will be performed on

		var init = function() {
		/*
		 * Initialise a section that is to contain tabbed content
		*/
			$('section').each(function() { // Loop through each section
				container = $(this); // Set: Current section
				var tab_count = 0; // Use: Track number of tabs within section

				/* Get tab count */
				var tab_count = $(this).find('> .tab__buttons .tab__button').length;

				/* If tab buttons are present, add count to tab buttons wrapper */
				if (tab_count > 0) {
					tab_wrapper = $(this).find('> .tab__buttons');
					$(this).addClass('tab__count--' + tab_count);
				}

				/* Set top positioning for stacked tabs based on height of added tabs */
				if ($(container).attr('id') === 'event__acts') {
					if ($(window).outerWidth() <= 768) {
						var mobtabs = $(container).parent().find('> .mobiletabs');
						if (mobtabs.length > 0) {
							$(container).css({
								'top': $(mobtabs).outerHeight() + 'px'
							});
						}
					} else {
						$(container).css({
							'top': 0
						});
					}
				}
			});

			/* Set first tab of each tab group to active */
			$('.tab__buttons').each(function() {
				container = $(this).closest('section');
				update(0);
				$(this).find('.tab__button').each(function(index) {
					if (index === 0) {
						$(this).addClass('active');
					}
				});
			});

			/* Event: On click of a tab button */
			$('.tab__button').each(function() {
				$(this).click(function(e) {
					e.preventDefault();
					container = $(this).closest('section'); // Get tab group of clicked tab button
					update($(this).index()); // Call function to update active tab
					$(this).addClass('active');


					/* Re-init acts slider on show/date tab change */
					var fnEndNerve = function (el) {
					    var promise = $(el).endNerveSlider();
					    return promise.promise();
					};
					var fnHideActs = function (el) {
					    var promise = $(el).hide();
					    return promise.promise();
					};
					var fnShowActs = function (el) {
					    var promise = $(el).show();
					    return promise.promise();
					};
					var fnResponsify = function (el) {
					    window.picturefill();
					    return;
					};
					if ($(container).attr('id') === 'event__dates') {
						/* If the content is not loaded, the click event will be handled in ajax.js */
						if($(this).hasClass('loaded')) {
							var postid = $(this).data('postid');
							/* Apply the Slide show only if the - Nerve slider ended, Acts hidden, Current act shown, Picturefill applied */
							$.when(
										fnEndNerve('#event__acts .actgroup'),
										fnHideActs('#event__acts .actgroup:not(.actgroup__' + postid +')'),
										fnShowActs('#event__acts .actgroup__' + postid),
										fnResponsify()
									).done(function() {
										init_slideshow('#event__acts .actgroup__' + postid, true, true);
							});
						}
						/* Hide any open booking processes */
						close_booknow_popup();
					}
				});
			});

		}

		var update = function(current) {
		/*
		 * Set the passed tab from the current tab group to active
		*/
			var tab_id = $(container).find('.tab__button').get(current).id; // Get id of the tab button pressed
			var ajax_loaded = $('#' + tab_id).hasClass('loaded');  /* Flag to indicate, if the tab is loaded through Ajax, is the content loaded? */
			tab_id = tab_id.replace('tab__button--', ''); // Strip prefix used to differentiate id of button and tab itself
			/* Reset: Hide all tabs */
			if(ajax_loaded) { /* If not, it will be managed in th ajax.js */
				$(container).find('> section').each(function() {
					$(this).removeClass('active-tab');
				});
			}
			/* Remove class from previously active tab */
			$(container).find('> .tab__buttons .active').each(function() {
				$(this).removeClass('active');
			});
			/* Update: Show current tab */
			$('#' + tab_id).addClass('active-tab');

			/* accordion */
			if ($(container).get(0).id === 'info') {
				accordion(current, tab_id);
			}
		}

		var accordion = function(current, tab_id) {
		/*
		 * Add accordion functionality for info section
		*/
			var tab_count;

			var classes = $(container).attr('class').split(/\s/);
			for (var i = 0; i < classes.length; i++) {
				if (classes[i].substring(0, 12) == 'tab__count--') {
					tab_count = classes[i].substring(12, 13);
				}
			}
			if ($(window).outerWidth() <= 768) {
				var btn = $(container).find('.tab__button').get(0);
				var btn_height = $(btn).height();
				var paddingtop = (current + 1) * btn_height;
				var paddingbottom = (tab_count - (current + 1)) * btn_height;
				var ctr = $(container).find('#' + tab_id);
				var ctr_height = $(ctr).height();
				if ($('#tab__button--' + tab_id).hasClass('loaded')) {
					$(container).css({
						'padding-top': paddingtop + 'px',
						'padding-bottom': paddingbottom + 'px'
					});
					$(container).find('.tab__button').css({
						'padding-bottom': 0
					});
					$(container).find('#tab__button--' + tab_id).css({
						'padding-bottom': ctr_height + 'px'
					});
				} else {
					$(container).find('.tab__button').css({
						'padding-bottom': 0
					});

					var dom_inserted = false;
					$(container).on('DOMNodeInserted', '#' + tab_id, function() {
						if(!dom_inserted ) {

							dom_inserted = true;

							var set_padding = function() {
									ctr_height = $(container).find('#' + tab_id).height();
								    $(container).css({
										'padding-top': paddingtop + 'px',
										'padding-bottom': paddingbottom + 'px'
									});
									$(container).find('#tab__button--' + tab_id).css({
										'padding-bottom': ctr_height + 'px'
									});
							};

							var interval_id = setInterval( function() {
								// When the document has finished loading, stop checking for new images
								// https://github.com/ded/domready/blob/master/ready.js#L15
								if ( /^loaded|^i|^c/.test( document.readyState ) ) {
									set_padding();
									clearInterval( interval_id );
									return;
								}
							}, 250 );

						}

					});
				}
			}
		}

		init();

	}

	$(window).resize(function() {

		square_blocks('.social__block');
		// centre_content('.section__content');
		tabbed_content(true);

	});

	function aweber_validation() {
		$("#offers #form input.submit").click(function(event){
		    var error_free=true;

		    var _name = $("#offers #form #name"),
		    	_from = $("#offers #form #from");

		    _name.removeClass('invalid');
		    _from.removeClass('invalid');

		    if(!_name.val()) {
		    	error_free = false;
		    	_name.addClass('invalid');
		    }
		    if(!_from.val()) {
		    	error_free = false;
		    	_from.addClass('invalid');
		    } else {
		    	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  				if(!regex.test(_from.val())) {
			    	error_free = false;
			    	_from.addClass('invalid');
  				}
		    }
		    if (!error_free){
		        event.preventDefault();
		    }
		});
	}

})(jQuery);
