<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico" type="image/x-icon" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="description" content="The Wam Bam Club is a lively fusion of burlesque, comedy, magic and music deep in the heart of London's Soho. When you let Lady Alex draw you in to her seductive world of cabaret, expect a fresh, fun and devilishly unpredictable show. You'll never forget your first Wam Bam!" />
	<meta name="keywords" content="wam bam club,london burlesque,burlesque,comedy,magic,music,burlesque women,burlesque dancers,burlesque stars,burlesque queens,burlesque show,burlesque striptease,vintage burlesque,burlesque dancer" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<?php // Using clients account ?>
    <!-- Start Google Analytics Code -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-3852007-1', 'wambamclub.com');
	  ga('send', 'pageview');

	</script>
    <!-- End Google Analytics Code -->
    <script type="text/javascript" src="//use.typekit.net/oze8vel.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body <?php body_class(); ?>>
	<div id="topribbon">
		<p>Burlesque Supper Club</p>
	</div>
	<header id="masthead" class="site-header" role="banner">
		<div id="header__inner">
			<div id="header__logo">
				<h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo get_template_directory_uri() ?>/images/assets/wam-bam_logo.png" alt="Wam Bam Logo"  class="desktop"/>
					<img src="<?php echo get_template_directory_uri() ?>/images/assets/wam-bam_logo-mobile.png" alt="Wam Bam Logo" class="mobile" />
				</a>
			</div>
			<a href="" id="nav__toggle">
				<span class="bar1"></span>
				<span class="bar2"></span>
				<span class="bar3"></span>
			</a>

			<nav id="header__nav" class="site-navigation primary-navigation" role="navigation">
				<?php wp_nav_menu( array(
											'theme_location' => 'primary',
											'container' => false ,
											'walker'  => new absolute_walker_nav_menu()
										)
				); ?>
			</nav>
		</div>
	</header>

	<main>
