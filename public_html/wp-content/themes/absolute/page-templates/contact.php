<?php
/**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage absolute
 * @since Absolute 1.0
 */
/*  THIS TEMPALTE IS NOT USED. BUT JUST IN CASE IF THE PAGE IS ACCESSED DIRECTLY  */
get_header(); ?>

<?php get_template_part( 'page-templates/partials/content', 'contact' ); ?>

<?php
get_footer();


