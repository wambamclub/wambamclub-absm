<?php
/**
 * Called from the templates:
 * 		page-templates/testimonials.php
 * 		home.php
 */
?>
<?php $pages = get_page_children_or_current(); ?>
<section class="testimonial">
	<?php foreach($pages as $page) : ?>
		<article>
			<div class="section__content">
				<div class="section__content-inner">
					<?php echo do_shortcode($page->post_content) ?>
				</div>
			</div>
		</article>
	<?php endforeach; ?>
</section>
