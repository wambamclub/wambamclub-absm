<?php
/**
 * Called from the templates:
 * 		page-templates/social.php
 * 		home.php
 */
?>
<?php $pages = get_page_children_or_current(); ?>

<?php foreach($pages as $page) : ?>
	<div class="section__content">
		<div class="section__content-inner">
			<?php echo do_shortcode($page->post_content) ?>
		</div>
		<?php //get_template_part( 'page-templates/partials/content', 'socialwall' ); ?>
	</div>
<?php endforeach; ?>
