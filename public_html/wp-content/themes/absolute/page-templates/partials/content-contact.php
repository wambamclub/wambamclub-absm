<?php
/**
 * Called from the templates:
 * 		page-templates/contact.php
 * 		home.php
 */
?>
<?php $pages = get_page_children_or_current(); ?>
<?php foreach($pages as $page) : ?>
	<?php echo do_shortcode($page->post_content) ?>
<?php endforeach; ?>
<?php echo get_tab_links(); ?>