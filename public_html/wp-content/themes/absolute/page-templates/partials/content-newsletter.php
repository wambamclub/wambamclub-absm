<?php
/**
 * Called from the templates:
 * 		page-templates/contact.php
 * 		home.php
 */
?>
<?php $pages = get_page_children_or_current(); ?>
<section id="offers">
	<?php foreach($pages as $page) : ?>
		<div class="section__content">
			<?php echo do_shortcode($page->post_content) ?>
		</div>
	<?php endforeach; ?>
</section>
