<?php

	$args 				= array( 'post_type' => 'shows', 'meta_key' => 'wpcf-start-date', 'meta_value' => time(), 'meta_compare' => '>=', 'orderby' => 'meta_value', 'order' => 'ASC', 'posts_per_page' => -1 );
	$loop 				= new WP_Query( $args );
	$showonlyfirst		= true;
	$child_posts_list	= null;
	$_idx 				= 0;
	$_date_list 		= null;
?>

<section id="event__dates" title="Dates">
	<img src="wp-content/themes/absolute/images/assets/show/events/dates/dates-bg.jpg" alt="" />
	<?php while ( $loop->have_posts() ) : $loop->the_post();?>
		<?php
			$_start 		= get_post_meta( get_the_ID(), 'wpcf-start-date', true );
			$_end 			= get_post_meta( get_the_ID(), 'wpcf-end-date', true );
			$_embed_height 	= get_post_meta( get_the_ID(), 'wpcf-embed-height', true );
			$_evnturl 		= get_post_meta( get_the_ID(), 'wpcf-url-to-the-event', true );
			$_dt_id 		= date('d', $_start) . '-' . date('m', $_start);
			$_hidelocation 	= get_post_meta( get_the_ID(), 'wpcf-hide-location-link', true );
			$_hidepdfmenu 	= get_post_meta( get_the_ID(), 'wpcf-hide-menu-link', true );
			$_pdfmenu 	 	= get_post_meta( get_the_ID(), 'wpcf-menu-for-this-show', true );
		?>
		<?php if( ($showonlyfirst ? $_idx == 0 : true) ) : ?>
			<?php echo get_event_details($_start, $_end, $_embed_height, $_evnturl, $_dt_id, get_the_content(), $_hidelocation, $_pdfmenu, $_hidepdfmenu); ?>
			<?php
				$child_posts_list[get_the_ID()] = types_child_posts('show-act');
			?>
		<?php endif; ?>

		<?php
			$_date_list[$_idx]['id'] 			= date('d', $_start) . '-' . date('m', $_start);
			$_date_list[$_idx]['month'] 		= date('F', $_start) ;
			$_date_list[$_idx]['day'] 			= date('d', $_start);
			$_date_list[$_idx]['startdate'] 	= $_start;
			$_date_list[$_idx]['url'] 			= get_permalink( get_the_ID() );
			$_date_list[$_idx]['postid'] 		= get_the_ID();
			$_idx++;
		?>

    <?php endwhile; ?>
	<div class="tab__buttons">
		<div class="tab__overflow-indicator leftscroll"></div>
	    <div class="tab__buttons-inner">
	    	<?php
	    		if($_date_list) {
	    			foreach ($_date_list as $_j => $_dt) {
			    		echo '<div class="tab__button' . ($_j == 0 ? ' loaded' : ''). '" id="tab__button--dates__' . $_dt['id'] . '"  data-postid="'. $_dt['postid'] .'" data-type="event"><div class="tab__button-inner"><small>'. $_dt['month'] . '</small><a href="'. $_dt['url'] .'">'. $_dt['day'] .'</a></div></div>';
			    	}
	    		}
	    	?>
	    </div>
	    <div class="tab__overflow-indicator rightscroll"></div>
	</div>
</section>

<?php if($child_posts_list): ?>
	<section id="event__acts" title="Acts">
        <?php foreach ($child_posts_list as $_pid => $child_posts) : ?>
			<?php echo get_act_details($child_posts, $_pid); ?>
		<?php endforeach; ?>
	</section>
<?php endif; ?>

<?php wp_reset_query(); ?>