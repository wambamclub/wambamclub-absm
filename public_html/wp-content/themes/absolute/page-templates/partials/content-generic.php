<?php
/**
 * Called from the templates:
 * 		page-templates/generic.php
 * 		home.php
 */
?>
<?php $pages = get_page_children_or_current(); ?>
<?php foreach($pages as $page) : ?>
	<article>
		<?php
			$_responsive_shortcode = '[responsive imageid="'. get_post_thumbnail_id( $page->ID ) .'" postid="'. $page->ID .'"]';
			echo do_shortcode($_responsive_shortcode);
		?>
		<div class="section__content">
			<div class="section__content-inner">
				<?php echo do_shortcode($page->post_content) ?>
			</div>
		</div>
	</article>
<?php endforeach; ?>
