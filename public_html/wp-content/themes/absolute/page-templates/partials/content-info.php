<?php
/**
 * Called from the templates:
 * 		page-templates/info.php
 * 		home.php
 */
?>
<?php
    $pages      = get_page_children_or_current();
	$showonlyfirst	= true;
?>
<?php foreach($pages as $_idx => $page) : ?>
	<?php if( ($showonlyfirst ? $_idx == 0 : true) ) : ?>
		<section id="info__<?php  echo $page->post_name ?>" class="active-tab" title="<?php  echo $page->post_title ?>">
			<?php
				$_responsive_shortcode = '[responsive imageid="'. get_post_thumbnail_id( $page->ID ) .'" postid="'. $page->ID .'"]';
				echo do_shortcode($_responsive_shortcode);
			?>
			<div class="section__content">
				<div class="section__content-inner">
					<?php echo do_shortcode($page->post_content) ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
<?php endforeach; ?>

<?php echo get_tab_links("info__", !$showonlyfirst); ?>