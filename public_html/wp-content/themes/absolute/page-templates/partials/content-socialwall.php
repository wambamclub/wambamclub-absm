<div class="social__grid">
	<?php
    $_count 		= isset($_POST['count']) ? $_POST['count'] : 20;
    $_count = 16; // Hard set it to 16 on all devices.
		$socialwall 	= new AbsoluteSocialWall("wambamclub", "100", $_count);
	?>
	<?php
		switch ($_count) {
			case 4:
				# Mobile 			<= 384
				# Tablet portrait  	<= 768
				?>
				<div class="w2xh2">
					<?php echo $socialwall->show_next_tweet(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_tweet(); ?>
				</div>
				<?php
				break;

			case 5:
				# Tablet landscape <= 1024
				# Desktop 		   < 1600
				?>
				<div class="w2xh2">
					<?php echo $socialwall->show_next_tweet(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_tweet(); ?>
				</div>
				<?php echo $socialwall->show_next_photo(); ?>
				<?php
				break;

  		case 16:
  				?>
  					<div class="w2xh2">
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  					</div>
					  <?php echo $socialwall->show_next_photo(); ?>
					  <?php echo $socialwall->show_next_photo(); ?>
  					<div class="w2xh2">
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  					</div>
  					<div class="w2xh2">
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  						<?php echo $socialwall->show_next_photo(); ?>
  					</div>
					  <?php echo $socialwall->show_next_photo(); ?>
            <?php
  				break;

			case 20:
				# Desktop with width >= 1600
				?>
				<?php for($i = 1; $i<= 2; $i++): ?>
					<div class="w2xh2">
						<?php echo $socialwall->show_next_photo(); ?>
						<?php echo $socialwall->show_next_photo(); ?>
						<?php echo $socialwall->show_next_photo(); ?>
						<?php echo $socialwall->show_next_photo(); ?>
					</div>
					<?php echo $socialwall->show_next_photo(); ?>
				<?php endfor; ?>
				<?php for($i = 1; $i<= 2; $i++): ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<div class="w2xh2">
						<?php echo $socialwall->show_next_photo(); ?>
						<?php echo $socialwall->show_next_photo(); ?>
						<?php echo $socialwall->show_next_photo(); ?>
						<?php echo $socialwall->show_next_photo(); ?>
					</div>
				<?php endfor; ?>
				<?php
				break;
        
			default:
				# Serve only 4
				?>
				<div class="w2xh2">
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
					<?php echo $socialwall->show_next_photo(); ?>
				</div>
				<?php
				break;
		}
	?>
</div>
