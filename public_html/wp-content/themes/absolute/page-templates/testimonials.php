<?php
/**
 * Template Name: Testimonials
 *
 * @package WordPress
 * @subpackage absolute
 * @since Absolute 1.0
 */
/*  THIS TEMPALTE IS NOT USED. BUT JUST IN CASE IF THE PAGE IS ACCESSED DIRECTLY  */
get_header(); ?>

<?php get_template_part( 'page-templates/partials/content', 'testimonials' ); ?>

<?php
get_footer();


