<?php
/**
 * Template Name: Home
 *
 * @package WordPress
 * @subpackage absolute
 * @since Absolute 1.0
 */

get_header(); ?>

<?php $_parent_id 	= 30; // Banner parent ?>
<section id="banner">
	<?php get_template_part( 'page-templates/partials/content', 'generic' ); ?>
</section>

<?php $_parent_id 		= 44; // Testimonials parent ?>
<?php get_template_part( 'page-templates/partials/content', 'testimonials' ); ?>

<?php /*
<?php $_parent_id 		= 48; // Video/Show ?>
<section id="video">
	<?php get_template_part( 'page-templates/partials/content', 'generic' ); ?>
</section>
*/ ?>

<section id="info">
	<?php /* Set the order of the pages through the Order field */ ?>
	<?php $_parent_id 		= 89; // Info ?>
	<?php get_template_part( 'page-templates/partials/content', 'info' ); ?>
</section>

<?php $_parent_id 		= 55; // Testimonials parent ?>
<?php get_template_part( 'page-templates/partials/content', 'testimonials' ); ?>

<section id="event">

	<div class="tab__buttons tab__count--2 mobiletabs">
	    <div class="tab__buttons-wrap">
	        <div class="tab__button loaded" id="tab__button--event__dates">
	            <div class="tab__button-inner">
	                <a href="#">Events</a>
	            </div>
	        </div><div class="tab__button loaded" id="tab__button--event__acts">
	            <div class="tab__button-inner">
	                <a href="#">Acts</a>
	            </div>
	        </div>
	    </div>
	</div>

	<?php get_template_part( 'page-templates/partials/show', 'listing' ); ?>

	<div id="event__bookticket">
		<div id="bookticket__content">
			<a href="" id="bookticket__close">Close</a>
			<?php $_parent_id 		= 246; //  ?>
			<?php get_template_part( 'page-templates/partials/content', 'generic' ); ?>
		</div>
	</div>

</section>

<section id="social">
	<?php $_parent_id 		= 59; // Social ?>
	<?php get_template_part( 'page-templates/partials/content', 'social' ); ?>
</section>

<?php // $_parent_id 		= 67; // Newsletter Signup ?>
<?php // get_template_part( 'page-templates/partials/content', 'newsletter' ); ?>

<a href="" id="overlay__close">Close</a>
<section id="overlay--contact">

	<?php $_parent_id 		= 250; // Contact ?>
	<?php get_template_part( 'page-templates/partials/content', 'contact' ); ?>

</section>

<?php
get_footer();
