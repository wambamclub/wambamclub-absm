<?php
/**
 * The template for displaying posts in the Gallery post format
 *
 * Used for both single and index/archive.
 *
 * @since Nord 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php 
    wp_enqueue_script( 'nord-owl-carousel' );
    wp_enqueue_script( 'nord-owl-init' );
    
    wp_enqueue_style( 'nord-owl-carousel-css' );
    wp_enqueue_style( 'nord-owl-theme' );
    
    nord_format_content( 
      array( 
        'before'          => '<div class="format-gallery-carousel owl-carousel">',
        'after'           => '</div>',
        'images_size'     => 'nord-post-thumbnail-crop',
        'fallback_cb'     => 'nord_post_thumbnail'
      ), 
      true 
    );
  ?>
  
  <div class="inner">
    <header class="entry-header">
    <?php 
      if( is_sticky() && is_home() && ! is_paged() ) :
        printf( '<span class="sticky-badge">%s</span>', get_theme_mod( 'sticky_post_title' ) ? esc_html( get_theme_mod( 'sticky_post_title' ) ) : esc_html__( 'Featured', 'nord-theme' ) );
      endif;
      
      nord_category_list();
      
      if ( is_single() ) :
        the_title( '<h1 class="entry-title">', '</h1>' );
      else :
        the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
      endif;
      
      nord_entry_meta( '<div class="entry-meta">', '</div>' ); 
    ?>
    </header><!-- .entry-header -->

    <?php if( is_search() ) : ?>
      <div class="entry-summary">
      <?php the_excerpt(); ?>
      </div><!-- .entry-summary -->
    <?php else : ?>
      <div class="entry-content">
      <?php
        the_content( esc_html__( 'Read more', 'nord-theme' ) );
        
        wp_link_pages( array(
          'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nord-theme' ) . '</span>',
          'after'       => '</div>',
          'link_before' => '<span>',
          'link_after'  => '</span>',
          'pagelink'    => ' %',
          'separator'   => ' ',
        ) );
      ?>  
      </div><!-- .entry-content -->
    <?php endif; ?>

    <?php nord_entry_footer( '<footer class="entry-footer">', '</footer>' ); ?>
  </div>
</article><!-- #post-## -->
