<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

  <?php if ( is_active_sidebar( 'widget-area' ) ) : ?>
  <a href="#" id="sidebar-toggle" class="sidebar-toggle"><i class="menu-icon"></i></a>
  <?php endif; ?>
  
  <div class="wrapper">
    <div class="container">
      <header id="musthead" class="site-header">
        <?php do_action( 'nord_before_site_heading' ); ?>
        
        <div class="site-identity">
        <?php nord_site_identity(); ?>
        </div>
        
        <?php nord_social_profiles(); ?>
        
        <?php do_action( 'nord_after_site_heading' ); ?>
      </header>
      
      <?php if( has_nav_menu( 'primary' ) ) : ?>
      <nav id="primary-navigation" class="primary-navigation">
      <a href="#" id="nav-toggle" class="nav-toggle"><?php esc_html_e( 'Menu', 'nord-theme' ); ?></a>
      <?php 
        wp_nav_menu( array( 
          'theme_location'  => 'primary', 
          'menu_class'      => 'nav-menu', 
          'fallback_cb'     => false, 
          'container'       => 'div',
          'container_id'    => 'menu-container',
          'container_class' => 'menu-container' 
        ) ); 
      ?>
      </nav>
      <?php endif; ?>
      
      <div id="content" class="site-content">
