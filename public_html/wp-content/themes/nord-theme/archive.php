<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @since Nord 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
      <?php
        if( is_author() ) :
          printf( '<div class="author-avatar">%s</div>', get_avatar( get_queried_object_id(), 150 ) ); ?>
          
          <div class="author-description">
          <?php
            the_archive_title( '<h1 class="page-title">', '</h1>' );
            
            if( get_the_author_meta( 'description', get_queried_object_id() ) ) : ?>
              <p class="author-bio">
                <?php the_author_meta( 'description', get_queried_object_id() ); ?>
              </p>
          <?php
            endif;
          ?>
          </div>
      <?php 
        else :
          the_archive_title( '<h1 class="page-title">', '</h1>' );
          the_archive_description( '<div class="taxonomy-description">', '</div>' );
        endif;
      ?>
			</header><!-- .page-header -->

      <?php
      // Start the Loop.
      while ( have_posts() ) : the_post();

        /*
         * Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        get_template_part( 'content', get_post_format() );

      // End the loop.
      endwhile;
      
			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => '<i class="entypo-chevron-small-left"></i>' . esc_html__( 'Previous posts', 'nord-theme' ),
				'next_text'          => esc_html__( 'Next posts', 'nord-theme' ) . '<i class="entypo-chevron-small-right"></i>',
				'before_page_number' => ''
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );
		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
