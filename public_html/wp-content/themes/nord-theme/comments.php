<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @since Nord 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">
  <div class="comments-inner">
	<?php if ( have_comments() ) : ?>

	<h3 class="comments-title section-title">
    <span><?php printf( _nx( 'One comment on &ldquo;%2$s&rdquo;', '%1$s comments on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'nord-theme' ), number_format_i18n( get_comments_number() ), get_the_title() ); ?></span>
	</h3>

	<ol class="comment-list">
		<?php
			wp_list_comments( array(
				'style'      => 'ol',
				'short_ping' => true,
				'avatar_size'=> 100,
			) );
		?>
	</ol><!-- .comment-list -->

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
		<div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'nord-theme' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'nord-theme' ) ); ?></div>
	</nav><!-- #comment-nav-below -->
	<?php endif; // Check for comment navigation. ?>

	<?php if ( ! comments_open() ) : ?>
	<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'nord-theme' ); ?></p>
	<?php endif; ?>

	<?php endif; // have_comments() ?>

  <?php 
    $req      = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $html_req = ( $req ? " required='required'" : '' );
    $html5    = 'html5';
    
    comment_form( array(
      'comment_field' => '<div class="col-md-12"><p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="3" placeholder="' . esc_attr__( 'Comment', 'nord-theme' ) . '" aria-required="true"></textarea></p></div>',
      'fields' => array(
        'author' => '<div class="col-md-4"><p class="comment-form-author">
                    <input id="author" name="author" type="text" placeholder="' . esc_attr__( 'Name', 'nord-theme' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . $html_req . ' /></p></div>',
        'email'  => '<div class="col-md-4"><p class="comment-form-email">
                    <input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' placeholder="' . esc_attr__( 'Email', 'nord-theme' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-describedby="email-notes"' . $aria_req . $html_req  . ' /></p></div>',
        'url'    => '<div class="col-md-4"><p class="comment-form-url">
                    <input id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' placeholder="' . esc_attr__( 'Website', 'nord-theme' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p></div>',
      ),
      'comment_notes_after' => ' ',
      'cancel_reply_link' => '<i class="entypo-squared-cross"></i>'
    ) ); 
  ?>
  </div><!-- .comments-inner -->
</div><!-- #comments -->
