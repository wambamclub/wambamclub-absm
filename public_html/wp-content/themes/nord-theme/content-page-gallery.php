<?php
/**
 * The template used for displaying page content
 *
 * @since Nord 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php 
    wp_enqueue_script( 'nord-owl-carousel' );
    wp_enqueue_script( 'nord-owl-init' );
    
    wp_enqueue_style( 'nord-owl-carousel-css' );
    wp_enqueue_style( 'nord-owl-theme' );
    
    nord_format_content( 
      array( 
        'before'          => '<div class="format-gallery-carousel owl-carousel">',
        'after'           => '</div>',
        'images_size'     => 'nord-post-thumbnail-crop',
        'fallback_cb'     => 'nord_post_thumbnail'
      ), 
      true 
    );
  ?>
  
  <div class="inner">
    <header class="entry-header">
      <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
      <?php 
        the_content();
        
        wp_link_pages( array(
          'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'nord-theme' ) . '</span>',
          'after'       => '</div>',
          'link_before' => '<span>',
          'link_after'  => '</span>',
          'pagelink'    => ' %',
          'separator'   => ', ',
        ) );
      ?>
    </div><!-- .entry-content -->
    
    <?php 
      if( get_theme_mod( 'display_page_share_buttons', 1 ) ) {
        nord_share_buttons( '<footer class="entry-footer"><div class="entry-share">', '</div></footer>' ); 
      }
    ?>
  </div>
</article><!-- #post-## -->
