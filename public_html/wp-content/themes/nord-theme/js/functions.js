/**
 * Theme functions file.
 */
(function($) {
  "use strict";

  $(document).ready(function($){

    var $body = $('body');
    
    $('#sidebar-toggle').on( 'click', function(e) {
      e.preventDefault();
      $body.toggleClass('sidebar-open');
    });
    
    $('#nav-toggle').on( 'click', function(e) {
      e.preventDefault();
      $(this).next('#menu-container').slideToggle(400);
    });

    // Share Buttons
    $('.nord-share-button').on( 'click', function(e) {
      e.preventDefault();
      var $button = $(this),
          newWindowWidth = 550,
          newWindowHeight = 450,
          leftPos = ( $( window ).width()/2 ) - ( newWindowWidth/2 ),
          topPos = ( $( window ).height()/2 ) - ( newWindowHeight/2 );
          
			var newWindow = window.open( $button.attr('href'), '', 'height=' + newWindowWidth + ', width=' + newWindowHeight + ', left=' + leftPos + ', top=' + topPos );

			if ( window.focus )
				newWindow.focus();
    });
    
    // Fitvids init
    $('.post').fitVids();
  });
})(jQuery);