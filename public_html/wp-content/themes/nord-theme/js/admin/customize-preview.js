/**
 * Live-update changed settings in real time in the Customizer preview.
 */

( function( $ ) {
	var api = wp.customize;
	
	api( 'background_color', function( value ) {
		value.bind( function( to ) {
			$( '.wrapper' ).css( 'background-color', to );
		} );
	} );
	
	// Site title
	api( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	
  // Tagline
	api( 'tagline', function( value ) {
		value.bind( function( to ) {
			$( '.tagline' ).html( to );
		} );
	} );
	
	api( 'outer_link_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-header a, .site-footer a' ).css( 'color', to );
		} );
	} );
	
  api( 'outer_text_color', function( value ) {
		value.bind( function( to ) {
			$( '.site-header, .site-footer' ).css( 'color', to );
		} );
	} );
	
  api( 'link_color', function( value ) {
		value.bind( function( to ) {
			$( '.page-content a, .entry-content a, .post-meta a, .author-link, .logged-in-as a, .comment-content a, .comment-edit-link, .textwidget a, .widget_calendar a' ).css( 'color', to );
		} );
	} );
	
	api( 'link_color_hover', function( value ) {
		value.bind( function( to ) {
      var head = $('head'),
				  style = $('#custom-link-color-on-hover-css');
			
			var css = 'color: ' + to;
				  
		  style.remove();
			style = $('<style type="text/css" id="custom-link-color-on-hover-css">.page-content a:hover, .entry-content a:hover, .post-meta a:hover, .author-link:hover, .logged-in-as a:hover, .comment-content a:hover, .comment-edit-link:hover, .textwidget a:hover, .widget_calendar a:hover { ' + css + ' }</style>').appendTo( head );
		} );
	} );
	
	api( 'category_label_color', function( value ) {
		value.bind( function( to ) {
			$( '.cat-links a' ).css( 'background', to );
		} );
	} );
	
	api( 'button_background_color', function( value ) {
		value.bind( function( to ) {
			$( 'button, input[type="button"], input[type="reset"], input[type="submit"]' ).css( 'background-color', to );
		} );
	} );
	
  api( 'button_background_color_hover', function( value ) {
		value.bind( function( to ) {
      var head = $('head'),
				  style = $('#custom-button-background-color-hover-css');
			
			var css = 'background-color: ' + to;
				  
		  style.remove();
			style = $('<style type="text/css" id="custom-button-background-color-hover-css">button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover { ' + css + ' }</style>').appendTo( head );
		} );
	} );
	
  api( 'button_text_color', function( value ) {
		value.bind( function( to ) {
			$( 'button, input[type="button"], input[type="reset"], input[type="submit"]' ).css( 'color', to );
		} );
	} );
	
	api( 'sidebar_button_color', function( value ) {
		value.bind( function( to ) {
			$( '#sidebar-toggle' ).css( 'background', to );
		} );
	} );
	
	api( 'facebook_button_color', function( value ) {
		value.bind( function( to ) {
			$( '.nord-facebook-share-button' ).css( 'background', to );
		} );
	} );
	
	api( 'twitter_button_color', function( value ) {
		value.bind( function( to ) {
			$( '.nord-twitter-share-button' ).css( 'background', to );
		} );
	} );
	
  api( 'google_button_color', function( value ) {
		value.bind( function( to ) {
			$( '.nord-google-share-button' ).css( 'background', to );
		} );
	} );
	
	api( 'pinterest_button_color', function( value ) {
		value.bind( function( to ) {
			$( '.nord-pinterest-share-button' ).css( 'background', to );
		} );
	} );
	
	api( 'tumblr_button_color', function( value ) {
		value.bind( function( to ) {
			$( '.nord-tumblr-share-button' ).css( 'background', to );
		} );
	} );
	
	api( 'linkedin_button_color', function( value ) {
		value.bind( function( to ) {
			$( '.nord-linkedin-share-button' ).css( 'background', to );
		} );
	} );
	
} )( jQuery );
