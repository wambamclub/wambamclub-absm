/**
 * Custom gallery types
 */

jQuery(document).ready(function(){
  // add your shortcode attribute and its default value to the
  // gallery settings list; $.extend should work as well...
  _.extend( wp.media.gallery.defaults, {
    gallery_type: nordGalleriesDefaults.galleryType,
  });

  // merge default gallery settings template with yours
  wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
    template: function(view){
      return wp.media.template('gallery-settings')(view)
           + wp.media.template('nord-gallery-settings')(view);
    }
  });
});
