(function($) {
  "use strict";

  $(document).ready(function($){
    $( '#nord-category-color' ).wpColorPicker( {
      defaultColor: $(this).attr('data-default-color')
    } );
  });
})(jQuery);