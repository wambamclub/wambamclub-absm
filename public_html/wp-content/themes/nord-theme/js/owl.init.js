/**
 * Owl Carousels Init
 */
(function($) {
  "use strict";

  $(document).ready(function($){
  
    if( $.fn.owlCarousel !== undefined ) {
      $('.format-gallery-carousel, .post-gallery-carousel').owlCarousel({
        singleItem:true,
        autoHeight : true,
        navigation : true,
        pagination : false,
        slideSpeed : 400,
        navigationText : [ '<i class="entypo-chevron-small-left"></i>','<i class="entypo-chevron-small-right"></i>']
      });
    }
    
  });
})(jQuery);