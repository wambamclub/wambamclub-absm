<?php
/**
 * The Sidebar containing the main widget area
 *
 * @since Nord 1.0
 */
?>
<?php if ( is_active_sidebar( 'widget-area' ) ) : ?>
<div id="sidebar" class="sidebar">   
  <?php if( is_active_sidebar( 'widget-area' ) ) : ?>
  <div id="widget-area" class="widget-area" role="complementary">
    <?php dynamic_sidebar( 'widget-area' ); ?>
  </div>
  <?php endif; ?>
</div>
<?php endif; ?>