=== Nord 1.0 ===
Author: Nord Studio
Tags: black, white, dark, one-column, right-sidebar, fixed-layout, responsive-layout, flexible-header, accessibility-ready, custom-background, custom-colors, custom-header, custom-menu, featured-image-header, featured-images, sticky-post, theme-options, threaded-comments, translation-ready, photoblogging
Requires at least: 4.3, 4.4, 4.4.1, 4.4.2, 4.5
Tested up to: 4.5
Stable tag: 4.3, 4.4, 4.5
License URI: http://themeforest.net/licenses

== Description ==
Nord is a minimal and clean Wordpress blog theme. This theme is a content-focused, so perfect for a personal blog. A good choice for travelers, writers, artists, photographers, designers and creative people. Nord has a minimalistic layout that focuses on simplicity and readability. Easy installation allows you to start post blogs immediately after the activation. Theme supported Customizer which allows you to customize and change design of your blog. Nord is 100% responsive built with HTML5 & CSS3, it's SEO friendly, mobile optimized & retina ready.

* Responsive Layout
* Custom Header Image
* Custom background
* Custom Colors
* Post Formats
* Social Links
* Navigation Menu
* Widget Ready
* Accessibility
* Localization Ready

== Installation ==

1. In your admin panel, go to Appearance -> Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

05.12.2015 - 1.0 
  * First release
  
23.02.2016 - 1.1
  * More Customizer options.
  * Minor code and css fixes
  * Two new gallery types
  * Two new share buttons: LinkedIn & Google Plus.

29.04.2016 - 1.1.1
  * Compatibility with Wordpress 4.5
  * Small improvements of design and typography.