<?php
/**
 * The template for displaying Author bios
 *
 * @since Nord 1.0
 */
?>

<div class="author-info clearfix">
  <div class="author-inner">
    <?php 
      printf( '<h3 class="author-heading section-title">%s %s</h3>', esc_html__( 'Published by', 'nord-theme' ), get_the_author() );
      printf( '<div class="author-avatar">%s</div>', get_avatar( get_the_author_meta( 'user_email' ), 150 ) ); 
    ?>

    <div class="author-description">
      <p class="author-bio">
        <?php the_author_meta( 'description' ); ?>
      </p><!-- .author-bio -->
      <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
        <?php printf( esc_html__( 'View all posts by %s &rarr;', 'nord-theme' ), get_the_author() ); ?>
      </a>
    </div><!-- .author-description -->
  </div><!-- .author-inner -->
</div><!-- .author-info -->
