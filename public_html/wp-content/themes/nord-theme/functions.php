<?php
/**
 * Theme setup class.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 */

class Nord_Setup {

  // Theme version
  protected $version;
  
  // Theme unique id
  protected $theme_slug;
  
  public function __construct() {
    $this->version    = '1.1.1';
    $this->theme_slug = 'nord-theme';
  }
  
  /**
   * Setup theme. Add actions, filters, features etc.
   *
   * @since Nord 1.0
   */   
  public function setup() {
    global $content_width;

    /**
     * Set up the content width value based on the theme's design.
     */
    if ( ! isset( $content_width ) ) {
      $content_width = 1080;
    }
  
    /*
     * Make theme available for translation.
     *
     * Translations can be added to the /languages/ directory.
     */
    load_theme_textdomain( 'nord-theme', get_template_directory() . '/languages' );
  
    // Add RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // Enable support for Post Thumbnails, and declare two sizes.
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 1300, 9999, false );
    add_image_size( 'nord-post-thumbnail-crop', 1300, 815, true );
    
    // Register navigation menus.
    register_nav_menus( array(
      'primary' => esc_html__( 'Primary Navigation', 'nord-theme' )
    ) );
    
    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );
    
    /*
     * Enable support for Post Formats.
     *
     * See: https://codex.wordpress.org/Post_Formats
     */
    add_theme_support( 'post-formats', array( 'video', 'gallery' ) );
    add_post_type_support( 'page', 'post-formats' );
    
    // Add document title tag to HTML <head>.
    add_theme_support( 'title-tag' );
    
    // Setup the WordPress core custom header image.  
    add_theme_support( 'custom-header', array( 
      'width'       => 200,
      'height'      => 200,
      'flex-height' => true,
      'flex-width'  => true
    ) );
    
    // Setup the WordPress core custom background feature.    
    add_theme_support( 'custom-background', array( 
      'default-color' => '222222',
      'wp-head-callback' => array( $this, 'custom_background_cb' )
    ) );
    
    // Add public scripts and styles
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ), 10 );
    add_action( 'wp_enqueue_scripts', array( $this, 'post_nav_background' ), 10 );
    
    // Widgets init
    add_action( 'widgets_init', array( $this, 'widgets_init' ) );
    
    // Modify Read More link
    add_filter( 'the_content_more_link', array( $this, 'modify_read_more_link' ) );
    
    // Modify search form
    add_filter( 'get_search_form', array( $this, 'search_form' ) );
    
    // Set new excerpt more text
    add_filter('excerpt_more', array( $this, 'custom_excerpt_more' ));
    
    // This theme uses its own gallery styles.
    add_filter( 'use_default_gallery_style', '__return_false' );
    
    // Add classes to body tag.
    add_filter( 'body_class', array( $this, 'add_body_classes' ) );
    
    // Remove custom styles and texts transients when this updated
    add_action( 'customize_save_after', array( $this, 'reset_custom_styles_cache' ) );
    add_action( 'edited_category',      array( $this, 'reset_custom_styles_cache' ) );
    add_action( 'create_category',      array( $this, 'reset_custom_styles_cache' ) );
    add_action( 'customize_save_after', array( $this, 'reset_custom_texts_cache' ) );

    add_filter( 'wp_list_categories', array( $this, 'cat_count_span' ) );
    add_filter( 'get_archives_link',  array( $this, 'archive_count_span' ) );
  }
  
  /**
   * Register and enqueue theme scripts.
   *
   * @since Nord 1.0
   */  
  public function enqueue_scripts() {
    // Enqueue scripts
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }
    
    wp_register_script( 'nord-owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array( 'jquery' ), '1.3.2', true );
    wp_register_script( 'nord-owl-init',     get_template_directory_uri() . '/js/owl.init.js', array( 'jquery', 'nord-owl-carousel' ), '1.0', true );

    wp_enqueue_script( 'nord-jquery-fitvids',  get_template_directory_uri() . '/js/jquery.fitvids.js', array( 'jquery' ), '1.1', true );
    wp_enqueue_script( 'nord-functions',       get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), $this->version, true );     
  }
  
  /**
   * Register and enqueue theme styles.
   *
   * @since Nord 1.0
   */    
  public function enqueue_styles() {
    // Add fonts, used in the main stylesheet.
    wp_enqueue_style( 'nord-fonts', $this->fonts_url(), array(), $this->version );
    
    // Entypo icons font style
    wp_register_style( 'nord-entypo', get_template_directory_uri() . '/css/entypo.css', array(), '2.0.0' );
    
    // Owl Carousel
    wp_register_style( 'nord-owl-carousel-css', get_template_directory_uri() . '/css/owl.carousel.css', array(), '1.3.2' );
    wp_register_style( 'nord-owl-theme', get_template_directory_uri() . '/css/owl.theme.css', array(), '1.3.2' );
    
    // normalize.css and bootstrap grid
    wp_enqueue_style( 'nord-bootstrap-custom', get_template_directory_uri() . '/css/bootstrap-custom.css', array(), '3.3.2' );      
    
    // Load our main stylesheet.
    wp_enqueue_style( 'nord-style', get_stylesheet_uri(), array( 'nord-entypo' ), $this->version );
    
    // Add customizer css.
    wp_add_inline_style( 'nord-style', $this->inline_style() );
  }
  
  /**
   * Register Google fonts for Nord.
   *
   * @since Nord 1.0
   *
   * @return string Google fonts URL for the theme.
   */  
  public function fonts_url() {
    $fonts_url = '';
    $fonts     = array();
    $subsets   = 'latin,latin-ext';

    /* translators: If there are characters in your language that are not supported by Lora, translate this to 'off'. Do not translate into your own language. */
    if ( 'off' !== esc_attr_x( 'on', 'Lora font: on or off', 'nord-theme' ) ) {
      $fonts[] = 'Lora:400,400italic,700,700italic';
    }

    /* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
    if ( 'off' !== esc_attr_x( 'on', 'Montserrat font: on or off', 'nord-theme' ) ) {
      $fonts[] = 'Montserrat:400';
    }

    /* translators: To add an additional character subset specific to your language, translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language. */
    $subset = esc_attr_x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'nord-theme' );

    if ( 'cyrillic' == $subset ) {
      $subsets .= ',cyrillic,cyrillic-ext';
    } elseif ( 'greek' == $subset ) {
      $subsets .= ',greek,greek-ext';
    } elseif ( 'devanagari' == $subset ) {
      $subsets .= ',devanagari';
    } elseif ( 'vietnamese' == $subset ) {
      $subsets .= ',vietnamese';
    }

    if ( $fonts ) {
      $fonts_url = esc_url( add_query_arg( array(
        'family' => urlencode( implode( '|', $fonts ) ),
        'subset' => urlencode( $subsets ),
      ), '//fonts.googleapis.com/css' ) );
    }

    return $fonts_url;
  }
  
  /**
   * Inline style. Used in wp_add_inline_style.
   *
   * @since Nord 1.0
   */  
  public function inline_style() {
    if( is_customize_preview() )
      return $this->get_inline_style();
      
    $inline_style = get_transient( 'nord_inline_style' );
    
    if( $inline_style === false ) {
      $inline_style = $this->get_inline_style();
      set_transient( 'nord_inline_style', $inline_style );
    }
    
    return $inline_style;
  }
  
  /**
   * Get inline style CSS.
   *
   * @since Nord 1.0
   *
   * @return string Css with custom styles.
   */  
  public function get_inline_style() {
    $css = '';
    
    $header_image_max_width = absint( get_theme_mod( 'header_image_max_width' ) );
    
    if( $header_image_max_width ){
      $css .= sprintf( '
        .header-image img{
          max-width: %dpx;
          height: auto;
        }',
        $header_image_max_width
      );
    }

    $header_image_margin_bottom = absint( get_theme_mod( 'header_image_margin_bottom' ) );
    
    if( $header_image_margin_bottom ){
      $css .= sprintf( '
        .header-image img {
          margin-bottom: %dpx;
        }',
        $header_image_margin_bottom
      );
    }
    
    $outer_link_color = $this->sanitize_hex_color( get_theme_mod( 'outer_link_color' ) );
    
    if( $outer_link_color ) {
      $css .= sprintf( '
        .site-header a,
        .site-footer a {
          color: %s;
        }',
        $outer_link_color
      );
    }
    
    $outer_text_color = $this->sanitize_hex_color( get_theme_mod( 'outer_text_color' ) );
    
    if( $outer_text_color ) {
      $css .= sprintf( '
        .site-header,
        .site-footer {
          color: %s;
        }',
        $outer_text_color
      );
    }
    
    $link_color = $this->sanitize_hex_color( get_theme_mod( 'link_color' ) );
    
    if( $link_color ) {
      $css .= sprintf( '
        .page-content a,
        .entry-content a,
        .post-meta a,
        .author-link,
        .logged-in-as a,
        .comment-content a,
        .comment-edit-link {
          color: %s;
        }',
        $link_color
      );
    }
    
    $link_color_hover = $this->sanitize_hex_color( get_theme_mod( 'link_color_hover' ) );
    
    if( $link_color_hover ) {
      $css .= sprintf( '
        .page-content a:hover,
        .entry-content a:hover,
        .post-meta a:hover,
        .author-link:hover,
        .logged-in-as a:hover,
        .comment-content a:hover,
        .comment-edit-link:hover {
          color: %s;
        }',
        $link_color_hover
      );
    }
    
    $category_label_color = $this->sanitize_hex_color( get_theme_mod( 'category_label_color' ) );
    
    if( $category_label_color ) {
      $css .= sprintf( '
        .cat-links a {
          background: %s;
        }',
        $category_label_color
      );
    }
    
    $categories_color = get_option( 'nord_categories_color' );
    
    if( is_array( $categories_color ) ) {
      foreach( $categories_color as $category_id => $category_color ) {
        if( $this->sanitize_hex_color( $category_color ) ) {
          $css .= sprintf( '
        .cat-links .category-%d { 
          background: %s 
        }
          ', 
          absint( $category_id ), 
          $category_color 
         );
        }
      }
    }
    
    $button_background_color = $this->sanitize_hex_color( get_theme_mod( 'button_background_color' ) );
    
    if( $button_background_color ) {
      $css .= sprintf( '
        button,
        input[type="button"],
        input[type="reset"],
        input[type="submit"] {
          background-color: %s;
        }',
        $button_background_color
      );
    }
    
    $button_background_color_hover = $this->sanitize_hex_color( get_theme_mod( 'button_background_color_hover' ) );
    
    if( $button_background_color_hover ) {
      $css .= sprintf( '
        button:hover,
        input[type="button"]:hover,
        input[type="reset"]:hover,
        input[type="submit"]:hover {
          background-color: %s;
        }',
        $button_background_color_hover
      );
    }
    
    $button_text_color = $this->sanitize_hex_color( get_theme_mod( 'button_text_color' ) );
    
    if( $button_text_color ) {
      $css .= sprintf( '
        button,
        input[type="button"],
        input[type="reset"],
        input[type="submit"] {
          color: %s;
        }',
        $button_text_color
      );
    }
    
    $sidebar_button_color = $this->sanitize_hex_color( get_theme_mod( 'sidebar_button_color' ) );
    
    if( $sidebar_button_color ) {
      $css .= sprintf( '
        .sidebar-toggle {
          background: %s;
        }', 
        $sidebar_button_color 
      );
    }
    
    $facebook_button_color = $this->sanitize_hex_color( get_theme_mod( 'facebook_button_color' ) );
    
    if( $facebook_button_color ) {
      $css .= sprintf( '
        .share-buttons .nord-facebook-share-button {
          background: %s;
        }', 
        $facebook_button_color 
      );
    }
    
    $twitter_button_color = $this->sanitize_hex_color( get_theme_mod( 'twitter_button_color' ) );
    
    if( $twitter_button_color ) {
      $css .= sprintf( '
        .share-buttons .nord-twitter-share-button {
          background: %s;
        }', 
        $twitter_button_color 
      );
    }
    
    $pinterest_button_color = $this->sanitize_hex_color( get_theme_mod( 'pinterest_button_color' ) );
    
    if( $pinterest_button_color ) {
      $css .= sprintf( '
        .share-buttons .nord-pinterest-share-button {
          background: %s;
        }', 
        $pinterest_button_color 
      );
    }
    
    $tumblr_button_color = $this->sanitize_hex_color( get_theme_mod( 'tumblr_button_color' ) );
    
    if( $tumblr_button_color ) {
      $css .= sprintf( '
        .share-buttons .nord-tumblr-share-button {
          background: %s;
        }', 
        $tumblr_button_color 
      );
    }
    
    $google_button_color = $this->sanitize_hex_color( get_theme_mod( 'google_button_color' ) );
    
    if( $google_button_color ) {
      $css .= sprintf( '
        .share-buttons .nord-google-share-button {
          background: %s;
        }', 
        $google_button_color 
      );
    }
    
    $linkedin_button_color = $this->sanitize_hex_color( get_theme_mod( 'linkedin_button_color' ) );
    
    if( $linkedin_button_color ) {
      $css .= sprintf( '
        .share-buttons .nord-linkedin-share-button {
          background: %s;
        }', 
        $linkedin_button_color 
      );
    }

    $custom_css = get_theme_mod( 'general_css' );
    
    if( $custom_css ) {
       $css .= wp_filter_nohtml_kses( $custom_css );
    }
    
    return $css;
  }
  
  /**
   * Sanitizes a hex color.
   *
   * @since Nord 1.0
   */  
  public function sanitize_hex_color( $color ) {
    if ( '' === $color )
      return '';
      
    // 3 or 6 hex digits, or the empty string.
    if ( preg_match('|^#([A-Fa-f0-9]{3}){1,2}$|', $color ) )
      return $color; 
  }
  
  /**
   * Add featured image as background image to post navigation elements.
   *
   * @since Nord 1.0
   *
   * @see wp_add_inline_style()
   */  
  function post_nav_background() {
    if ( ! is_single() ) {
      return;
    }

    $previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );
    $css      = '';

    if ( is_attachment() && 'attachment' == $previous->post_type ) {
      return;
    }

    if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
      $prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
      $css .= '
        .post-navigation .nav-previous { 
          background-image: url(' . esc_url( $prevthumb[0] ) . '); 
        }
        .post-navigation .nav-previous .post-title, 
        .post-navigation .nav-previous a:hover .post-title { 
          color: #fff; 
        }
        .post-navigation .nav-previous .nav-meta {
          color: #ddd;
        }
        .post-navigation .nav-previous:before { 
          background-color: rgba(0, 0, 0, 0.5); 
        }
        .post-navigation .nav-previous:hover:before { 
          background-color: rgba(0, 0, 0, 0.8); 
        }
      ';
    }

    if ( $next && has_post_thumbnail( $next->ID ) ) {
      $nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
      $css .= '
        .post-navigation .nav-next { 
          background-image: url(' . esc_url( $nextthumb[0] ) . '); 
        }
        .post-navigation .nav-next .post-title, 
        .post-navigation .nav-next a:hover .post-title { 
          color: #fff; 
        }
        .post-navigation .nav-next .nav-meta {
          color: #ddd;
        }
        .post-navigation .nav-next:before { 
          background-color: rgba(0, 0, 0, 0.5); 
        }
        .post-navigation .nav-next:hover:before { 
          background-color: rgba(0, 0, 0, 0.8); 
        }
      ';
    }

    wp_add_inline_style( 'nord-style', $css );
  }
  
  /**
   * Reset custom styles cache.
   *
   * @since Nord 1.0
   */
  function reset_custom_styles_cache() {
    delete_transient( 'nord_inline_style' );
  }
  
  /**
   * Reset custom text cache.
   *
   * @since Nord 1.0
   */
  function reset_custom_texts_cache() {
    delete_transient( 'nord_tagline' );
    delete_transient( 'nord_copyright' );
  }

  /**
   * Register widget area.
   *
   * @since Nord 1.0
   *
   * @link https://codex.wordpress.org/Function_Reference/register_sidebar
   */
  public function widgets_init() {
    require get_template_directory() . '/inc/widgets.php';
    register_widget( 'Nord_Instagram_Widget' );
    register_widget( 'Nord_Recent_Posts' );
    
    // Register sidebar
    register_sidebar( array(
      'name'          => esc_html__( 'Sidebar Widgets', 'nord-theme' ),
      'id'            => 'widget-area',
      'description'   => esc_html__( 'Add widgets here to appear in sidebar', 'nord-theme' ),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget'  => '</aside>',
      'before_title'  => '<h3 class="widget-title">',
      'after_title'   => '</h3>',
    ) );
  }
  
  /**
   * More link modification.
   *
   * @since Nord 1.0
   *
   * @return string Modified more link.
   */  
  public function modify_read_more_link() {
    $more_link_text = get_theme_mod( 'more_link_title' ) ? get_theme_mod( 'more_link_title' ) : esc_html__( 'Read More', 'nord-theme' );
  
    return sprintf( '<p class="entry-more"><a class="more-link" href="%s">%s<i class="entypo-chevron-small-right"></i></a></p>', esc_url( get_permalink() ), esc_html( $more_link_text ) );
  }
  
  /**
   * Search form modification.
   *
   * @since Nord 1.0
   *
   * @return string HTML with search form.
   */   
  public function search_form( $form ) {
  
    $form = '
      <form method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
        <div class="search-wrap">
          <label>
            <input type="search" class="search-field" placeholder="' . esc_attr_x( 'Search', 'placeholder', 'nord-theme' ) . '" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', 'title tag', 'nord-theme' ) . '" />
          </label>
          <button type="submit" class="search-submit"><i class="entypo-magnifying-glass"></i></button>
        </div>
			</form>';
			
    return $form;
  }
  
  /**
   * Modification default excerpt more.
   *
   * @since Nord 1.0
   *
   * @return string Excerpt more text.
   */   
  public function custom_excerpt_more( $more ) {
    return '...';
  }
  
   /**
   * Add classes to body tag.
   *
   * @since Nord 1.0
   *
   * @return array Body classes array.
   */    
  public function add_body_classes( $classes ) {
    if( in_array( get_theme_mod( 'navigation_color_scheme', 'dark' ), array( 'dark', 'light' ) ) ) {
      $classes[] = 'primary-navigation-' . get_theme_mod( 'navigation_color_scheme', 'dark' );
    }

    //$classes[] = 'sidebar-open';
          
    return $classes;
  }
   
  /**
   * Remove () and add <span> tag to posts count in categories list.
   *
   * @since Nord 1.0
   *
   * @return string Html with links
   */      
  function cat_count_span( $links ) {
    $links = str_replace( '</a> (', '</a> <span>', $links );
    $links = str_replace( ')', '</span>', $links );
    return $links;
  }
  
  /**
   * Remove () and add <span> tag to posts count in archives list.
   *
   * @since Nord 1.0
   *
   * @return string Html with links
   */   
  function archive_count_span( $link ) {
    $link = str_replace( '</a>&nbsp;(', '</a> <span>', $link );
    $link = str_replace( ')</li>', '</span></li>', $link );
    return $link;
  }
  
  /**
   * Custom background callback.
   *
   * @since Nord 1.0
   */  
  public function custom_background_cb() {
    $color = get_background_color();

    if ( $color === get_theme_support( 'custom-background', 'default-color' ) ) {
      $color = false;
    }

    if ( ! $color )
      return;

    $style = $color ? "background-color: #$color;" : '';
    
    if( $style ) {
      printf( '<style type="text/css" id="custom-background-css">.custom-background .wrapper { %s }</style>', esc_attr( trim( $style ) ) );
    }
  }
}

$nord_setup = new Nord_Setup();

add_action( 'after_setup_theme', array( $nord_setup, 'setup' ), 10 );

/**
 * Customizer additions.
 *
 * @since Nord 1.0
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom template tags for this theme.
 *
 * @since Nord 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Post formats output class.
 *
 * @since Nord 1.0
 */
require get_template_directory() . '/inc/post-formats.php';

/**
 * Add fields to category form.
 *
 * @since Nord 1.0
 */
require get_template_directory() . '/inc/category-form-fields.php';

/**
 * Additional types of galleries
 *
 * @since Nord 1.1
 */
require get_template_directory() . '/inc/galleries.php';