<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @since Nord 1.0
 */
?>
        </div><!-- .site-content --> 
        
        <footer id="colophon" class="site-footer">
        <?php 
          do_action( 'nord_before_site_copyright' );
          
          nord_site_copyright( '<div class="site-info">', '</div>' );
          
          do_action( 'nord_after_site_copyright' ); 
        ?>
        </footer>
      </div><!-- .container -->
    </div><!-- .wrapper -->
    
    <?php get_sidebar(); ?>
  </div><!-- #page -->
  
	<?php wp_footer(); ?>
</body>
</html>