<?php
/**
 * Theme Widgets
 *
 * @package WordPress
 * @since Nord 1.0
 */

/**
 * Nord Instagram Widget
 *
 * Display Instagram feed in your blog.
 *
 * @since Nord 1.0
 */
class Nord_Instagram_Widget extends WP_Widget {
  /**
	 * Constructor.
	 */
	public function __construct() {
		parent::__construct( 'nord-instagram-feed', esc_html__( 'Nord &mdash; Instagram', 'nord-theme' ), array(
			'classname'   => 'nord-instagram-feed',
			'description' => esc_html__( 'Widget displays latest Instagram images.', 'nord-theme' ),
		) );
	}

	function widget( $args, $instance ) {

		extract( $args, EXTR_SKIP );

		$title    = empty( $instance['title'] ) ? '' : apply_filters( 'widget_title', $instance['title'] );
		$username = empty( $instance['username'] ) ? '' : $instance['username'];
		$number   = empty( $instance['number'] ) ? 9 : absint( $instance['number'] );
		$columns  = empty( $instance['cols'] ) ? 3 : absint( $instance['cols'] );

		echo $args['before_widget'];
		
		if ( ! empty( $title ) ) {
			printf( '%s%s%s', $args['before_title'], $title, $args['after_title'] );
		} 

		if ( $username && $number ) {

			$instagram_items = $this->parse_instagram( $username, $number );

			if ( is_wp_error( $instagram_items ) ) {
			   printf( '<div class="error-message">%s</div>', $instagram_items->get_error_message() );
      } else {
      
        $intagram_output = '';
        
        foreach ( $instagram_items as $item ) {
          $intagram_output .= sprintf( '<li><a href="%s" target="_blank" rel="nofollow"><img src="%s" alt="%s" title="%s" /></a></li>', esc_url( $item['link'] ), esc_url( $item['thumbnail'] ), esc_attr( $item['description'] ), esc_attr( $item['description'] ) );
        } 
        
        if( $intagram_output ) {
          printf( '<ul class="instagram-pics clearfix %1$s">%2$s</ul>', $columns ? 'cols-' . $columns : '', $intagram_output );
        }
      }
		}
		
		echo $args['after_widget'];
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, 
      array( 
        'title'    => esc_html__( 'Instagram', 'nord-theme' ), 
        'username' => '', 
        'number'   => 9, 
        'cols'     => 3
      ) 
    );
	?>
		<p>
      <label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e( 'Title:', 'nord-theme' ); ?>
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
      </label>
    </p>
    
		<p>
      <label for="<?php echo esc_attr( $this->get_field_id('username') ); ?>"><?php esc_html_e( 'Username:', 'nord-theme' ); ?> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('username') ); ?>" name="<?php echo esc_attr( $this->get_field_name('username') ); ?>" type="text" value="<?php echo esc_attr( $instance['username'] ); ?>" />
      </label>
    </p>
    
		<p>
      <label for="<?php echo esc_attr( $this->get_field_id('number') ); ?>"><?php esc_html_e( 'Number of photos:', 'nord-theme' ); ?> 
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('number') ); ?>" name="<?php echo esc_attr( $this->get_field_name('number') ); ?>" type="text" value="<?php echo absint( $instance['number'] ); ?>" />
      </label>
    </p>
    
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id('cols') ); ?>"><?php esc_html_e( 'Columns:', 'nord-theme' ); ?> 
        <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('cols') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cols') ); ?>">
          <option value="2" <?php selected( absint( $instance['cols'] ), 2 ); ?>>2</option>
          <option value="3" <?php selected( absint( $instance['cols'] ), 3 ); ?>>3</option>
          <option value="4" <?php selected( absint( $instance['cols'] ), 4 ); ?>>4</option>
        </select>
      </label>
    </p>
		<?php
	}

	function update( $new_instance, $instance ) {
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['username'] = trim( strip_tags( $new_instance['username'] ) );
		$instance['number']   = $new_instance['number'] ? absint( $new_instance['number'] ) : 9;
		$instance['cols']     = $new_instance['cols'] ? absint( $new_instance['cols'] ) : 9;
		return $instance;
	}

	// based on https://gist.github.com/cosmocatalano/4544576
	// and https://github.com/scottsweb/wp-instagram-widget
	// with some changes in the code
	function parse_instagram( $username, $slice = 9 ) {

		$username = strtolower( $username );
    $instagram = get_transient( 'nord-instagram-media-new-' . sanitize_title_with_dashes( $username ) );
    
		if ( false === $instagram ) {

			$remote = wp_remote_get( 'http://instagram.com/' . trim( $username ), array( 'timeout' => 10 )  );

			if ( is_wp_error( $remote ) )
				return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'nord-theme' ) );

			if ( 200 != wp_remote_retrieve_response_code( $remote ) )
				return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'nord-theme' ) );

			$shards = explode( 'window._sharedData = ', $remote['body'] );
			$insta_json = explode( ';</script>', $shards[1] );
			$insta_array = json_decode( $insta_json[0], TRUE );

			if ( ! $insta_array )
				return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'nord-theme' ) );

			if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
				$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
			} else {
				return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'nord-theme' ) );
			}

			if ( ! is_array( $images ) )
				return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'nord-theme' ) );

			$instagram = array();

			foreach ( $images as $image ) {
				$image['thumbnail_src'] = preg_replace( "/^https:/i", "", $image['thumbnail_src'] );
				
				$type = ( $image['is_video'] == true ) ? 'video' : 'image';
        $description = ! empty( $image['caption'] ) ? $image['caption'] : '';

				$instagram[] = array(
					'description' => $description,
					'link'		  	=> '//instagram.com/p/' . $image['code'],
					'time'		  	=> $image['date'],
					'comments'	  => $image['comments']['count'],
					'likes'		 	  => $image['likes']['count'],
					'thumbnail'	 	=> $image['thumbnail_src']
				);
			}

			// do not set an empty transient - should help catch private or empty accounts
			if ( ! empty( $instagram ) ) {
				$instagram = serialize( $instagram );
				set_transient( 'nord-instagram-media-new-' . sanitize_title_with_dashes( $username ), $instagram, apply_filters( 'nord_instagram_cache_time', HOUR_IN_SECONDS*2 ) );
			}
		}

		if ( ! empty( $instagram ) ) {
			$instagram = unserialize( $instagram );
			return array_slice( $instagram, 0, $slice );
		} else {
			return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'nord-theme' ) );
		}
	}
}

/**
 * Advanced recent posts widget class
 *
 * @since Nord 1.0
 */
class Nord_Recent_Posts extends WP_Widget {

	private $orderby;
	
	/**
	 * Constructor.
	 */
	public function __construct() {
	
		parent::__construct( 'nord-widget-recent-posts', esc_html__( 'Nord &mdash; Recent Posts', 'nord-theme' ), array(
			'classname'   => 'nord-widget-recent-posts',
			'description' => esc_html__( 'Use this widget to list your recent posts.', 'nord-theme' ),
		) );

    $this->orderby = array(
     'date'         => esc_html__( 'date', 'nord-theme' ), 
     'comments_num' => esc_html__( 'number of comments', 'nord-theme' ),
     'random'       => esc_html__( 'random', 'nord-theme' )
    );
    
    add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
	}
	
		/**
	 * Enqueue admin widgets page scripts and styles.
	 *
	 * @access public
	 */	
	public function admin_enqueue_scripts( $hook ) {
    if ( 'widgets.php' != $hook )
          return;

    wp_enqueue_style( 'nord-widget-recent-posts-css', get_template_directory_uri() . '/css/admin/widget-recent-posts.css', array(), '1.0'); 
	}
	
	/**
	 * Output the HTML for this widget.
	 *
	 * @access public
	 *
	 * @param array $args     An array of standard parameters for widgets in this theme.
	 * @param array $instance An array of settings for this widget instance.
	 */
	public function widget( $args, $instance ) {
	
		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		$title = ( ! empty( $instance['title'] ) ) ? strip_tags( $instance['title'] ) : esc_html__( 'Recent Posts', 'nord-theme' );
	
		$query_args = array(
		  'order'          => 'DESC',
			'posts_per_page' => $number,
			'no_found_rows'  => true,
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'post__not_in'   => get_option( 'sticky_posts' )
		);
		
    $query_args['tax_query'] = array();
    
    if( ! empty( $instance['category'] ) ){
      $query_args['tax_query'][] = array(
          'taxonomy' => 'category',
          'terms'    => absint( $instance['category'] ),
          'field'    => 'term_id'
      );
		}
		
		if( ! empty( $instance['tags'] ) ) {
      $tags = explode( ',', $instance['tags'] );
      $query_args['tax_query'][] = array(
          'taxonomy' => 'post_tag',
          'terms'    => $tags,
          'field'    => 'name'
      );
		}

    if( count( $query_args['tax_query'] ) > 1 ){
      $query_args['tax_query']['relation'] = 'AND';
    }

		$orderby = isset( $instance['orderby'] ) ? $instance['orderby'] : 'date';
		
		switch ($orderby) {
      case 'comments_num':
        $query_args['orderby'] = 'comment_count';
        break;
      case 'random':
        $query_args['orderby'] = 'rand';
        break;
		}

		$recent_posts = new WP_Query( $query_args );

		if ( $recent_posts->have_posts() ) :
      $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

			echo $args['before_widget'];
	    
			if ( ! empty( $title ) ) {
        printf( '%s%s%s', $args['before_title'], $title, $args['after_title'] );
      } 
		?>
			
		<ul>
      <?php while ( $recent_posts->have_posts() ) : $recent_posts->the_post(); ?>
      
    	<li <?php post_class( 'clearfix' ); ?>>
    	
        <?php if ( has_post_thumbnail() ) : ?>
        <div class="post-thumbnail">
          <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a>
        </div>
        <?php endif; ?>
        
        <header class="entry-header">
          <?php 
            if( ! get_the_title() ) {
              $format = get_post_format();
              $default_title = ( ! $format || $format == 'standard' ) ? esc_html__( 'Untitled Post', 'nord-theme' ) : get_post_format_string( $format );
            }
          ?>
          <a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : printf( $default_title ); ?></a>
          <?php 
            if( $show_date ) {
              printf( '<span class="post-date">%s</span>', get_the_date() );
            } 
          ?>
        </header>
			</li>  
      <?php endwhile; ?>
		</ul>	
			
	  <?php
			echo $args['after_widget'];

			// Reset the post globals as this query will have stomped on it.
			wp_reset_postdata();
		endif;
	}
	
	/**
	 * Deal with the settings when they are saved by the admin.
	 *
	 * Here is where any validation should happen.
	 *
	 * @param array $new_instance New widget instance.
	 * @param array $instance     Original widget instance.
	 * @return array Updated widget instance.
	 */
	function update( $new_instance, $instance ) {
    $instance['title']  = strip_tags( $new_instance['title'] );
    $instance['number'] = ( !empty( $new_instance['number'] ) ) ? absint( $new_instance['number'] ) : 5;
    
    if( array_key_exists( $new_instance['orderby'], $this->orderby ) ){
      $instance['orderby'] = $new_instance['orderby'];
    }

		$instance['category']   = isset( $new_instance['category'] ) ? absint( $new_instance['category'] ) : 0;
		$instance['tags']       = strip_tags( $new_instance['tags'] );
		$instance['show_date']  = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
    return $instance;
	}
	
	/**
	 * Display the form for this widget on the Widgets page of the Admin area.
	 *
	 * @param array $instance
	 */
	function form( $instance ) {
    $instance = wp_parse_args( (array) $instance, array( 
      'title'      => '', 
      'number'     => 5, 
      'orderby'    => 'date',
      'category'   => 0,
      'tags'       => '',
      'show_date'  => true
    ) );
	?>
		
    <p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title:', 'nord-theme' ); ?></label>
    <input id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>"></p>

    <p><label for="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>"><?php esc_html_e( 'Number of posts to show:', 'nord-theme' ); ?></label>
    <input id="<?php echo esc_attr( $this->get_field_id( 'number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'number' ) ); ?>" type="text" value="<?php echo absint( $instance['number'] ); ?>" size="3"></p>

    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>"><?php esc_html_e( 'Order by:', 'nord-theme' ); ?></label>
      <select id="<?php echo esc_attr( $this->get_field_id( 'orderby' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'orderby' ) ); ?>">
        <?php foreach ( $this->orderby as $slug => $name ) : ?>
        <option value="<?php echo esc_attr( $slug ); ?>"<?php selected( $instance['orderby'], $slug ); ?>><?php echo esc_attr( $name ); ?></option>
        <?php endforeach; ?>
      </select>
    </p>
    
    <p>
      <label for="<?php echo esc_attr( $this->get_field_id( 'category' ) ); ?>"><?php esc_html_e( 'Category:', 'nord-theme' ); ?></label>
      <?php
        wp_dropdown_categories( array(
          'show_option_all' => esc_html__( 'All Categories', 'nord-theme' ),
          'selected'        => absint( $instance['category'] ),
          'name'            => esc_attr( $this->get_field_name( 'category' ) ),
          'id'              => esc_attr( $this->get_field_id( 'category' ) ),
          'class'           => 'widefat'
        ) );
      ?>
    </p>
    
    <p><label for="<?php echo esc_attr( $this->get_field_id( 'tags' ) ); ?>"><?php esc_html_e( 'Tags (separated by commas):', 'nord-theme' ); ?></label>
    <input id="<?php echo esc_attr( $this->get_field_id( 'tags' ) ); ?>" class="widefat" name="<?php echo esc_attr( $this->get_field_name( 'tags' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['tags'] ); ?>"></p>
    
    <p><input class="checkbox" type="checkbox" <?php checked( $instance['show_date'] ); ?> id="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show_date' ) ); ?>" />
		<label for="<?php echo esc_attr( $this->get_field_id( 'show_date' ) ); ?>"><?php esc_html_e( 'Display post date?', 'nord-theme' ); ?></label></p>
		
  <?php
	}
}
?>