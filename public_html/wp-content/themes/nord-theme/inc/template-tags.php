<?php
/**
 * Custom template tags for Nord
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @since Nord 1.0
 */

if ( ! function_exists( '_wp_render_title_tag' ) ) :
  /**
   * Add wp_title on head if theme not supported "title-tag" feature.
   *
   * @since Nord 1.0
   */
  function nord_render_title() {
  ?>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php
  }

  add_action( 'wp_head', 'nord_render_title' );
endif;

if ( ! function_exists( 'nord_site_identity' ) ) :
  /**
   * Prints HTML with blog title and description.
   *
   * @since Nord 1.0
   */
  function nord_site_identity() {
    if( get_header_image() ) {
      printf( '<div class="header-image %s"><a href="%s" rel="home"><img src="%s" height="%d" width="%d" alt="%s" /></a></div>',
        get_theme_mod( 'header_image_rounded' ) ? 'rounded' : '',
        esc_url( home_url( '/' ) ),
        esc_url( get_header_image() ),
        absint( get_custom_header()->height ),
        absint( get_custom_header()->width ),
        esc_attr( get_bloginfo( 'title' ) )
      );
    }
    
    if( get_theme_mod( 'display_site_title', 1 ) ) {
      printf( '<%1$s class="site-title"><a href="%2$s" rel="home">%3$s</a></%1$s>', 
        is_front_page() && is_home() ? 'h1' : 'p', 
        esc_url( home_url( '/' ) ), 
        esc_html( get_bloginfo( 'name' ) ) 
      );
    }

    nord_tagline( '<div class="tagline">', '</div>' );
  }
endif;

if ( ! function_exists( 'nord_tagline' ) ) :
  /**
   * Prints HTML with custom header text.
   *
   * @since Nord 1.0
   */
  function nord_tagline( $before = '', $after = '' ) {
    $header_text = get_transient( 'nord_tagline' );
    
    if( $header_text === false || is_customize_preview() ) {
      $header_text = get_theme_mod( 'tagline' );
      
      if( $header_text ) {
        $header_text = wp_kses( $header_text, array(
            'a' => array(
              'href' => array(),
              'title' => array()
            ),
            'p'      => array(),
            'b'      => array(),
            'strong' => array(),
            'em'     => array(),
            'i'      => array(),
            'br'     => array(),
            'span'   => array(),
            'img' => array(
              'src' => array(),
              'alt' => array(),
              'title' => array()
            )
          )
        );
      }
      set_transient( 'nord_tagline', $header_text );
    }
    
    printf( '%s%s%s', $before, $header_text, $after );
  }
endif;

if ( ! function_exists( 'nord_site_copyright' ) ) :
  /**
   * Prints HTML with custom or default copyright text.
   *
   * @since Nord 1.0
   */
  function nord_site_copyright( $before = '', $after = '' ) {
    $default_copyright = sprintf( '&copy; %1$d <a href="%2$s">%3$s</a>', date('Y'), esc_url( home_url( '/' ) ), esc_html( get_bloginfo( 'name' ) ) );
    
    $copyright = get_transient( 'nord_copyright' );
    
    if( $copyright === false || is_customize_preview() ) {
      $copyright = get_theme_mod( 'copyright' );
      
      if( $copyright ) {
        $copyright = wp_kses( $copyright, array(
            'a' => array(
              'href' => array(),
              'title' => array()
            ),
            'p'      => array(),
            'b'      => array(),
            'strong' => array(),
            'em'     => array(),
            'i'      => array(),
            'br'     => array(),
            'span'   => array(),
            'img' => array(
              'src' => array(),
              'alt' => array(),
              'title' => array()
            )
          )
        );
      }
      set_transient( 'nord_copyright', $copyright );
    }
    
    printf( '%s%s%s', $before, $copyright ? $copyright : $default_copyright, $after );
  }
endif;

if ( ! function_exists( 'nord_social_profiles' ) ) :
  /**
   * Prints HTML with social profiles buttons.
   *
   * @since Nord 1.0
   */
  function nord_social_profiles( $before = '', $after = '' ) {
    $social_profiles = get_theme_mod( 'social' );
    
    if( empty( $social_profiles ) )
      return;
    
    $output = '';
    
    if( is_array( $social_profiles ) ) {
      foreach( $social_profiles as $network_id => $url ) {
        if( $url )
          $output .= sprintf( 
            '<li><a href="%1$s" class="%2$s"><i class="entypo-%2$s"></i></a></li>', 
            ( $network_id == 'email' ) ? 'mailto:' . esc_attr( $url ) : esc_url( $url ), 
            esc_attr( $network_id ) 
         );
      }
    }
    
    if( $output )
      printf( '%s<ul class="social-profiles">%s</ul>%s', $before, $output, $after );
  }
endif;

if ( ! function_exists( 'nord_post_thumbnail' ) ) :
  /**
   * Display an optional post thumbnail.
   *
   * Wraps the post thumbnail in an anchor element on index views, or a div
   * element when on single views.
   *
   * @since Nord 1.0
   */
  function nord_post_thumbnail() {
    if ( post_password_required() || is_attachment() || ! has_post_thumbnail()  ) {
      return;
    }

    if ( is_singular() ) :
    ?>

    <div class="post-thumbnail">
      <?php the_post_thumbnail( 'nord-post-thumbnail-crop' ); ?>
    </div><!-- .post-thumbnail -->

    <?php else : ?>

    <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
      <?php the_post_thumbnail( 'nord-post-thumbnail-crop', array( 'alt' => get_the_title() ) ); ?>
    </a>

    <?php endif; // End is_singular()
  }
endif;

if ( ! function_exists( 'nord_entry_meta' ) ) :
  /**
   * Prints HTML with meta information for the categories, post date.
   *
   * @since Nord 1.0
   */
  function nord_entry_meta( $before = '', $after = '' ) {
    $output = '';
    $post_type = get_post_type();

    if ( in_array( $post_type, array( 'post', 'attachment' ) ) && get_theme_mod( 'display_date', 1 ) ) {
      $time_string = sprintf( '<time class="entry-date published" datetime="%1$s">%2$s</time>',
        esc_attr( get_the_date( 'c' ) ),
        get_the_date()
      );

      $output .= sprintf( '<span class="posted-on post-meta">%s %s</span>', esc_html__( 'Posted on', 'nord-theme' ), $time_string );
    }

    if ( 'post' == $post_type && get_theme_mod( 'display_author', 1 ) ) {
      $output .= sprintf( '<span class="byline post-meta"><span class="author vcard">%1$s <a class="url fn n" href="%2$s">%3$s</a></span></span>',
        esc_html__( 'by', 'nord-theme' ),
        esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
        get_the_author()
      );
    }

    if( $output )
      printf( '%s%s%s', $before, $output, $after );
  }
endif;

if ( ! function_exists( 'nord_category_list' ) ) :
  /**
   * Prints HTML with post categories list.
   *
   * @since Nord 1.0
   */
  function nord_category_list() {
    $post_type = get_post_type();
    
    if ( 'post' == $post_type && get_theme_mod( 'display_categories', 1 ) ) {
      $categories = get_the_category();
      $output = '';
    
      if( $categories ) {
        foreach( $categories as $category ) {
          $output .= sprintf( '<a href="%s" class="category-%d">%s</a>', esc_url( get_category_link( $category->term_id ) ), $category->term_id, esc_attr( $category->cat_name ) );
        }
      }
    
      if( $output ) {
        printf( '<div class="cat-links">%s</div>', $output );
      }  
    }
  }
endif;

if ( ! function_exists( 'nord_entry_footer' ) ) :
  /**
   * Prints HTML with post tags and share buttons in post footer
   *
   * @since Nord 1.0
   */
  function nord_entry_footer( $before = '', $after = '' ) {
    $output = '';
    $post = get_post();
    
    if ( is_singular( 'post' ) && get_theme_mod( 'display_tags_list', 1 ) ) {
      $tags_list = get_the_tag_list( '<div class="tags-list">',' ','</div>' );
    
      if ( $tags_list  ) 
        $output .= $tags_list;
    }

    if( is_singular() && get_theme_mod( 'display_share_buttons', 1 ) ) {
      $output .= sprintf( '<div class="entry-share">%s</div>', nord_get_share_buttons() );
    }
    
    if( $output )
      printf( '%s%s%s', $before, $output, $after );
  }
endif;

/**
 * Get HTML with post share buttons.
 *
 * @since Nord 1.0
 */
function nord_get_share_buttons() {
  $post = get_post();

  if( !$post )
    return;

  $thumbnail_src = '';
  $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );
  
  if( $thumbnail )
    $thumbnail_src = urlencode( esc_url( $thumbnail['0'] ) );

  $post_permalink = urlencode( esc_url( get_permalink( $post->ID ) ) );
  $title          = urlencode( strip_tags( get_the_title() ) );
  $excerpt        = urlencode( strip_tags( get_the_excerpt() ) );
  
  $buttons = array(
    'facebook' => array(
      'text'   => esc_html__( 'Share', 'nord-theme' ),
      'url'    => 'http://www.facebook.com/sharer/sharer.php?u=' . $post_permalink,
      'title'  => esc_html__( 'Share on Facebook', 'nord-theme' ),
      'icon'   => '<i class="entypo-facebook"></i>',
      'show'   => 1
     ),
    'twitter' => array(
      'text'   => esc_html__( 'Tweet', 'nord-theme' ),
      'url'    => 'https://twitter.com/intent/tweet?text=' . $title . '&amp;url=' . $post_permalink,
      'title'  => esc_html__( 'Tweet It', 'nord-theme' ),
      'icon'   => '<i class="entypo-twitter"></i>',
      'show'   => 1
     ),
    'pinterest' => array(
      'text'   => esc_html__( 'Pin It', 'nord-theme' ),
      'url'    => 'https://www.pinterest.com/pin/create/button/?description=' . $title . '&amp;media=' . $thumbnail_src . '&amp;url=' . $post_permalink,
      'title'  => esc_html__( 'Pin It', 'nord-theme' ),
      'icon'   => '<i class="entypo-pinterest"></i>',
      'show'   => 1
     ),
    'tumblr' => array(
      'text'   => esc_html__( 'Share', 'nord-theme' ),
      'url'    => 'http://www.tumblr.com/share/link?url=' . $post_permalink . '&amp;name=' . $title . '&amp;description=' . $excerpt,
      'title'  => esc_html__( 'Post on Tumblr', 'nord-theme' ),
      'icon'   => '<i class="entypo-tumblr"></i>',
      'show'   => 1
     ),
    'google' => array(
      'text'   => esc_html__( 'Share', 'nord-theme' ),
      'url'    => 'https://plus.google.com/share?url=' . $post_permalink,
      'title'  => esc_html__( 'Share on Google Plus', 'nord-theme' ),
      'icon'   => '<i class="entypo-googleplus"></i>',
      'show'   => 0
     ),
    'linkedin' => array(
      'text'   => esc_html__( 'Share', 'nord-theme' ),
      'url'    => 'https://www.linkedin.com/shareArticle?url=' . $post_permalink . '&amp;title=' . $title . '&amp;summary=' . $excerpt,
      'title'  => esc_html__( 'Share on LinkedIn', 'nord-theme' ),
      'icon'   => '<i class="entypo-linkedin"></i>',
      'show'   => 0
     )
  );
  
  $output = '';

  foreach( $buttons as $button_id => $button_data ) {
    if( get_theme_mod( 'display_' . $button_id . '_share_button', $button_data['show'] ) ) {
      $output .= sprintf( '<a href="%s" title="%s" class="nord-share-button nord-%s-share-button" rel="nofollow">%s</a>', esc_url( $button_data['url'] ), esc_attr( $button_data['title'] ), esc_attr( $button_id ), $button_data['icon'] );
    }
  } 
  
  if( $output )
    return sprintf( '<div class="share-buttons">%s</div>', $output );
}

if ( ! function_exists( 'nord_share_buttons' ) ) :
  /**
   * Prints HTML with post share buttons.
   *
   * @since Nord 1.0
   */
  function nord_share_buttons( $before = '', $after = '' ) {
    printf( '%s%s%s', $before, nord_get_share_buttons(), $after );
  }
endif;
?>