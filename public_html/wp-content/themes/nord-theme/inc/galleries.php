<?php
/**
 * The addition of two new types of galleries: the carousel and a wide list of images.
 *
 * @since Nord 1.1
 */
 
class Nord_Galleries {

  protected $gallery_defaults;
  protected $gallery_types;

  public function __construct() {
    $this->gallery_defaults = array(
      'gallery_type'  => 'default'
    );
    
    $this->gallery_types = array( 
      'default'    => __( 'Thumbnail Grid', 'nord-theme' ), 
      'carousel'   => __( 'Carousel', 'nord-theme' ),
      'wide_list'  => __( 'Wide List', 'nord-theme' )
    );
  }
  
  /*
   * Setup filters and actions
   *
   * @since Nord 1.1
   */    
  public function setup() {
    add_action( 'print_media_templates', array( $this, 'print_media_templates' ) );
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
    add_filter( 'post_gallery', array( $this, 'post_gallery' ), 10, 2 );
  }
  
  /*
   * Register and enqueue admin-specific JavaScript.
   *
   * @since Nord 1.1
   */      
  public function enqueue_admin_scripts() { 
    $screen = get_current_screen();
    
    if ( $screen->base == 'post' ) {
      wp_enqueue_script( 'nord-galleries', get_template_directory_uri() . '/js/admin/gallery-settings.js', array( 'jquery' ), '1.1' );
      
      $defaults = $this->gallery_defaults;
      
      wp_localize_script( 
        'nord-galleries', 
        'nordGalleriesDefaults', 
        array( 
          'galleryType'  => $defaults['gallery_type']
         )
      );   
    }
  }
  
  /**
	 * Outputs a view template which can be used with wp.media.template
	 *
	 * @Source Jetpack Gallery
	 */
	function print_media_templates() {
		?>
		<script type="text/html" id="tmpl-nord-gallery-settings">
			<label class="setting">
				<span><?php _e( 'Gallery Type', 'nord-theme' ); ?></span>
				<select class="type" name="gallery_type" data-setting="gallery_type">
					<?php foreach ( $this->gallery_types as $value => $caption ) : ?>
						<option value="<?php echo esc_attr( $value ); ?>" <?php selected( $value, $this->gallery_defaults['gallery_type'] ); ?>><?php echo esc_html( $caption ); ?></option>
					<?php endforeach; ?>
				</select>
			</label>
		</script>
		<?php
	}
	
  /*
   * Gallery shortcode output
   *
   * @since Nord 1.1
   */    	
	function post_gallery( $output, $attr ) {
    $post = get_post();

    static $instance = 0;
    $instance++;
    
    if( isset( $attr['gallery_type'] ) && ( $attr['gallery_type'] == 'carousel' || $attr['gallery_type'] == 'wide_list' ) ) {
      
      $html5 = current_theme_supports( 'html5', 'gallery' );
      $atts = shortcode_atts( array(
        'order'         => 'ASC',
        'orderby'       => 'menu_order ID',
        'id'            => $post ? $post->ID : 0,
        'itemtag'       => $html5 ? 'figure'     : 'div',
        'icontag'       => $html5 ? 'div'        : 'div',
        'captiontag'    => $html5 ? 'figcaption' : 'div',
        'columns'       => 1,
        'size'          => 'thumbnail',
        'include'       => '',
        'exclude'       => '',
        'link'          => ''
      ), $attr, 'gallery' );

      $gallery_type = $attr['gallery_type'];
      $id = intval( $atts['id'] );
      
      if ( ! empty( $atts['include'] ) ) {
        $_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
          $attachments[$val->ID] = $_attachments[$key];
        }
      } elseif ( ! empty( $atts['exclude'] ) ) {
        $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
      } else {
        $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
      }

      if ( empty( $attachments ) ) {
        return '';
      }

      if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment ) {
          $output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
        }
        return $output;
      }
      
      $itemtag = tag_escape( $atts['itemtag'] );
      $captiontag = tag_escape( $atts['captiontag'] );
      $icontag = tag_escape( $atts['icontag'] );
      $valid_tags = wp_kses_allowed_html( 'post' );
      if ( ! isset( $valid_tags[ $itemtag ] ) ) {
        $itemtag = 'div';
      }
      if ( ! isset( $valid_tags[ $captiontag ] ) ) {
        $captiontag = 'div';
      }
      if ( ! isset( $valid_tags[ $icontag ] ) ) {
        $icontag = 'div';
      }

      $selector = "gallery-{$instance}";
      
      $size_class = sanitize_html_class( $atts['size'] );
      
      $output .= "<div id='$selector' class='custom-theme-gallery galleryid-{$id} gallery-size-{$size_class}'>";
      
      if( $gallery_type == 'carousel' ) {
        wp_enqueue_script( 'nord-owl-carousel' );
        wp_enqueue_script( 'nord-owl-init' );
        
        wp_enqueue_style( 'nord-owl-carousel-css' );
        wp_enqueue_style( 'nord-owl-theme' );
        
        $output .= "<div class='post-gallery-carousel owl-carousel'>";
      }
      
      foreach ( $attachments as $id => $attachment ) {
        $attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
        if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
          $image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
        } elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
          $image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
        } else {
          $image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
        }

        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= "
          <{$icontag} class='gallery-icon'>
            $image_output
          </{$icontag}>";
        if ( $captiontag && trim($attachment->post_excerpt) ) {
          $output .= "
            <{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
            " . wptexturize($attachment->post_excerpt) . "
            </{$captiontag}>";
        }
        $output .= "</{$itemtag}>";
      }
      
      if( $gallery_type == 'carousel' )
        $output .= "</div>\n";
        
      $output .= "</div>\n";
    }
    return $output;
	}
}

$nord_galleries = new Nord_Galleries();

add_action( 'after_setup_theme', array( $nord_galleries, 'setup' ), 16 );
?>