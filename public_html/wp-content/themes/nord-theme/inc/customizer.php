<?php
/**
 * Nord Customizer support
 *
 * @package WordPress
 * @since Nord 1.0
 */
 
/**
 * Implement Customizer additions and adjustments.
 *
 * @since Nord 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function nord_customize_register( $wp_customize ) {
  /*
   * Modify default Wordpress sections and controls
   */
  $wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
  $wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
	$wp_customize->get_control( 'blogdescription' )->label = esc_html__( 'Site Description', 'nord-theme' );
	$wp_customize->get_control( 'background_color' )->label = esc_html__( 'Outer Background Color', 'nord-theme' );
	$wp_customize->get_section( 'header_image' )->title = esc_html__( 'Logo Image', 'nord-theme' );
	$wp_customize->remove_control('display_header_text');
  $wp_customize->remove_control('header_textcolor');
  $wp_customize->remove_section('background_image');
  /*
   * Site Title & Tagline
   */   
  $wp_customize->add_setting( 'display_site_title',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize'
    )
  );
  
  $wp_customize->add_control( 'display_site_title', array(
    'type'     => 'checkbox',
    'priority' => 10,
    'label'    => esc_html__( 'Display Site Title', 'nord-theme' ),
    'section'  => 'title_tagline',
  ) );
  
  $wp_customize->add_setting(
    'tagline',
    array(
      'default' => '',
      'sanitize_callback' => 'nord_textarea_sanitize',
      'transport' => 'postMessage'
    )
  );
 
  $wp_customize->add_control(
      'tagline',
      array(
        'type'        => 'textarea',
        'label'       => esc_html__( 'Tagline', 'nord-theme' ),
        'description' => esc_html__( 'Allowed HTML tags: p, a, strong, b, em, i, br, span, img', 'nord-theme' ),
        'section'     => 'title_tagline',
      )
  );
  
  /*
   * Header Image
   */
  $wp_customize->add_setting( 'header_image_max_width',
    array(
      'default' => 150,
      'sanitize_callback' => 'absint'
    )
  );
  
  $wp_customize->add_control( 'header_image_max_width', array(
    'type'     => 'text',
    'priority' => 20,
    'section'  => 'header_image',
    'label'    => esc_html__( 'Max width (px)', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'header_image_margin_bottom',
    array(
      'default' => 0,
      'sanitize_callback' => 'absint'
    )
  );
  
  $wp_customize->add_control( 'header_image_margin_bottom', array(
    'type'     => 'text',
    'priority' => 30,
    'section'  => 'header_image',
    'label'    => esc_html__( 'Margin bottom (px)', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'header_image_rounded',
    array(
      'default' => 0,
      'sanitize_callback' => 'nord_checkbox_sanitize'
    )
  );
  
  $wp_customize->add_control( 'header_image_rounded', array(
    'type'     => 'checkbox',
    'priority' => 40,
    'section'  => 'header_image',
    'label'    => esc_html__( 'Rounded (recomended for square images)', 'nord-theme' ),
  ) );

  /*
   * Colors
   */
  $wp_customize->add_setting( 'outer_link_color',
    array(
      'default' => '#ffffff',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
    new WP_Customize_Color_Control(
      $wp_customize,
      'outer_link_color',
      array(
          'label' => esc_html__( 'Outer Links Color', 'nord-theme' ),
          'description' => esc_html__( 'Site title & social Profiles', 'nord-theme' ),
          'section' => 'colors',
          'settings' => 'outer_link_color',
      )
    )
  ); 
  
  $wp_customize->add_setting( 'outer_text_color',
    array(
      'default' => '#dddddd',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
    new WP_Customize_Color_Control(
      $wp_customize,
      'outer_text_color',
      array(
          'label' => esc_html__( 'Outer Text Color', 'nord-theme' ),
          'description' => esc_html__( 'Tagline & copyright', 'nord-theme' ),
          'section' => 'colors',
          'settings' => 'outer_text_color',
      )
    )
  ); 
  
  $wp_customize->add_setting( 'link_color',
    array(
      'default' => '#57ad68',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'link_color',
          array(
              'label' => esc_html__( 'Link color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'link_color',
          )
      )
  ); 

  $wp_customize->add_setting( 'link_color_hover',
    array(
      'default' => '#468c54',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );

  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'link_color_hover',
          array(
              'label' => esc_html__( 'Link color on hover', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'link_color_hover',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'category_label_color',
    array(
      'default' => '#72b4c3',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );

  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'category_label_color',
          array(
              'label' => esc_html__( 'Category label color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'category_label_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'button_background_color',
    array(
      'default' => '#57ad68',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'button_background_color',
          array(
              'label' => esc_html__( 'Button Background Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'button_background_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'button_background_color_hover',
    array(
      'default' => '#468c54',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'button_background_color_hover',
          array(
              'label' => esc_html__( 'Button Background Color on Hover', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'button_background_color_hover',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'button_text_color',
    array(
      'default' => '#ffffff',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'button_text_color',
          array(
              'label' => esc_html__( 'Button Text Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'button_text_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'sidebar_button_color',
    array(
      'default' => '#292929',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'sidebar_button_color',
          array(
              'label' => esc_html__( 'Sidebar Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'sidebar_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'facebook_button_color',
    array(
      'default' => '#3b5998',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'facebook_button_color',
          array(
              'label' => esc_html__( 'Facebook Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'facebook_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'twitter_button_color',
    array(
      'default' => '#00aced',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'twitter_button_color',
          array(
              'label' => esc_html__( 'Twitter Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'twitter_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'pinterest_button_color',
    array(
      'default' => '#cb2027',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'pinterest_button_color',
          array(
              'label' => esc_html__( 'Pinterest Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'pinterest_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'tumblr_button_color',
    array(
      'default' => '#35465c',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'tumblr_button_color',
          array(
              'label' => esc_html__( 'Tumblr Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'tumblr_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'google_button_color',
    array(
      'default' => '#dd4b39',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'google_button_color',
          array(
              'label' => esc_html__( 'Google Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'google_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting( 'linkedin_button_color',
    array(
      'default' => '#0077b5',
      'sanitize_callback' => 'sanitize_hex_color',
      'transport' => 'postMessage'
    )
  );
  
  $wp_customize->add_control(
      new WP_Customize_Color_Control(
          $wp_customize,
          'linkedin_button_color',
          array(
              'label' => esc_html__( 'LinkedIn Button Color', 'nord-theme' ),
              'section' => 'colors',
              'settings' => 'linkedin_button_color',
          )
      )
  ); 
  
  $wp_customize->add_setting(
    'navigation_color_scheme',
    array(
      'default' => 'dark',
      'sanitize_callback' => 'nord_navigation_color_sanitize',
    )
  );
  
  $wp_customize->add_control(
      'navigation_color_scheme',
      array(
        'type'    => 'select',
        'label'   => esc_html__( 'Navigation', 'nord-theme' ),
        'section' => 'colors',
        'choices' => array(
          'dark'  => esc_html__( 'Dark', 'nord-theme' ),
          'light' => esc_html__( 'Light', 'nord-theme' )
        )
      )
  );
  
  /*
   * Social Profiles
   */ 
  $wp_customize->add_section( 'social',
    array(
      'title' => esc_html__( 'Social Profiles', 'nord-theme' )
    )
  );

  $wp_customize->add_setting( 'social[facebook]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[facebook]', array(
    'type'     => 'url',
    'priority' => 10,
    'section'  => 'social',
    'label'    => esc_html__( 'Facebook', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[twitter]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[twitter]', array(
    'type'     => 'url',
    'priority' => 15,
    'section'  => 'social',
    'label'    => esc_html__( 'Twitter', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[googleplus]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[googleplus]', array(
    'type'     => 'url',
    'priority' => 20,
    'section'  => 'social',
    'label'    => esc_html__( 'Google Plus', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[bloglovin]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[bloglovin]', array(
    'type'     => 'url',
    'priority' => 20,
    'section'  => 'social',
    'label'    => esc_html__( 'Bloglovin', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[instagram]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[instagram]', array(
    'type'     => 'url',
    'priority' => 25,
    'section'  => 'social',
    'label'    => esc_html__( 'Instagram', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[pinterest]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[pinterest]', array(
    'type'     => 'url',
    'priority' => 30,
    'section'  => 'social',
    'label'    => esc_html__( 'Pinterest', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[linkedin]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[linkedin]', array(
    'type'     => 'url',
    'priority' => 35,
    'section'  => 'social',
    'label'    => esc_html__( 'LinkedIn', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[flickr]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[flickr]', array(
    'type'     => 'url',
    'priority' => 40,
    'section'  => 'social',
    'label'    => esc_html__( 'Flickr', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[medium]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[medium]', array(
    'type'     => 'url',
    'priority' => 45,
    'section'  => 'social',
    'label'    => esc_html__( 'Medium', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[vkontakte]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[vkontakte]', array(
    'type'     => 'url',
    'priority' => 50,
    'section'  => 'social',
    'label'    => esc_html__( 'Vkontakte', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[f500px]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[f500px]', array(
    'type'     => 'url',
    'priority' => 55,
    'section'  => 'social',
    'label'    => esc_html__( '500px', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[tumblr]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[tumblr]', array(
    'type'     => 'url',
    'priority' => 60,
    'section'  => 'social',
    'label'    => esc_html__( 'Tumblr', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[dribbble]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[dribbble]', array(
    'type'     => 'url',
    'priority' => 65,
    'section'  => 'social',
    'label'    => esc_html__( 'Dribbble', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[behance]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[behance]', array(
    'type'     => 'url',
    'priority' => 70,
    'section'  => 'social',
    'label'    => esc_html__( 'Behance', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[youtube]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[youtube]', array(
    'type'     => 'url',
    'priority' => 75,
    'section'  => 'social',
    'label'    => esc_html__( 'YouTube', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[vimeo]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[vimeo]', array(
    'type'     => 'url',
    'priority' => 80,
    'section'  => 'social',
    'label'    => esc_html__( 'Vimeo', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[github]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[github]', array(
    'type'     => 'url',
    'priority' => 85,
    'section'  => 'social',
    'label'    => esc_html__( 'Github', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[soundcloud]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[soundcloud]', array(
    'type'     => 'url',
    'priority' => 90,
    'section'  => 'social',
    'label'    => esc_html__( 'SoundCloud', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[rss]',
    array(
      'default' => '',
      'sanitize_callback' => 'esc_url_raw'
    )
  );
  
  $wp_customize->add_control( 'social[rss]', array(
    'type'     => 'url',
    'priority' => 95,
    'section'  => 'social',
    'label'    => esc_html__( 'Rss', 'nord-theme' ),
  ) );
  
  $wp_customize->add_setting( 'social[email]',
    array(
      'default' => '',
      'sanitize_callback' => 'nord_email_sanitize'
    )
  );
  
  $wp_customize->add_control( 'social[email]', array(
    'type'     => 'url',
    'priority' => 100,
    'section'  => 'social',
    'label'    => esc_html__( 'Email', 'nord-theme' ),
  ) );
  
  /*
   * Titles & Headings
   */  
  $wp_customize->add_section( 'titles_and_headings',
    array(
      'title' => esc_html__( 'Titles & Headings', 'nord-theme' )
    )
  );
  
  $wp_customize->add_setting( 'sticky_post_title',
    array(
      'default' => '',
      'sanitize_callback' => 'sanitize_text_field'
    )
  );
  
  $wp_customize->add_control( 'sticky_post_title', array(
    'type'        => 'text',
    'priority'    => 0,
    'label'       => esc_html__( 'Sticky post badge text', 'nord-theme' ),
    'section'     => 'titles_and_headings',
    'description' => esc_html__( 'Default: &laquo;Featured&raquo;', 'nord-theme' )
   ) );
   
  
  $wp_customize->add_setting( 'more_link_title',
    array(
      'default' => '',
      'sanitize_callback' => 'sanitize_text_field'
    )
  );
  
  $wp_customize->add_control( 'more_link_title', array(
    'type'        => 'text',
    'priority'    => 0,
    'label'       => esc_html__( '"Read More" button text', 'nord-theme' ),
    'section'     => 'titles_and_headings',
    'description' => esc_html__( 'Default: &laquo;Read More&raquo;', 'nord-theme' )
   ) );
   
  /*
   * Post Options
   */
  $wp_customize->add_section( 'post_options',
    array(
      'title' => esc_html__( 'Post Options', 'nord-theme' )
    )
  ); 
  
  $wp_customize->add_setting( 'display_categories',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_categories', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display categories', 'nord-theme' ),
    'section'  => 'post_options',
  ) );
  
  $wp_customize->add_setting( 'display_date',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_date', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display post date', 'nord-theme' ),
    'section'  => 'post_options',
  ) );
  
  $wp_customize->add_setting( 'display_tags_list',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_tags_list', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display tags list', 'nord-theme' ),
    'section'  => 'post_options',
  ) );
  
  $wp_customize->add_setting( 'display_share_buttons',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_share_buttons', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display share buttons', 'nord-theme' ),
    'section'  => 'post_options',
  ) );
  
  $wp_customize->add_setting( 'display_author',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_author', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display author', 'nord-theme' ),
    'section'  => 'post_options',
  ) );
  
  $wp_customize->add_setting( 'display_author_box',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_author_box', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display author box', 'nord-theme' ),
    'section'  => 'post_options',
  ) );
  
  /*
   * Page Options
   */
  $wp_customize->add_section( 'page_options',
    array(
      'title' => esc_html__( 'Page Options', 'nord-theme' )
    )
  ); 
  
  $wp_customize->add_setting( 'display_page_share_buttons',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_page_share_buttons', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display share buttons', 'nord-theme' ),
    'section'  => 'page_options',
  ) );
  
  /*
   * Page Options
   */
  $wp_customize->add_section( 'page_options',
    array(
      'title' => esc_html__( 'Page Options', 'nord-theme' )
    )
  ); 
  
  $wp_customize->add_setting( 'display_page_share_buttons',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_page_share_buttons', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Display share buttons', 'nord-theme' ),
    'section'  => 'page_options',
  ) );
  
  /*
   * Share Buttons
   */
  $wp_customize->add_section( 'share_buttons',
    array(
      'title' => esc_html__( 'Share Buttons', 'nord-theme' )
    )
  ); 
  
  $wp_customize->add_setting( 'display_facebook_share_button',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_facebook_share_button', array(
    'type'     => 'checkbox',
    'priority' => 0,
    'label'    => esc_html__( 'Facebook', 'nord-theme' ),
    'section'  => 'share_buttons',
  ) );
  
  $wp_customize->add_setting( 'display_twitter_share_button',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_twitter_share_button', array(
    'type'     => 'checkbox',
    'priority' => 5,
    'label'    => esc_html__( 'Twitter', 'nord-theme' ),
    'section'  => 'share_buttons',
  ) );
  
  $wp_customize->add_setting( 'display_google_share_button',
    array(
      'default' => 0,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_google_share_button', array(
    'type'     => 'checkbox',
    'priority' => 10,
    'label'    => esc_html__( 'Google Plus', 'nord-theme' ),
    'section'  => 'share_buttons',
  ) );
  
  $wp_customize->add_setting( 'display_pinterest_share_button',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_pinterest_share_button', array(
    'type'     => 'checkbox',
    'priority' => 15,
    'label'    => esc_html__( 'Pinterest', 'nord-theme' ),
    'section'  => 'share_buttons',
  ) );
  
  $wp_customize->add_setting( 'display_tumblr_share_button',
    array(
      'default' => 1,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_tumblr_share_button', array(
    'type'     => 'checkbox',
    'priority' => 15,
    'label'    => esc_html__( 'Tumblr', 'nord-theme' ),
    'section'  => 'share_buttons',
  ) );
  
  $wp_customize->add_setting( 'display_linkedin_share_button',
    array(
      'default' => 0,
      'sanitize_callback' => 'nord_checkbox_sanitize',
    )
  );
  
  $wp_customize->add_control( 'display_linkedin_share_button', array(
    'type'     => 'checkbox',
    'priority' => 20,
    'label'    => esc_html__( 'LinkedIn', 'nord-theme' ),
    'section'  => 'share_buttons',
  ) );
  
  /*
   * Footer
   */  
  $wp_customize->add_section( 'footer',
    array(
      'title' => esc_html__( 'Footer', 'nord-theme' )
    )
  ); 

  $wp_customize->add_setting(
    'copyright',
    array(
      'default' => '',
      'sanitize_callback' => 'nord_textarea_sanitize',
    )
  );
 
  $wp_customize->add_control(
      'copyright',
      array(
        'type'    => 'textarea',
        'label'   => esc_html__( 'Copyright', 'nord-theme' ),
        'description' => esc_html__( 'Allowed HTML tags: p, a, strong, b, em, i, br, span, img', 'nord-theme' ),
        'section' => 'footer',
      )
  );
  
  /*
   * Custom CSS
   */
  $wp_customize->add_section( 'custom_css',
    array(
      'title' => esc_html__( 'Custom CSS', 'nord-theme' )
    )
  ); 
  
  $wp_customize->add_setting(
		'general_css',
		array(
			'default'           => '',
			'sanitize_callback' => 'wp_filter_nohtml_kses'
		)
	);
	
	$wp_customize->add_control(
		'general_css',
		array(
			'section'  => 'custom_css',
			'label'    => esc_html__( 'General', 'nord-theme' ),
			'type'     => 'textarea'
		)
	);
	
  /*
   * Other Options
   */    
  $wp_customize->add_section( 'other_options',
    array(
      'title' => esc_html__( 'Other Options', 'nord-theme' )
    )
  ); 
}
add_action( 'customize_register', 'nord_customize_register' );

/**
 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
 *
 * @since Nord 1.0
 */
function nord_customize_preview_js() {
	wp_enqueue_script( 'nord-customize-preview', get_template_directory_uri() . '/js/admin/customize-preview.js', array( 'customize-preview' ), '201602242', true );
}
add_action( 'customize_preview_init', 'nord_customize_preview_js' );

if ( ! function_exists( 'nord_textarea_sanitize' ) ) :
  /**
   * Sanitization callback for textarea field.
   *
   * @since Nord 1.0
   * @return string Textarea value.
   */
  function nord_textarea_sanitize( $value ) {
    if ( !current_user_can('unfiltered_html') )
			$value  = stripslashes( wp_filter_post_kses( addslashes( $value ) ) ); // wp_filter_post_kses() expects slashed

    return $value;
  }
endif;

if ( ! function_exists( 'nord_checkbox_sanitize' ) ) :
  /**
   * Sanitization callback for checkbox.
   *
   * @since Nord 1.0
   */
  function nord_checkbox_sanitize( $value ) {
    if ( $value == 1 ) {
        return 1;
    } else {
        return '';
    }
  }
endif;

if ( ! function_exists( 'nord_background_size_sanitize' ) ) :
  /**
   * Sanitization callback for background size option.
   *
   * @since Nord 1.0
   */
  function nord_background_size_sanitize( $value ) {
    $background_sizes = array( 'auto', 'cover', 'contain' );
    if ( ! in_array( $value, $background_sizes ) ) {
      $value = 'cover';
    }

    return $value;
  }
endif;

if ( ! function_exists( 'nord_navigation_color_sanitize' ) ) :
  /**
   * Sanitization callback for navigation color scheme.
   *
   * @since Nord 1.0
   */
  function nord_navigation_color_sanitize( $value ) {
    $schemes = array( 'dark', 'light' );
    
    if ( in_array( $value, $schemes ) )
      return $value;

    return 'dark';
  }
endif;

if ( ! function_exists( 'nord_email_sanitize' ) ) :
  /**
   * Sanitization callback for email field.
   *
   * @since Nord 1.0
   * @return string|null Verified email adress or null.
   */
  function nord_email_sanitize( $value ) {
    return ( is_email( $value ) ) ? $value : '';
  }
endif;
?>