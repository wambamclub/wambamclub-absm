<?php
require_once(dirname(__FILE__) . "/../includes/ActiveCampaign.class.php");

function addTag($ac_url, $ac_key, $ac_email, $ac_tag = null) {
    if (!isset($ac_tag)) {
      return;
    }
    $ac = new ActiveCampaign($ac_url, $ac_key);
    $ac->track_email = $ac_email;
    $post = getPost($ac_key, $ac_email, $ac_tag); 
    return $ac->api("contact/tag/add", $post);
}

function removeTag($ac_url, $ac_key, $ac_email, $ac_tag = null) {
    if (!isset($ac_tag)) {
      return;
    }
    $ac = new ActiveCampaign($ac_url, $ac_key);
    $ac->track_email = $ac_email;
    $post = getPost($ac_key, $ac_email, $ac_tag); 
    return $ac->api("contact/tag/remove", $post); 
}

function trackEvent($ac_url, $ac_key, $ac_email, $ac_event_id, $ac_event_key, $event_id, $event_value) {
    $ac = new ActiveCampaign($ac_url, $ac_key);
    $ac->track_actid = $ac_event_id;
    $ac->track_key = $ac_event_key;
    $ac->track_email = $ac_email;
    $post = array(
        "event" => $event_id,
        "eventdata" => $event_value);
    return $ac->api("tracking/log", $post); 
}

function getPost($ac_key, $ac_email, $ac_tag) {
    return array(
      'api_key' => $ac_key,	
      'api_output' => 'xml',
      'email' => $ac_email,
      'tags' => $ac_tag);

}
?>