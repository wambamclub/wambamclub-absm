<?php

/*
Plugin Name: Absolute Responsive Images
Plugin URI: http://absolutemedia.co.uk
Description: Fully responsive image solution using picturefill and the ID of your image.
Version: 1.0.0
Author: Absolute
Author URI: http://absolutemedia.co.u
*/


// First we queue the polyfill
function absolute_get_picturefill() {
    wp_enqueue_script( 'picturefill', plugins_url( '/js/picturefill.js', __FILE__ ) );
    wp_enqueue_script( 'matchmedia', plugins_url( '/js/matchmedia.js', __FILE__ ) );
}
add_action( 'wp_enqueue_scripts', 'absolute_get_picturefill' );


// Add support for our desired image sizes - if you add to these, you may have to adjust your shortcode function
// TODO: Add UI for adjusting?
function absolute_add_image_sizes() {
    add_image_size( 'mobile-small',     384);
    add_image_size( 'mobile-medium',    512);
    add_image_size( 'mobile-large',     768);
    add_image_size( 'desktop-small',    1024);
    add_image_size( 'desktop-medium',   1536);
    add_image_size( 'desktop-large',    2048);
}
add_action( 'plugins_loaded', 'absolute_add_image_sizes' );

// alt tags will now be automatically included
function absolute_get_img_alt( $image ) {
    $img_alt = trim( strip_tags( get_post_meta( $image, '_wp_attachment_image_alt', true ) ) );
    return $img_alt;
}

function absolute_get_picture_srcs( $image, $mappings, $postid ) {

    $arr = array();
    $featured_images = array();

    // Check if the Dynamic Featured Image plugin is active.
    // If so check for mobile specific images
    // We can upload multiple featured images to a post - https://github.com/ankitpokhrel/Dynamic-Featured-Image/wiki/Retrieving-data-in-a-theme
    if( class_exists('Dynamic_Featured_Image') ) {
        global $dynamic_featured_image;
        $featured_images = $dynamic_featured_image->get_featured_images( $postid );
    }

    foreach ( $mappings as $size => $type ) {
        $image_src      = wp_get_attachment_image_src( $image, $type[0] );
        if( strpos($type[0], "mobile-") === 0 ) {
            if(count($featured_images) > 0 ) {
                $image_src      = wp_get_attachment_image_src( $featured_images[0]['attachment_id'], $type[0] );
            }
        }
        $_image_to_show = $image_src[0];
        if($_image_to_show > '') {
            $arr[] ='<span data-src="'. $_image_to_show . '" '. $type[1] .'></span>'. "\n";
        }
    }
    return implode( $arr );
}

function absolute_responsive_shortcode( $atts ) {
    extract( shortcode_atts( array(
        'postid'    => 1,
        'imageid'    => 1,
        // You can add more sizes for your shortcodes here
        'size1' => 384,
        'size2' => 512,
        'size3' => 768,
        'size4' => 1024,
        'size5' => 1536,
        'size6' => 2048,
        'size7' => 0,

    ), $atts ) );

    $mappings = array(
        $size1 => array('mobile-small', ""),   // type, media query (if any)
        $size2 => array('mobile-medium', 'data-media="(min-width: 385px)"'),
        $size3 => array('mobile-large', 'data-media="(min-width: 513px) and (max-width: 767px)"'),
        $size4 => array('desktop-small', 'data-media="(min-width: 768px)"'),
        $size5 => array('desktop-medium', 'data-media="(min-width: 1025px)"'),
        $size6 => array('desktop-large', 'data-media="(min-width: 1537px)"'),
        $size7 => array('full', 'data-media="(-webkit-min-device-pixel-ratio: 2) and (min-width: 2048px)"'),
    );

    $_alt = get_post_meta($imageid, '_wp_attachment_image_alt', true);

    $_image_html = absolute_get_picture_srcs($imageid, $mappings, $postid);
    if($_image_html > "") {
        $_image_html = '<div class="featured-image"><span data-picture data-alt="'. $_alt .'">' . "\n"
          . $_image_html .
          // this noscript tag contains our default image for old / JavaScript-less browsers, and we're using size2 as our default
          '<noscript>' . wp_get_attachment_image($imageid, $size6) . '</noscript>
        </span></div>';
    }
    return $_image_html;
}
// TODO: It this the best name? responsive_img? picture?
add_shortcode( 'responsive', 'absolute_responsive_shortcode' );

// Alter Media Uploader output to output shortcode instead
// TODO: Make optional?
// TODO: Make this know what sizes are chosen, rather than hardcoded
/*
function absolute_responsive_insert_image( $html, $id, $caption, $title, $align, $url ) {
    return "[responsive imageid='$id']";
}
add_filter( 'image_send_to_editor', 'absolute_responsive_insert_image', 10, 9 );
*/