/**
 * Handles our ActiveCampaign events
 */


jQuery(document).ready(function($) {    
});

/**
 * Fires an AJAX post. This will do an action for our AC events
 * 
 * @param {string} Any string value
 * @returns {undefined}
 */
function ac_events($event, $value) {
    jQuery.post(
        ajaxurl, 
        {
            'action': 'ac_events',
            'event':  $event,
            'data':   $value
        }, 
        function(data){
            // Do something
        }
    );
    
    return;
}

/**
 * Fires an AJAX post. This will do an action for our AC tag
 * 
 * @param {string} Any string value
 * @returns {undefined}
 */
function ac_tag($tag) {
    console.log($tag);
    jQuery.post(
        ajaxurl, 
        {
            'action': 'ac_tag',
            'tag':  $tag,
        }, 
        function(data){
            // Do something
        }
    );
    
    return;
}

