﻿# WordPress Active Campaign API plugin #

This README would normally document whatever steps are necessary to get your application up and running.

### Summary ###

This plugin integrates with Active Campaign's API which utilizes the site and event tracking.

### Requirements ###

* Site Tracking and Event Tracking are enabled
* Active Campaign Url
* API Key
* Event Key
* Event ID
* Domain being whitelisted

*All of this can be manage from your site tracking and event tracking page. Which is under integration— in the sidebar's other option.*

### Configuration ###

Once you have installed and activated the plugin. Go to Settings -> Active Campaign API. Add the API key, Event key, Event ID, and AC's api url. Click 'Save General Changes' to update.

### Usage ###
________________________________________________________

**Tracking Events**

This can be done through a JavaScript event. For example, if you want to track event when they click a link, you can do this:

```
#!html
<a href="#" onclick="ac_events('test_event', 'shots fired!');">AC Events</a>

```
The onclick event will trigger the ac_events function that would run the event. This events are registered on the current logged-in user using their email address.
________________________________________________________

**Site Events**

This event is automatically triggered when they visit any of the site pages. This event will register on the current logged-in user using their email address.
