<?php
$options = array();

$options[] = array( 'name' => __( 'General', 'activecampaign' ), 'type' => 'heading' );

$options[] = array(
	'name' => __( 'Active Campaign URL', 'activecampaign' ),
	'id'   => AC_PREFIX . 'endpoint',
	'type' => 'text',
);

$options[] = array(
	'name' => __( 'API KEY', 'activecampaign' ),
	'id'   => AC_PREFIX . 'api_key',
	'type' => 'text',
);

$options[] = array(
	'name' => __( 'Event KEY', 'activecampaign' ),
	'id'   => AC_PREFIX . 'event_key',
	'type' => 'text',
);

$options[] = array(
	'name' => __( 'Event ID', 'activecampaign' ),
	'id'   => AC_PREFIX . 'event_id',
	'type' => 'text',
);