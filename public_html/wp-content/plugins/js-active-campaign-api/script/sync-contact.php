<?php

/**
 * Script that add contacts to ActiveMember when a new MouseMembers is added to the system.
 * After they are opt-in they are registered as free member and at the same time they are listed to list 20.
 */
require_once ('../../../../wp-blog-header.php');

$acsf = new SF_Settings_API($id = 'wp_activecampaign_api', $title = 'Active Campaign API', $menu = 'options-general.php', __FILE__);
$acsf->load_options( AC_LIB_PATH . '/wp-sf-settings/sf-options.php' );


// ---- GET EVENT TYPE ----
if(!isset($_GET["event_type"])) 
{
	// event type was not found, so exit
	exit;
}
else 
{
	$eventType = $_GET["event_type"];
}


// ---- ACCESS DATA ----
// member data
$memberId = $_GET["member_id"];
$registeredDate = $_GET["registered"];
$lastLoginDate = $_GET["last_logged_in"];
$lastUpdatedDate = $_GET["last_updated"];
$daysAsMember = $_GET["days_as_member"];
$status = $_GET["status"];
$statusName = $_GET["status_name"];
$membershipLevelId = $_GET["membership_level"];
$membershipLevelName = $_GET["membership_level_name"];
$isComplimentary = $_GET["is_complimentary"];
$username = $_GET["username"];
$email = $_GET["email"];
$phone = $_GET["phone"];
$firstName = $_GET["first_name"];
$lastName = $_GET["last_name"];
$expirationDate = $_GET["expiration_date"];
$cancellationDate = $_GET["cancellation_date"];
$billingAddress = $_GET["billing_address"];
$billingCity = $_GET["billing_city"];
$billingState = $_GET["billing_state"];
$billingZipCode = $_GET["billing_zip_code"];
$billingCountry = $_GET["billing_country"];
$shippingAddress = $_GET["shipping_address"];
$shippingCity = $_GET["shipping_city"];
$shippingState = $_GET["shipping_state"];
$shippingZipCode = $_GET["shipping_zip_code"];
$shippingCountry = $_GET["shipping_country"];

// bundle data
$bundleId = $_GET["bundle_id"];
$bundleName = $_GET["bundle_name"];
$daysWithBundle = $_GET["days_with_bundle"];
$bundleStatus = $_GET["bundle_status"];
$bundleStatusName = $_GET["bundle_status_name"];
$bundleIsComplimentary = $_GET["bundle_is_complimentary"];
$bundleDateAdded = $_GET["bundle_date_added"];
$bundleLastUpdatedDate = $_GET["bundle_last_updated"];


require_once( AC_LIB_PATH .'/activecampaign-api-php/includes/ActiveCampaign.class.php' );

if($eventType == 'mm_bundles_add' && !empty($email)) {
    
    $ac = new ActiveCampaign($acsf->get_option(AC_PREFIX . 'endpoint'), $acsf->get_option(AC_PREFIX . 'api_key'));

    if (!(int)$ac->credentials_test()) {
        exit();
    }
    
    if($bundleId == 1) {
        $list_id = 22;	// have to do this with individual bundles instead of memberships
    }	else {
	exit();
    }

/* commenting out for using a bundle instead
 *
 *    if($membershipLevelId == 1) {
 *        $list_id = 19;
 *    } elseif ($membershipLevelId == 2) {
 *        $list_id = 20;
 *    }
 */

    $contact = array(
            "email" => $email,
            "first_name" => $firstName,
            "last_name" => $lastName,
            "p[{$list_id}]" => $list_id,
            "status[{$list_id}]" => 1, // "Active" status
    );

    $contact_sync = $ac->api("contact/sync", $contact);
    
}

    
?>