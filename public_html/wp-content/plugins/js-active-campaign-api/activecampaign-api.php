<?php
/**
 * Plugin Name: WP Active Campaign API
 * Plugin URI: http://campaignplugins.com/downloads/wp-activecampaign-api
 * Description: WordPress Active Campaign API plugin
 * Author: Art Layese
 * Author URI: http://campaignplugins.com/
 * Version: 1.1.1
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

if (!defined('AC_PREFIX'))
	define('AC_PREFIX', 'ac_'); // Our prefix

if (!defined('AC_TEXTDOMAIN'))
	define('AC_TEXTDOMAIN', 'active-campaign'); // Domain for translating text

/**
 * Defined Plugin url and path.
 */
if (!defined('AC_URL'))
	define('AC_URL', plugin_dir_url(__FILE__));

if (!defined('AC_PATH'))
	define('AC_PATH', untrailingslashit(plugin_dir_path(__FILE__)));
/**/

/**
 * Defined Plugin library url and path.
 * Houses all the collective framework (not authored by me) that is essential for this plugin. 
 */
if (!defined('AC_LIB_URL'))
	define('AC_LIB_URL', AC_URL . '/libraries');

if (!defined('AC_LIB_PATH'))
	define('AC_LIB_PATH', AC_PATH . '/libraries');
/**/

/**
 * Plugin JavaScript url and path
 */
if (!defined('AC_JS_URL'))
	define('AC_JS_URL', AC_URL . 'js');
/**/

require_once('classes/api-toolkit.php');

if (!class_exists('WP_Active_Campaign')) {

class WP_Active_Campaign extends API_Toolkit
{
	const VERSION = '1.1.1';
	const NAME = 'WP ActiveCampaign API';
	const AUTHOR = 'Brecht Palombo';
	const STORE_URL = 'https://campaignplugins.com';
	const LICENSE_KEY = 'FREE';

	public static $endpoint;
	public static $api_key;
	public static $acsf;
	public static $ac;

	public function __construct()
	{
		require_once(AC_LIB_PATH .'/wp-sf-settings/classes/sf-class-settings.php');
		self::$acsf = new SF_Settings_API('wp_activecampaign_api', 'Active Campaign API', 'options-general.php', __FILE__);
		self::$acsf->load_options(AC_LIB_PATH . '/wp-sf-settings/sf-options.php');

		require_once(AC_LIB_PATH .'/activecampaign-api-php/includes/ActiveCampaign.class.php');
		self::$ac = new ActiveCampaignAPI(self::$acsf->get_option(AC_PREFIX . 'endpoint'), self::$acsf->get_option (AC_PREFIX . 'api_key'));
		self::$ac->track_actid = self::$acsf->get_option(AC_PREFIX . 'event_id');
		self::$ac->track_key   = self::$acsf->get_option(AC_PREFIX . 'event_key');

		require_once AC_LIB_PATH . '/codemate/EDD_SL_Plugin_Updater.php';
		if (is_admin()) {
			new EDD_SL_Plugin_Updater(self::STORE_URL, __FILE__, array(
					'version' => self::VERSION,
					'license' => self::LICENSE_KEY,
					'item_name' => self::NAME,
					'author' => self::AUTHOR,
				)
			);
		}


		// Putting it all together...
		add_action('wp_enqueue_scripts', array($this, 'event_tracking_js_script'));
		add_action('wp_enqueue_scripts', array($this, 'site_tracking_js_script'));
		add_action('wp_ajax_ac_events', array($this, 'ajax_activecampaign_events'));
		add_action('wp_ajax_ac_tag', array($this, 'ajax_activecampaign_tag'));
		add_action('wp_ajax_nopriv_ac_events', array($this, 'ajax_activecampaign_events'));
		add_action('wp_ajax_nopriv_ac_tag', array($this, 'ajax_activecampaign_tag'));
		add_filter('query_vars', array($this, 'add_email_to_query'));

		// You can't call register_activation_hook() inside a function hooked to the 'plugins_loaded' or 'init' hooks
		register_activation_hook(__FILE__, array($this, 'activate'));
	}

	public function activate()
	{
		$api_params = array(
			'edd_action'=> 'activate_license',
			'license' => self::LICENSE_KEY,
			'item_name' => urlencode(self::NAME),
			'url'       => home_url()
		);
		wp_remote_post(self::STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $api_params));
	}

	/**
	 * Handles our AJAX Active Campaign Events.
	 * Submit our event data.
	 */
	public function ajax_activecampaign_events()
	{
		$email = null;
		if (isset($_COOKIE['trackcmp_email']) && !empty($_COOKIE['trackcmp_email'])) {
			$email = $_COOKIE['trackcmp_email'];
		}
    /*
		if (is_user_logged_in()) {
			$current_user = wp_get_current_user();
			$email = $current_user->user_email;
		}
    */
		if ($email) {
			$data = array(
				'event' => $_POST['event'],
				'eventdata' => $_POST['data'],
			);

			self::$ac->track_email = $email;
			$response = self::request('tracking/log', $data);
			echo json_encode($response);
			self::$ac->track_email = '';
			die();
		}
	}

	/**
	 * Do an event tracking
	 *
	 * @param  array $data
	 * @return array $response
	 */
	public static function ac_event($data)
	{
		$response = self::request('tracking/log', $data);
		return $response;
	}


	/**
	 * Handles our AJAX Active Campaign Add Tag.
	 * Submit our event data.
   * Note: this isn't working yet. Needs some work.
	 */
	public function ajax_activecampaign_tag()
	{
		$email = null;
		if (isset($_COOKIE['trackcmp_email']) && !empty($_COOKIE['trackcmp_email'])) {
			$email = $_COOKIE['trackcmp_email'];
		}
    print "Yoyoyoyo";
		if ($email) {
			$data = array(
				'tag' => $_POST['tag']
			);

			self::$ac->track_email = $email;
			$response = self::request('contact/tag/add', $data);
			echo json_encode($response);
			self::$ac->track_email = '';
			die();
		}
	}

	/**
	 * Add a tag
	 *
	 * @param  array $data
	 * @return array $response
	 */
	public static function ac_tag($data)
	{
		$response = self::request('contact/tag/add', $data);
		return $response;
	}


	/**
	 * Perform an API requests
	 *
	 * @param   string  $string
	 * @param   array   $para
	 * @return  array   Results
	 */
	public static function request($string, $para = array())
	{
		$results = self::$ac->api($string, $para);
		return $results;
	}

	/**
	 * Register event tracking script.
	 * We localize admin-ajax.php to handle AJAX requests and trigger AC events.
	 */
	public static function event_tracking_js_script()
	{
		$ajaxurl = admin_url('admin-ajax.php');
		wp_register_script('ac-events-js', AC_JS_URL . '/ac_events.js', array('jquery'), '1.0', TRUE);
		wp_localize_script('ac-events-js', 'ajaxurl', $ajaxurl);
		wp_enqueue_script('ac-events-js');
	}

	/**
	 * Register site tracking script.
	 * Localize current log user's email or $_GET['ac_email'].
	 */
	public static function site_tracking_js_script()
	{
		wp_register_script('ac-site-tracking-js', AC_JS_URL . '/ac_site_tracking.js', array('jquery'), '1.0', TRUE);
		$actid = self::$acsf->get_option(AC_PREFIX . 'event_id');
		$email = null;
		if (isset($_COOKIE['trackcmp_email']) && !empty($_COOKIE['trackcmp_email'])) {
			$email = $_COOKIE['trackcmp_email'];
		}

		if (isset($_GET['ac_email']) && !empty($_GET['ac_email'])) {
			$email = $_GET['ac_email'];
			$expires = time() + (20 * 365 * 24 * 60 * 60);
			@setcookie('trackcmp_email', $email, $expires, '/');
		}

    /*
		if (is_user_logged_in()) {
			$current_user = wp_get_current_user();
			$email = $current_user->user_email;
		}
    */

		if ($email) {
			wp_localize_script('ac-site-tracking-js', 'trackcmp_email', $email);
			wp_localize_script('ac-site-tracking-js', 'trackcmp_actid', $actid);
			wp_enqueue_script('ac-site-tracking-js');
		}
	}

	/**
	 * Add our Email variable to the WP_Query
	 *
	 * @param  array $vars
	 * @return array        Updated query variable
	 */
	public static function add_email_to_query($vars)
	{
		$vars[] = 'email';
		return $vars;
	}

}

$wp_activecampaign = new WP_Active_Campaign();

}
