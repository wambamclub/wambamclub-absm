<?php

if (!$_POST) {
	// silence is gold
	exit;
}

define('AC_URL', 'http://dp.realtymotor.com/proc.php');


$ch = curl_init();
if (!$ch) {
	exit;
}
curl_setopt($ch, CURLOPT_URL, AC_URL);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
curl_setopt ($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

$redirect = null;
if ($output = curl_exec($ch)) {
	$lines = explode("\n", $output);
	$needle = 'Location:';
	foreach ($lines as $line) {
		$pos = strpos($line, $needle);
		if ($pos !== false) {
			$redirect = $line;
			break;
		}
	}
}

if ($redirect !== null) {
	header($redirect);
}

curl_close($ch);
