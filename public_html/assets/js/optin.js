$(document).ready(function() {
    $('a.privacy-policy').click(function(event) {
        event.preventDefault();
        showPrivacy();
    });
    $('a.special-offer').click(function(event) {
        event.preventDefault();
        showSpecialOffer();
    });
});

function showPrivacy() {
    $("#privacy-policy").dialog();
    $('#privacy-policy').dialog('option','width',400);
}

function showSpecialOffer() {
    $("#special-offer").dialog();
    $('#special-offer').dialog('option','width',600);
}
