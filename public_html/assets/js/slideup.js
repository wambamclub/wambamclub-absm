function slideUp() {
  $(function() {
    if($.cookie('dont_show') == null) {
      $('#slideup').slideDown("slow");
    }
  });
}

function slideDown() {
  $(function() {
    $(".dont-show").click(function() {
        $.cookie('dont_show', 'true', { expires: 3650, path: '/', domain: 'wambamclub.com' });
    });
    $('#slideup').slideUp("slow");
  });
}

