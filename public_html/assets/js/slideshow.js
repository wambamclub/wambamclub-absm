$(document).ready(function() {
    $('#slideshow').crossSlide({
        sleep: 2,
        fade: 1
    }, [
        {
            src: '/assets/img/slideshow/cafe-de-paris.png'
        }, {
            src: '/assets/img/slideshow/millie-dollar.png'
        }, {
            src: '/assets/img/slideshow/kitty-peels.png'
        }, {
            src: '/assets/img/slideshow/vicky-butterfly.png'
        }, {
            src: '/assets/img/slideshow/lady-ane-angel.png'
        }, {
            src: '/assets/img/slideshow/star-wars.png'
        }, {
            src: '/assets/img/slideshow/lady-alex.png'
        }, {
            src: '/assets/img/slideshow/lady-alex-old-lady.png'
        }, {
            src: '/assets/img/slideshow/polly-rae.png'
        }, {
            src: '/assets/img/slideshow/hurly-burly-girls.png'
        }, {
            src: '/assets/img/slideshow/richard-herring.png'
        }, {
            src: '/assets/img/slideshow/frisky-and-mannish.png'
        }, {
            src: '/assets/img/slideshow/frisky-and-mannish2.png'
        }, {
            src: '/assets/img/slideshow/stella-plumes.png'
        }, {
            src: '/assets/img/slideshow/moonfish-rhumba.png'
        }, {
            src: '/assets/img/slideshow/barry-and-stuart.png'
        }, {
            src: '/assets/img/slideshow/joe-bor.png'
        }, {
            src: '/assets/img/slideshow/scales-of-the-unexpected.png'
        }, {
            src: '/assets/img/slideshow/delightee.png'
        }, {
            src: '/assets/img/slideshow/hula-boy.png'
        }
    ]);
});

