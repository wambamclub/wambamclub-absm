#!/usr/bin/env php
<?php
date_default_timezone_set('UTC');

require_once(dirname(__FILE__) . '/../classes/aweber_queuer_class.php');
require_once(dirname(__FILE__) . '/../classes/writer.php');

if (! class_exists('Environment')) {
    require_once(dirname(__FILE__) . '/../classes/environment_class.php');
}

$list = isset($argv[1]) ? $argv[1] : '';
$queuer = new AweberQueuer(new Environment(), $list);
define('BATCH', 10);
set_time_limit(3600);

$i = 0;
while (($row = $queuer->next()) && $i < BATCH) {
    
    if (! $row) {
        die('Nothing to send!');
    }

    $response = postToAweber(
            trim($row['name']),
            trim($row['email']),
            trim($row['list']),
            'http://www.wambamclub.com/burlesque',
            'http://www.wambamclub.com/please-confirm/');
    $post = "[" . date("Y-m-d H:i:s") . "]: Posted [" . trim($row['name']) . "] to email [" . trim($row['email']) . "] on list [" . trim($row['list']) . "]\n"; 

    writeToLog($post, $response->getContent());
    print $post;
    $i++;
    sleep(rand(20, 60));
}


function postToAweber($name, $email, $list, $referer, $redirect) {
    $poster = new AweberPoster('http://www.aweber.com/scripts/addlead.pl');
    return $poster->post($name, $email, $list, $referer, $redirect);
}

function writeToLog($post, $content) {
    $writer = new FileWriter('/tmp/aweber.log');
    $writer->write($post . "\n");
    $writer->write($content . "\n\n");
}
?>