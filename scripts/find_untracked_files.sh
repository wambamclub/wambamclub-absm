SRC=/home/app/repo/wambamclub-absm.com/public_html
DST=/home/app/sites/wambamclub.com/public_html

cd ..

# Do not use --delete otherwise newer plugins in the sandbox get removed
# Don't use out-format in the rsync as they are meant to be quiet. The untracked files are listed at the end.

rsync  --include ".*" --recursive --times --perms --compress --links --update $SRC/wp-content/uploads/ $DST/wp-content/uploads

rsync  --exclude 'cache*' --include ".*" --recursive --times --perms --compress --links --update $SRC/wp-content/plugins/ $DST/wp-content/plugins

rsync  --include ".*" --recursive --times --perms --compress --links --update $SRC/wp-content/themes/ $DST/wp-content/themes

rsync --exclude="uploads" --exclude="plugins" --exclude="themes" --include ".*" --recursive --times --perms --compress --links --update $SRC/wp-content/ $DST/wp-content

# Do use --delete as we want the wp-admin and wp-includes system files to be exactly the same
rsync  --include ".*" --delete --recursive --times --perms --update --compress --links $SRC/wp-admin/ $DST/wp-admin

rsync   --include ".*" --delete --recursive --times --perms --update --compress --links $SRC/wp-includes/ $DST/wp-includes

# Only sync files in the public_html directory that have changed (not any wp-* subdirectories)
rsync -rtu --exclude=wp-*/ $SRC/ $DST/

# If a whole directory is classified as "other", show just its name (with a trailing slash) and not its whole contents.
git ls-files . --exclude-standard --others --exclude='scripts/*' --directory --no-empty-directory

