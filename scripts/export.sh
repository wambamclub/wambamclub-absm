# If you run into problems look at the .gitignore file to see what's being ignored.
# The clean -dfx removes untracked directories & ignored files.
# Also files which are in the captcha folder can end up back in the sandbox through the rsync & then are removed by the clean -dfx.

DST=/home/app/sites/wambamclub.com/public_html

echo $ Clean untracked local files
git clean -dfx ..

echo $ Pull latest repository updates
git pull

echo $ Testing for differences between live/sandbox
./find_untracked_files.sh
lines=`./find_untracked_files.sh | wc -l`
        if [ $lines -ge 1 ]
then
    echo $ WARNING: Found [$lines] untracked files. Please add/commit to repository before rollout. Thanks!
    exit;
else
    echo $ No untracked files. Rollout can proceed...
fi

echo $ Rolling out wambamclub.com
cd ..
rsync  --exclude "*/.git/" --delete --recursive --progress --stats --times --perms --compress --links * $DST

mkdir -p $DST/wp-content/plugins/alpine-photo-tile-for-instagram/cache/
chmod -R g+w $DST/wp-content/plugins/alpine-photo-tile-for-instagram/cache/
chmod -R g+w $DST/wp-content/gallery/
chmod -R 775 $DST/wp-content/uploads/

