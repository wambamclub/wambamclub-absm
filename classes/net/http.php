<?php
require_once(dirname(__FILE__) . '/socket.php');
require_once(dirname(__FILE__) . '/cookies.php');
require_once(dirname(__FILE__) . '/url.php');

class Route {
	var $_url;

	function Route($url) {
		$this->_url = $url;
	}

	function getUrl() {
		return $this->_url;
	}

	function _getRequestLine($method) {
		return $method . ' ' . $this->_url->getPath() .
				$this->_url->getEncodedRequest() . ' HTTP/1.0';
	}

	function _getHostLine() {
		$line = 'Host: ' . $this->_url->getHost();
		if ($this->_url->getPort()) {
			$line .= ':' . $this->_url->getPort();
		}
		return $line;
	}

	function &createConnection($method, $timeout) {
		$default_port = ('https' == $this->_url->getScheme()) ? 443 : 80;
		$socket = &$this->_createSocket(
				$this->_url->getScheme() ? $this->_url->getScheme() : 'http',
				$this->_url->getHost(),
				$this->_url->getPort() ? $this->_url->getPort() : $default_port,
				$timeout);
		if (! $socket->isError()) {
			$socket->write($this->_getRequestLine($method) . "\r\n");
			$socket->write($this->_getHostLine() . "\r\n");
			$socket->write("Connection: close\r\n");
		}
		return $socket;
	}

	function &_createSocket($scheme, $host, $port, $timeout) {
		if (in_array($scheme, array('https'))) {
			$socket = &new SecureSocket($host, $port, $timeout);
		} else {
			$socket = &new Socket($host, $port, $timeout);
		}
		return $socket;
	}
}

class ProxyRoute extends Route {
	var $_proxy;
	var $_username;
	var $_password;

	function ProxyRoute($url, $proxy, $username = false, $password = false) {
		$this->Route($url);
		$this->_proxy = $proxy;
		$this->_username = $username;
		$this->_password = $password;
	}

	function _getRequestLine($method) {
		$url = $this->getUrl();
		$scheme = $url->getScheme() ? $url->getScheme() : 'http';
		$port = $url->getPort() ? ':' . $url->getPort() : '';
		return $method . ' ' . $scheme . '://' . $url->getHost() . $port .
				$url->getPath() . $url->getEncodedRequest() . ' HTTP/1.0';
	}

	function _getHostLine() {
		$host = 'Host: ' . $this->_proxy->getHost();
		$port = $this->_proxy->getPort() ? $this->_proxy->getPort() : 8080;
		return "$host:$port";
	}

	function &createConnection($method, $timeout) {
		$socket = &$this->_createSocket(
				$this->_proxy->getScheme() ? $this->_proxy->getScheme() : 'http',
				$this->_proxy->getHost(),
				$this->_proxy->getPort() ? $this->_proxy->getPort() : 8080,
				$timeout);
		if ($socket->isError()) {
			return $socket;
		}
		$socket->write($this->_getRequestLine($method) . "\r\n");
		$socket->write($this->_getHostLine() . "\r\n");
		if ($this->_username && $this->_password) {
			$socket->write('Proxy-Authorization: Basic ' .
					base64_encode($this->_username . ':' . $this->_password) .
					"\r\n");
		}
		$socket->write("Connection: close\r\n");
		return $socket;
	}
}

class HttpRequest {
	var $_route;
	var $_encoding;
	var $_headers = array();
	var $_cookies = array();

	function HttpRequest(&$route, $encoding) {
		$this->_route = &$route;
		$this->_encoding = $encoding;
	}

	function &fetch($timeout) {
		$socket = &$this->_route->createConnection($this->_encoding->getMethod(), $timeout);
		if (! $socket->isError()) {
			$this->_dispatchRequest($socket, $this->_encoding);
		}
		$response = &$this->_createResponse($socket);
		return $response;
	}

	function _dispatchRequest(&$socket, $encoding) {
		foreach ($this->_headers as $header => $value) {
			$socket->write($header . ': ' . $value . "\r\n");
		}
		if (count($this->_cookies) > 0) {
			$socket->write("Cookie: " . implode(";", $this->_cookies) . "\r\n");
		}
		$encoding->writeHeadersTo($socket);
		$socket->write("\r\n");
		$encoding->writeTo($socket);
	}

	function setHeader($header, $value) {
		$this->_headers[$header] = $value;
	}

	function readCookiesFromJar($jar, $url) {
		$this->_cookies = $jar->selectAsPairs($url);
	}

	function &_createResponse(&$socket) {
		$response = &new HttpResponse(
				$socket,
				$this->_route->getUrl(),
				$this->_encoding);
		return $response;
	}
}

class HttpHeaders {
	var $_raw_headers;
	var $_response_code;
	var $_http_version;
	var $_mime_type;
	var $_location;
	var $_cookies;
	var $_authentication;
	var $_realm;

	function HttpHeaders($headers) {
		$this->_raw_headers = $headers;
		$this->_response_code = false;
		$this->_http_version = false;
		$this->_mime_type = '';
		$this->_location = false;
		$this->_cookies = array();
		$this->_authentication = false;
		$this->_realm = false;
		foreach (split("\r\n", $headers) as $header_line) {
			$this->_parseHeaderLine($header_line);
		}
	}

	function getHttpVersion() {
		return $this->_http_version;
	}

	function getRaw() {
		return $this->_raw_headers;
	}

	function getResponseCode() {
		return (integer)$this->_response_code;
	}

	function getLocation() {
		return $this->_location;
	}

	function isRedirect() {
		return in_array($this->_response_code, array(301, 302, 303, 307)) &&
				(boolean)$this->getLocation();
	}

	function isChallenge() {
		return ($this->_response_code == 401) &&
				(boolean)$this->_authentication &&
				(boolean)$this->_realm;
	}

	function getMimeType() {
		return $this->_mime_type;
	}

	function getAuthentication() {
		return $this->_authentication;
	}

	function getRealm() {
		return $this->_realm;
	}

	function writeCookiesToJar(&$jar, $url) {
		foreach ($this->_cookies as $cookie) {
			$jar->setCookie(
					$cookie->getName(),
					$cookie->getValue(),
					$url->getHost(),
					$cookie->getPath(),
					$cookie->getExpiry());
		}
	}

	function _parseHeaderLine($header_line) {
		if (preg_match('/HTTP\/(\d+\.\d+)\s+(\d+)/i', $header_line, $matches)) {
			$this->_http_version = $matches[1];
			$this->_response_code = $matches[2];
		}
		if (preg_match('/Content-type:\s*(.*)/i', $header_line, $matches)) {
			$this->_mime_type = trim($matches[1]);
		}
		if (preg_match('/Location:\s*(.*)/i', $header_line, $matches)) {
			$this->_location = trim($matches[1]);
		}
		if (preg_match('/Set-cookie:(.*)/i', $header_line, $matches)) {
			$this->_cookies[] = $this->_parseCookie($matches[1]);
		}
		if (preg_match('/WWW-Authenticate:\s+(\S+)\s+realm=\"(.*?)\"/i', $header_line, $matches)) {
			$this->_authentication = $matches[1];
			$this->_realm = trim($matches[2]);
		}
	}

	function _parseCookie($cookie_line) {
		$parts = split(";", $cookie_line);
		$cookie = array();
		preg_match('/\s*(.*?)\s*=(.*)/', array_shift($parts), $cookie);
		foreach ($parts as $part) {
			if (preg_match('/\s*(.*?)\s*=(.*)/', $part, $matches)) {
				$cookie[$matches[1]] = trim($matches[2]);
			}
		}
		return new Cookie(
				$cookie[1],
				trim($cookie[2]),
				isset($cookie["path"]) ? $cookie["path"] : "",
				isset($cookie["expires"]) ? $cookie["expires"] : false);
	}
}

class HttpResponse extends NetStickyError {
	var $_url;
	var $_encoding;
	var $_sent;
	var $_content;
	var $_headers;

	function HttpResponse(&$socket, $url, $encoding) {
		$this->NetStickyError();
		$this->_url = $url;
		$this->_encoding = $encoding;
		$this->_sent = $socket->getSent();
		$this->_content = false;
		$raw = $this->_readAll($socket);
		if ($socket->isError()) {
			$this->_setError('Error reading socket [' . $socket->getError() . ']');
			return;
		}
		$this->_parse($raw);
	}

	function _parse($raw) {
		if (! $raw) {
			$this->_setError('Nothing fetched');
			$this->_headers = &new HttpHeaders('');
		} elseif (! strstr($raw, "\r\n\r\n")) {
			$this->_setError('Could not split headers from content');
			$this->_headers = &new HttpHeaders($raw);
		} else {
			list($headers, $this->_content) = split("\r\n\r\n", $raw, 2);
			$this->_headers = &new HttpHeaders($headers);
		}
	}

	function getMethod() {
		return $this->_encoding->getMethod();
	}

	function getUrl() {
		return $this->_url;
	}

	function getRequestData() {
		return $this->_encoding;
	}

	function getSent() {
		return $this->_sent;
	}

	function getContent() {
		return $this->_content;
	}

	function getHeaders() {
		return $this->_headers;
	}

	function getNewCookies() {
		return $this->_headers->getNewCookies();
	}

	function _readAll(&$socket) {
		$all = '';
		while (! $this->_isLastPacket($next = $socket->read())) {
			$all .= $next;
		}
		return $all;
	}

	function _isLastPacket($packet) {
		if (is_string($packet)) {
			return $packet === '';
		}
		return ! $packet;
	}
}
?>