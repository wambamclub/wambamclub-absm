<?php
require_once(dirname(__FILE__) . '/../../external/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../url.php');
require_once(dirname(__FILE__) . '/../socket.php');

Mock::generate('Socket');

class TestOfEncodedParts extends UnitTestCase {

    function testFormEncodedAsKeyEqualsValue() {
        $pair = new EncodedPair('a', 'A');
        $this->assertEqual($pair->asRequest(), 'a=A');
    }

    function testMimeEncodedAsHeadersAndContent() {
        $pair = new EncodedPair('a', 'A');
        $this->assertEqual(
                $pair->asMime(),
                "Content-Disposition: form-data; name=\"a\"\r\n\r\nA");
    }

    function testAttachmentEncodedAsHeadersWithDispositionAndContent() {
        $part = new Attachment('a', 'A', 'aaa.txt');
        $this->assertEqual(
                $part->asMime(),
                "Content-Disposition: form-data; name=\"a\"; filename=\"aaa.txt\"\r\n" .
                        "Content-Type: text/plain\r\n\r\nA");
    }
}

class TestOfEncoding extends UnitTestCase {
    var $_content_so_far;

    function write($content) {
        $this->_content_so_far .= $content;
    }

    function clear() {
        $this->_content_so_far = '';
    }

    function assertWritten($encoding, $content, $message = '%s') {
        $this->clear();
        $encoding->writeTo($this);
        $this->assertIdentical($this->_content_so_far, $content, $message);
    }

    function testGetEmpty() {
        $encoding = &new GetEncoding();
        $this->assertIdentical($encoding->getValue('a'), false);
        $this->assertIdentical($encoding->asUrlRequest(), '');
    }

    function testPostEmpty() {
        $encoding = &new PostEncoding();
        $this->assertIdentical($encoding->getValue('a'), false);
        $this->assertWritten($encoding, '');
    }

    function testPrefilled() {
        $encoding = &new PostEncoding(array('a' => 'aaa'));
        $this->assertIdentical($encoding->getValue('a'), 'aaa');
        $this->assertWritten($encoding, 'a=aaa');
    }

    function testPrefilledWithObject() {
        $encoding = &new PostEncoding(new Encoding(array('a' => 'aaa')));
        $this->assertIdentical($encoding->getValue('a'), 'aaa');
        $this->assertWritten($encoding, 'a=aaa');
    }

    function testMultiplePrefilled() {
        $encoding = &new PostEncoding(array('a' => array('a1', 'a2')));
        $this->assertIdentical($encoding->getValue('a'), array('a1', 'a2'));
        $this->assertWritten($encoding, 'a=a1&a=a2');
    }

    function testSingleParameter() {
        $encoding = &new PostEncoding();
        $encoding->add('a', 'Hello');
        $this->assertEqual($encoding->getValue('a'), 'Hello');
        $this->assertWritten($encoding, 'a=Hello');
    }

    function testFalseParameter() {
        $encoding = &new PostEncoding();
        $encoding->add('a', false);
        $this->assertEqual($encoding->getValue('a'), false);
        $this->assertWritten($encoding, '');
    }

    function testUrlEncoding() {
        $encoding = &new PostEncoding();
        $encoding->add('a', 'Hello there!');
        $this->assertWritten($encoding, 'a=Hello+there%21');
    }

    function testMultipleParameter() {
        $encoding = &new PostEncoding();
        $encoding->add('a', 'Hello');
        $encoding->add('b', 'Goodbye');
        $this->assertWritten($encoding, 'a=Hello&b=Goodbye');
    }

    function testEmptyParameters() {
        $encoding = &new PostEncoding();
        $encoding->add('a', '');
        $encoding->add('b', '');
        $this->assertWritten($encoding, 'a=&b=');
    }

    function testRepeatedParameter() {
        $encoding = &new PostEncoding();
        $encoding->add('a', 'Hello');
        $encoding->add('a', 'Goodbye');
        $this->assertIdentical($encoding->getValue('a'), array('Hello', 'Goodbye'));
        $this->assertWritten($encoding, 'a=Hello&a=Goodbye');
    }

    function testAddingLists() {
        $encoding = &new PostEncoding();
        $encoding->add('a', array('Hello', 'Goodbye'));
        $this->assertIdentical($encoding->getValue('a'), array('Hello', 'Goodbye'));
        $this->assertWritten($encoding, 'a=Hello&a=Goodbye');
    }

    function testMergeInHash() {
        $encoding = &new GetEncoding(array('a' => 'A1', 'b' => 'B'));
        $encoding->merge(array('a' => 'A2'));
        $this->assertIdentical($encoding->getValue('a'), array('A1', 'A2'));
        $this->assertIdentical($encoding->getValue('b'), 'B');
    }

    function testMergeInObject() {
        $encoding = &new GetEncoding(array('a' => 'A1', 'b' => 'B'));
        $encoding->merge(new Encoding(array('a' => 'A2')));
        $this->assertIdentical($encoding->getValue('a'), array('A1', 'A2'));
        $this->assertIdentical($encoding->getValue('b'), 'B');
    }
}

class TestOfFormHeaders extends UnitTestCase {

    function testEmptyEncodingWritesZeroContentLength() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("Content-Length: 0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Content-Type: application/x-www-form-urlencoded\r\n"));
        $encoding = &new PostEncoding();
        $encoding->writeHeadersTo($socket);
    }
}
?>