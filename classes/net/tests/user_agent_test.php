<?php
require_once(dirname(__FILE__) . '/../../external/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../user_agent.php');
require_once(dirname(__FILE__) . '/../authentication.php');
require_once(dirname(__FILE__) . '/../http.php');
require_once(dirname(__FILE__) . '/../encoding.php');
Mock::generate('HttpRequest');
Mock::generate('HttpResponse');
Mock::generate('HttpHeaders');
Mock::generatePartial('UserAgent', 'MockRequestUserAgent', array('_createHttpRequest'));

class TestOfFetchingUrlParameters extends UnitTestCase {

    function setUp() {
        $this->_headers = &new MockHttpHeaders();

        $this->_response = &new MockHttpResponse();
        $this->_response->setReturnValue('isError', false);
        $this->_response->setReturnReference('getHeaders', new MockHttpHeaders());

        $this->_request = &new MockHttpRequest();
        $this->_request->setReturnReference('fetch', $this->_response);
    }

    function testGetRequestWithoutIncidentGivesNoErrors() {
        $url = new Url('http://test:secret@this.com/page.html');
        $url->addRequestParameters(array('a' => 'A', 'b' => 'B'));

        $agent = &new MockRequestUserAgent();
        $agent->setReturnReference('_createHttpRequest', $this->_request);
        $agent->UserAgent();

        $response = &$agent->fetchResponse(
                new Url('http://test:secret@this.com/page.html'),
                new GetEncoding(array('a' => 'A', 'b' => 'B')));
        $this->assertFalse($response->isError());
    }
}

class TestOfAdditionalHeaders extends UnitTestCase {

    function testAdditionalHeaderAddedToRequest() {
        $response = &new MockHttpResponse();
        $response->setReturnReference('getHeaders', new MockHttpHeaders());

        $request = &new MockHttpRequest();
        $request->setReturnReference('fetch', $response);
        $request->expectOnce(
                'setHeader',
                array('User-Agent', 'NetTest'));

        $agent = &new MockRequestUserAgent();
        $agent->setReturnReference('_createHttpRequest', $request);
        $agent->UserAgent();
        $agent->setHeader('User-Agent', 'NetTest');
        $response = &$agent->fetchResponse(new Url('http://this.host/'), new GetEncoding());
    }
}

class TestOfBrowserCookies extends UnitTestCase {

    function &_createStandardResponse() {
        $response = &new MockHttpResponse();
        $response->setReturnValue("isError", false);
        $response->setReturnValue("getContent", "stuff");
        $response->setReturnReference("getHeaders", new MockHttpHeaders());
        return $response;
    }

    function &_createCookieSite($header_lines) {
        $headers = &new HttpHeaders($header_lines);

        $response = &new MockHttpResponse();
        $response->setReturnValue("isError", false);
        $response->setReturnReference("getHeaders", $headers);
        $response->setReturnValue("getContent", "stuff");

        $request = &new MockHttpRequest();
        $request->setReturnReference("fetch", $response);
        return $request;
    }

    function &_createMockedRequestUserAgent(&$request) {
        $agent = &new MockRequestUserAgent();
        $agent->setReturnReference('_createHttpRequest', $request);
        $agent->UserAgent();
        return $agent;
    }

    function testCookieJarIsSentToRequest() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A');

        $request = &new MockHttpRequest();
        $request->setReturnReference('fetch', $this->_createStandardResponse());
        $request->expectOnce('readCookiesFromJar', array($jar, '*'));

        $agent = &$this->_createMockedRequestUserAgent($request);
        $agent->setCookie('a', 'A');
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
    }

    function testNoCookieJarIsSentToRequestWhenCookiesAreDisabled() {
        $request = &new MockHttpRequest();
        $request->setReturnReference('fetch', $this->_createStandardResponse());
        $request->expectNever('readCookiesFromJar');

        $agent = &$this->_createMockedRequestUserAgent($request);
        $agent->setCookie('a', 'A');
        $agent->ignoreCookies();
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
    }

    function testReadingNewCookie() {
        $request = &$this->_createCookieSite('Set-cookie: a=AAAA');
        $agent = &$this->_createMockedRequestUserAgent($request);
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $this->assertEqual($agent->getCookieValue("this.com", "/this/path/", "a"), "AAAA");
    }

    function testGetAllCookies() {
        $request = &$this->_createCookieSite('Set-cookie: a=AAAA');
        $agent = &$this->_createMockedRequestUserAgent($request);
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $this->assertEqual($agent->getCookies("this.com", "/this/path/"), array("a" => "AAAA"));
    }

    function testIgnoringNewCookieWhenCookiesDisabled() {
        $request = &$this->_createCookieSite('Set-cookie: a=AAAA');
        $agent = &$this->_createMockedRequestUserAgent($request);
        $agent->ignoreCookies();
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $this->assertIdentical($agent->getCookieValue("this.com", "this/path/", "a"), false);
    }

    function testOverwriteCookieThatAlreadyExists() {
        $request = &$this->_createCookieSite('Set-cookie: a=AAAA');
        $agent = &$this->_createMockedRequestUserAgent($request);
        $agent->setCookie('a', 'A');
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $this->assertEqual($agent->getCookieValue("this.com", "this/path/", "a"), "AAAA");
    }

    function testClearCookieBySettingExpiry() {
        $request = &$this->_createCookieSite('Set-cookie: a=b');
        $agent = &$this->_createMockedRequestUserAgent($request);

        $agent->setCookie("a", "A", "this/path/", "Wed, 25-Dec-02 04:24:21 GMT");
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $this->assertIdentical(
                $agent->getCookieValue("this.com", "this/path/", "a"),
                "b");
        $agent->restart("Wed, 25-Dec-02 04:24:20 GMT");
        $this->assertIdentical(
                $agent->getCookieValue("this.com", "this/path/", "a"),
                false);
    }

    function testAgeingAndClearing() {
        $request = &$this->_createCookieSite('Set-cookie: a=A; expires=Wed, 25-Dec-02 04:24:21 GMT; path=/this/path');
        $agent = &$this->_createMockedRequestUserAgent($request);

        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $agent->restart("Wed, 25-Dec-02 04:24:20 GMT");
        $this->assertIdentical(
                $agent->getCookieValue("this.com", "this/path/", "a"),
                "A");
        $agent->ageCookies(2);
        $agent->restart("Wed, 25-Dec-02 04:24:20 GMT");
        $this->assertIdentical(
                $agent->getCookieValue("this.com", "this/path/", "a"),
                false);
    }

    function testReadingIncomingAndSettingNewCookies() {
        $request = &$this->_createCookieSite('Set-cookie: a=AAA');
        $agent = &$this->_createMockedRequestUserAgent($request);

        $this->assertNull($agent->getBaseCookieValue("a", false));
        $agent->fetchResponse(
                new Url('http://this.com/this/path/page.html'),
                new GetEncoding());
        $agent->setCookie("b", "BBB", "this.com", "this/path/");
        $this->assertEqual(
                $agent->getBaseCookieValue("a", new Url('http://this.com/this/path/page.html')),
                "AAA");
        $this->assertEqual(
                $agent->getBaseCookieValue("b", new Url('http://this.com/this/path/page.html')),
                "BBB");
    }
}

class TestOfHttpRedirects extends UnitTestCase {

    function &createRedirect($content, $redirect) {
        $headers = &new MockHttpHeaders();
        $headers->setReturnValue('isRedirect', (boolean)$redirect);
        $headers->setReturnValue('getLocation', $redirect);

        $response = &new MockHttpResponse();
        $response->setReturnValue('getContent', $content);
        $response->setReturnReference('getHeaders', $headers);

        $request = &new MockHttpRequest();
        $request->setReturnReference('fetch', $response);
        return $request;
    }

    function testDisabledRedirects() {
        $agent = &new MockRequestUserAgent();
        $agent->setReturnReference(
                '_createHttpRequest',
                $this->createRedirect('stuff', 'there.html'));
        $agent->expectOnce('_createHttpRequest');
        $agent->UserAgent();

        $agent->setMaximumRedirects(0);
        $response = &$agent->fetchResponse(new Url('here.html'), new GetEncoding());
        $this->assertEqual($response->getContent(), 'stuff');
    }

    function testSingleRedirect() {
        $agent = &new MockRequestUserAgent();
        $agent->setReturnReferenceAt(
                0,
                '_createHttpRequest',
                $this->createRedirect('first', 'two.html'));
        $agent->setReturnReferenceAt(
                1,
                '_createHttpRequest',
                $this->createRedirect('second', 'three.html'));
        $agent->expectCallCount('_createHttpRequest', 2);
        $agent->UserAgent();

        $agent->setMaximumRedirects(1);
        $response = &$agent->fetchResponse(new Url('one.html'), new GetEncoding());
        $this->assertEqual($response->getContent(), 'second');
    }

    function testDoubleRedirect() {
        $agent = &new MockRequestUserAgent();
        $agent->setReturnReferenceAt(
                0,
                '_createHttpRequest',
                $this->createRedirect('first', 'two.html'));
        $agent->setReturnReferenceAt(
                1,
                '_createHttpRequest',
                $this->createRedirect('second', 'three.html'));
        $agent->setReturnReferenceAt(
                2,
                '_createHttpRequest',
                $this->createRedirect('third', 'four.html'));
        $agent->expectCallCount('_createHttpRequest', 3);
        $agent->UserAgent();

        $agent->setMaximumRedirects(2);
        $response = &$agent->fetchResponse(new Url('one.html'), new GetEncoding());
        $this->assertEqual($response->getContent(), 'third');
    }

    function testSuccessAfterRedirect() {
        $agent = &new MockRequestUserAgent();
        $agent->setReturnReferenceAt(
                0,
                '_createHttpRequest',
                $this->createRedirect('first', 'two.html'));
        $agent->setReturnReferenceAt(
                1,
                '_createHttpRequest',
                $this->createRedirect('second', false));
        $agent->setReturnReferenceAt(
                2,
                '_createHttpRequest',
                $this->createRedirect('third', 'four.html'));
        $agent->expectCallCount('_createHttpRequest', 2);
        $agent->UserAgent();

        $agent->setMaximumRedirects(2);
        $response = &$agent->fetchResponse(new Url('one.html'), new GetEncoding());
        $this->assertEqual($response->getContent(), 'second');
    }

    function testRedirectChangesPostToGet() {
        $agent = &new MockRequestUserAgent();
        $agent->setReturnReferenceAt(
                0,
                '_createHttpRequest',
                $this->createRedirect('first', 'two.html'));
        $agent->expectArgumentsAt(0, '_createHttpRequest', array('*', new IsAExpectation('PostEncoding')));
        $agent->setReturnReferenceAt(
                1,
                '_createHttpRequest',
                $this->createRedirect('second', 'three.html'));
        $agent->expectArgumentsAt(1, '_createHttpRequest', array('*', new IsAExpectation('GetEncoding')));
        $agent->expectCallCount('_createHttpRequest', 2);
        $agent->UserAgent();
        $agent->setMaximumRedirects(1);
        $response = &$agent->fetchResponse(new Url('one.html'), new PostEncoding());
    }
}

class TestOfBadHosts extends UnitTestCase {

    function &_createSimulatedBadHost() {
        $response = &new MockHttpResponse();
        $response->setReturnValue('isError', true);
        $response->setReturnValue('getError', 'Bad socket');
        $response->setReturnValue('getContent', false);

        $request = &new MockHttpRequest();
        $request->setReturnReference('fetch', $response);
        return $request;
    }

    function testUntestedHost() {
        $request = &$this->_createSimulatedBadHost();

        $agent = &new MockRequestUserAgent();
        $agent->setReturnReference('_createHttpRequest', $request);
        $agent->UserAgent();

        $response = &$agent->fetchResponse(
                new Url('http://this.host/this/path/page.html'),
                new GetEncoding());
        $this->assertTrue($response->isError());
    }
}

class TestOfAuthorisation extends UnitTestCase {

    function testAuthenticateHeaderAdded() {
        $response = &new MockHttpResponse();
        $response->setReturnReference('getHeaders', new MockHttpHeaders());

        $request = &new MockHttpRequest();
        $request->setReturnReference('fetch', $response);
        $request->expectOnce(
                'setHeader',
                array('Authorization', 'Basic ' . base64_encode('test:secret')));

        $agent = &new MockRequestUserAgent();
        $agent->setReturnReference('_createHttpRequest', $request);
        $agent->UserAgent();
        $response = &$agent->fetchResponse(
                new Url('http://test:secret@this.host'),
                new GetEncoding());
    }
}
?>