<?php
require_once(dirname(__FILE__) . '/../../external/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../url.php');

class TestOfUrl extends UnitTestCase {

    function testDefaultUrl() {
        $url = new Url('');
        $this->assertEqual($url->getScheme(), '');
        $this->assertEqual($url->getHost(), '');
        $this->assertEqual($url->getScheme('http'), 'http');
        $this->assertEqual($url->getHost('localhost'), 'localhost');
        $this->assertEqual($url->getPath(), '');
    }

    function testBasicParsing() {
        $url = new Url('https://www.lastcraft.com/test/');
        $this->assertEqual($url->getScheme(), 'https');
        $this->assertEqual($url->getHost(), 'www.lastcraft.com');
        $this->assertEqual($url->getPath(), '/test/');
    }

    function testRelativeUrls() {
        $url = new Url('../somewhere.php');
        $this->assertEqual($url->getScheme(), false);
        $this->assertEqual($url->getHost(), false);
        $this->assertEqual($url->getPath(), '../somewhere.php');
    }

    function testParseBareParameter() {
        $url = new Url('?a');
        $this->assertEqual($url->getPath(), '');
        $this->assertEqual($url->getEncodedRequest(), '?a');
        $url->addRequestParameter('x', 'X');
        $this->assertEqual($url->getEncodedRequest(), '?a=&x=X');
    }

    function testParseEmptyParameter() {
        $url = new Url('?a=');
        $this->assertEqual($url->getPath(), '');
        $this->assertEqual($url->getEncodedRequest(), '?a=');
        $url->addRequestParameter('x', 'X');
        $this->assertEqual($url->getEncodedRequest(), '?a=&x=X');
    }

    function testParseParameterPair() {
        $url = new Url('?a=A');
        $this->assertEqual($url->getPath(), '');
        $this->assertEqual($url->getEncodedRequest(), '?a=A');
        $url->addRequestParameter('x', 'X');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&x=X');
    }

    function testParseMultipleParameters() {
        $url = new Url('?a=A&b=B');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=B');
        $url->addRequestParameter('x', 'X');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=B&x=X');
    }

    function testParsingParameterMixture() {
        $url = new Url('?a=A&b=&c');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=&c');
        $url->addRequestParameter('x', 'X');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=&c=&x=X');
    }

    function testAddParametersFromScratch() {
        $url = new Url('');
        $url->addRequestParameter('a', 'A');
        $this->assertEqual($url->getEncodedRequest(), '?a=A');
        $url->addRequestParameter('b', 'B');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=B');
        $url->addRequestParameter('a', 'aaa');
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=B&a=aaa');
    }

    function testClearingParameters() {
        $url = new Url('');
        $url->addRequestParameter('a', 'A');
        $url->clearRequest();
        $this->assertIdentical($url->getEncodedRequest(), '');
    }

    function testEncodingParameters() {
        $url = new Url('');
        $url->addRequestParameter('a', '?!"\'#~@[]{}:;<>,./|�$%^&*()_+-=');
        $this->assertIdentical(
                $request = $url->getEncodedRequest(),
                '?a=%3F%21%22%27%23%7E%40%5B%5D%7B%7D%3A%3B%3C%3E%2C.%2F%7C%A3%24%25%5E%26%2A%28%29_%2B-%3D');
    }

    function testDecodingParameters() {
        $url = new Url('?a=%3F%21%22%27%23%7E%40%5B%5D%7B%7D%3A%3B%3C%3E%2C.%2F%7C%A3%24%25%5E%26%2A%28%29_%2B-%3D');
        $this->assertEqual(
                $url->getEncodedRequest(),
                '?a=' . urlencode('?!"\'#~@[]{}:;<>,./|�$%^&*()_+-='));
    }

    function testSettingCordinates() {
        $url = new Url('');
        $url->setCoordinates('32', '45');
        $this->assertIdentical($url->getX(), 32);
        $this->assertIdentical($url->getY(), 45);
        $this->assertEqual($url->getEncodedRequest(), '');
    }

    function testParseCordinates() {
        $url = new Url('?32,45');
        $this->assertIdentical($url->getX(), 32);
        $this->assertIdentical($url->getY(), 45);
    }

    function testClearingCordinates() {
        $url = new Url('?32,45');
        $url->setCoordinates();
        $this->assertIdentical($url->getX(), false);
        $this->assertIdentical($url->getY(), false);
    }

    function testParsingParameterCordinateMixture() {
        $url = new Url('?a=A&b=&c?32,45');
        $this->assertIdentical($url->getX(), 32);
        $this->assertIdentical($url->getY(), 45);
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=&c');
    }

    function testParsingParameterWithBadCordinates() {
        $url = new Url('?a=A&b=&c?32');
        $this->assertIdentical($url->getX(), false);
        $this->assertIdentical($url->getY(), false);
        $this->assertEqual($url->getEncodedRequest(), '?a=A&b=&c?32');
    }

    function testPageSplitting() {
        $url = new Url('./here/../there/somewhere.php');
        $this->assertEqual($url->getPath(), './here/../there/somewhere.php');
        $this->assertEqual($url->getPage(), 'somewhere.php');
        $this->assertEqual($url->getBasePath(), './here/../there/');
    }

    function testAbsolutePathPageSplitting() {
        $url = new Url("http://host.com/here/there/somewhere.php");
        $this->assertEqual($url->getPath(), "/here/there/somewhere.php");
        $this->assertEqual($url->getPage(), "somewhere.php");
        $this->assertEqual($url->getBasePath(), "/here/there/");
    }

    function testSplittingUrlWithNoPageGivesEmptyPage() {
        $url = new Url('/here/there/');
        $this->assertEqual($url->getPath(), '/here/there/');
        $this->assertEqual($url->getPage(), '');
        $this->assertEqual($url->getBasePath(), '/here/there/');
    }

    function testPathNormalisation() {
        $this->assertEqual(
                Url::normalisePath('https://host.com/I/am/here/../there/somewhere.php'),
                'https://host.com/I/am/there/somewhere.php');
    }

    function testUsernameAndPasswordAreUrlDecoded() {
        $url = new Url('http://' . urlencode('test@test') .
                ':' . urlencode('$!�@*&%') . '@www.lastcraft.com');
        $this->assertEqual($url->getUsername(), 'test@test');
        $this->assertEqual($url->getPassword(), '$!�@*&%');
    }

    function testBlitz() {
        $this->assertUrl(
                "https://username:password@www.somewhere.com:243/this/that/here.php?a=1&b=2#anchor",
                array("https", "username", "password", "www.somewhere.com", 243, "/this/that/here.php", "com", "?a=1&b=2", "anchor"),
                array("a" => "1", "b" => "2"));
        $this->assertUrl(
                "username:password@www.somewhere.com/this/that/here.php?a=1",
                array(false, "username", "password", "www.somewhere.com", false, "/this/that/here.php", "com", "?a=1", false),
                array("a" => "1"));
        $this->assertUrl(
                "username:password@somewhere.com:243?1,2",
                array(false, "username", "password", "somewhere.com", 243, "/", "com", "", false),
                array(),
                array(1, 2));
        $this->assertUrl(
                "https://www.somewhere.com",
                array("https", false, false, "www.somewhere.com", false, "/", "com", "", false));
        $this->assertUrl(
                "username@www.somewhere.com:243#anchor",
                array(false, "username", false, "www.somewhere.com", 243, "/", "com", "", "anchor"));
        $this->assertUrl(
                "/this/that/here.php?a=1&b=2?3,4",
                array(false, false, false, false, false, "/this/that/here.php", false, "?a=1&b=2", false),
                array("a" => "1", "b" => "2"),
                array(3, 4));
        $this->assertUrl(
                "username@/here.php?a=1&b=2",
                array(false, "username", false, false, false, "/here.php", false, "?a=1&b=2", false),
                array("a" => "1", "b" => "2"));
    }

    function testAmbiguousHosts() {
        $this->assertUrl(
                "tigger",
                array(false, false, false, false, false, "tigger", false, "", false));
        $this->assertUrl(
                "/tigger",
                array(false, false, false, false, false, "/tigger", false, "", false));
        $this->assertUrl(
                "//tigger",
                array(false, false, false, "tigger", false, "/", false, "", false));
        $this->assertUrl(
                "//tigger/",
                array(false, false, false, "tigger", false, "/", false, "", false));
        $this->assertUrl(
                "tigger.com",
                array(false, false, false, "tigger.com", false, "/", "com", "", false));
        $this->assertUrl(
                "me.net/tigger",
                array(false, false, false, "me.net", false, "/tigger", "net", "", false));
    }

    function testAsString() {
        $this->assertPreserved('https://www.here.com');
        $this->assertPreserved('http://me:secret@www.here.com');
        $this->assertPreserved('http://here/there');
        $this->assertPreserved('http://here/there?a=A&b=B');
        $this->assertPreserved('http://here/there?a=1&a=2');
        $this->assertPreserved('http://here/there?a=1&a=2?9,8');
        $this->assertPreserved('http://host?a=1&a=2');
        $this->assertPreserved('http://host#stuff');
        $this->assertPreserved('http://me:secret@www.here.com/a/b/c/here.html?a=A?7,6');
        $this->assertPreserved('http://www.here.com/?a=A__b=B');
    }

    function assertUrl($raw, $parts, $params = false, $coords = false) {
        if (! is_array($params)) {
            $params = array();
        }
        $url = new Url($raw);
        $this->assertIdentical($url->getScheme(), $parts[0], "[$raw] scheme -> %s");
        $this->assertIdentical($url->getUsername(), $parts[1], "[$raw] username -> %s");
        $this->assertIdentical($url->getPassword(), $parts[2], "[$raw] password -> %s");
        $this->assertIdentical($url->getHost(), $parts[3], "[$raw] host -> %s");
        $this->assertIdentical($url->getPort(), $parts[4], "[$raw] port -> %s");
        $this->assertIdentical($url->getPath(), $parts[5], "[$raw] path -> %s");
        $this->assertIdentical($url->getTld(), $parts[6], "[$raw] tld -> %s");
        $this->assertIdentical($url->getEncodedRequest(), $parts[7], "[$raw] encoded -> %s");
        $this->assertIdentical($url->getFragment(), $parts[8], "[$raw] fragment -> %s");
        if ($coords) {
            $this->assertIdentical($url->getX(), $coords[0], "[$raw] x -> %s");
            $this->assertIdentical($url->getY(), $coords[1], "[$raw] y -> %s");
        }
    }

    function testUrlWithTwoSlashesInPath() {
        $url = new Url('/article/categoryedit/insert//');
        $this->assertEqual($url->getPath(), '/article/categoryedit/insert//');
    }

    function assertPreserved($string) {
        $url = new Url($string);
        $this->assertEqual($url->asString(), $string);
    }
}

class TestOfAbsoluteUrls extends UnitTestCase {

    function testMakingAbsolute() {
        $url = new Url('../there/somewhere.php');
        $this->assertEqual($url->getPath(), '../there/somewhere.php');
        $absolute = $url->makeAbsolute('https://host.com:1234/I/am/here/');
        $this->assertEqual($absolute->getScheme(), 'https');
        $this->assertEqual($absolute->getHost(), 'host.com');
        $this->assertEqual($absolute->getPort(), 1234);
        $this->assertEqual($absolute->getPath(), '/I/am/there/somewhere.php');
    }

    function testMakingAnEmptyUrlAbsolute() {
        $url = new Url('');
        $this->assertEqual($url->getPath(), '');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/page.html');
        $this->assertEqual($absolute->getScheme(), 'http');
        $this->assertEqual($absolute->getHost(), 'host.com');
        $this->assertEqual($absolute->getPath(), '/I/am/here/page.html');
    }

    function testMakingAnEmptyUrlAbsoluteWithMissingPageName() {
        $url = new Url('');
        $this->assertEqual($url->getPath(), '');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/');
        $this->assertEqual($absolute->getScheme(), 'http');
        $this->assertEqual($absolute->getHost(), 'host.com');
        $this->assertEqual($absolute->getPath(), '/I/am/here/');
    }

    function testMakingAShortQueryUrlAbsolute() {
        $url = new Url('?a#b');
        $this->assertEqual($url->getPath(), '');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/');
        $this->assertEqual($absolute->getScheme(), 'http');
        $this->assertEqual($absolute->getHost(), 'host.com');
        $this->assertEqual($absolute->getPath(), '/I/am/here/');
        $this->assertEqual($absolute->getEncodedRequest(), '?a');
        $this->assertEqual($absolute->getFragment(), 'b');
    }

    function testMakingADirectoryUrlAbsolute() {
        $url = new Url('hello/');
        $this->assertEqual($url->getPath(), 'hello/');
        $this->assertEqual($url->getBasePath(), 'hello/');
        $this->assertEqual($url->getPage(), '');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/page.html');
        $this->assertEqual($absolute->getPath(), '/I/am/here/hello/');
    }

    function testMakingARootUrlAbsolute() {
        $url = new Url('/');
        $this->assertEqual($url->getPath(), '/');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/page.html');
        $this->assertEqual($absolute->getPath(), '/');
    }

    function testMakingARootPageUrlAbsolute() {
        $url = new Url('/here.html');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/page.html');
        $this->assertEqual($absolute->getPath(), '/here.html');
    }

    function testCarryAuthenticationFromRootPage() {
        $url = new Url('here.html');
        $absolute = $url->makeAbsolute('http://test:secret@host.com/');
        $this->assertEqual($absolute->getPath(), '/here.html');
        $this->assertEqual($absolute->getUsername(), 'test');
        $this->assertEqual($absolute->getPassword(), 'secret');
    }

    function testMakingCoordinateUrlAbsolute() {
        $url = new Url('?1,2');
        $this->assertEqual($url->getPath(), '');
        $absolute = $url->makeAbsolute('http://host.com/I/am/here/');
        $this->assertEqual($absolute->getScheme(), 'http');
        $this->assertEqual($absolute->getHost(), 'host.com');
        $this->assertEqual($absolute->getPath(), '/I/am/here/');
        $this->assertEqual($absolute->getX(), 1);
        $this->assertEqual($absolute->getY(), 2);
    }

    function testMakingAbsoluteAppendedPath() {
        $url = new Url('./there/somewhere.php');
        $absolute = $url->makeAbsolute('https://host.com/here/');
        $this->assertEqual($absolute->getPath(), '/here/there/somewhere.php');
    }

    function testMakingAbsoluteBadlyFormedAppendedPath() {
        $url = new Url('there/somewhere.php');
        $absolute = $url->makeAbsolute('https://host.com/here/');
        $this->assertEqual($absolute->getPath(), '/here/there/somewhere.php');
    }

    function testMakingAbsoluteHasNoEffectWhenAlreadyAbsolute() {
        $url = new Url('https://test:secret@www.lastcraft.com:321/stuff/?a=1#f');
        $absolute = $url->makeAbsolute('http://host.com/here/');
        $this->assertEqual($absolute->getScheme(), 'https');
        $this->assertEqual($absolute->getUsername(), 'test');
        $this->assertEqual($absolute->getPassword(), 'secret');
        $this->assertEqual($absolute->getHost(), 'www.lastcraft.com');
        $this->assertEqual($absolute->getPort(), 321);
        $this->assertEqual($absolute->getPath(), '/stuff/');
        $this->assertEqual($absolute->getEncodedRequest(), '?a=1');
        $this->assertEqual($absolute->getFragment(), 'f');
    }

    function testMakingAbsoluteCarriesAuthenticationWhenAlreadyAbsolute() {
        $url = new Url('https://www.lastcraft.com');
        $absolute = $url->makeAbsolute('http://test:secret@host.com/here/');
        $this->assertEqual($absolute->getHost(), 'www.lastcraft.com');
        $this->assertEqual($absolute->getUsername(), 'test');
        $this->assertEqual($absolute->getPassword(), 'secret');
    }

    function testMakingHostOnlyAbsoluteDoesNotCarryAnyOtherInformation() {
        $url = new Url('http://www.lastcraft.com');
        $absolute = $url->makeAbsolute('https://host.com:81/here/');
        $this->assertEqual($absolute->getScheme(), 'http');
        $this->assertEqual($absolute->getHost(), 'www.lastcraft.com');
        $this->assertIdentical($absolute->getPort(), false);
        $this->assertEqual($absolute->getPath(), '/');
    }
}

class TestOfFrameUrl extends UnitTestCase {

    function testTargetAttachment() {
        $url = new Url('http://www.site.com/home.html');
        $this->assertIdentical($url->getTarget(), false);
        $url->setTarget('A frame');
        $this->assertIdentical($url->getTarget(), 'A frame');
    }
}
?>