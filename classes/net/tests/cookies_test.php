<?php
require_once(dirname(__FILE__) . '/../../external/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../cookies.php');

class TestOfCookie extends UnitTestCase {

    function testCookieDefaults() {
        $cookie = new Cookie("name");
        $this->assertFalse($cookie->getValue());
        $this->assertEqual($cookie->getPath(), "/");
        $this->assertIdentical($cookie->getHost(), false);
        $this->assertFalse($cookie->getExpiry());
        $this->assertFalse($cookie->isSecure());
    }

    function testCookieAccessors() {
        $cookie = new Cookie(
                "name",
                "value",
                "/path",
                "Mon, 18 Nov 2002 15:50:29 GMT",
                true);
        $this->assertEqual($cookie->getName(), "name");
        $this->assertEqual($cookie->getValue(), "value");
        $this->assertEqual($cookie->getPath(), "/path/");
        $this->assertEqual($cookie->getExpiry(), "Mon, 18 Nov 2002 15:50:29 GMT");
        $this->assertTrue($cookie->isSecure());
    }

    function testFullHostname() {
        $cookie = new Cookie("name");
        $this->assertTrue($cookie->setHost("host.name.here"));
        $this->assertEqual($cookie->getHost(), "host.name.here");
        $this->assertTrue($cookie->setHost("host.com"));
        $this->assertEqual($cookie->getHost(), "host.com");
    }

    function testHostTruncation() {
        $cookie = new Cookie("name");
        $cookie->setHost("this.host.name.here");
        $this->assertEqual($cookie->getHost(), "host.name.here");
        $cookie->setHost("this.host.com");
        $this->assertEqual($cookie->getHost(), "host.com");
        $this->assertTrue($cookie->setHost("dashes.in-host.com"));
        $this->assertEqual($cookie->getHost(), "in-host.com");
    }

    function testBadHosts() {
        $cookie = new Cookie("name");
        $this->assertFalse($cookie->setHost("gibberish"));
        $this->assertFalse($cookie->setHost("host.here"));
        $this->assertFalse($cookie->setHost("host..com"));
        $this->assertFalse($cookie->setHost("..."));
        $this->assertFalse($cookie->setHost("host.com."));
    }

    function testHostValidity() {
        $cookie = new Cookie("name");
        $cookie->setHost("this.host.name.here");
        $this->assertTrue($cookie->isValidHost("host.name.here"));
        $this->assertTrue($cookie->isValidHost("that.host.name.here"));
        $this->assertFalse($cookie->isValidHost("bad.host"));
        $this->assertFalse($cookie->isValidHost("nearly.name.here"));
    }

    function testPathValidity() {
        $cookie = new Cookie("name", "value", "/path");
        $this->assertFalse($cookie->isValidPath("/"));
        $this->assertTrue($cookie->isValidPath("/path/"));
        $this->assertTrue($cookie->isValidPath("/path/more"));
    }

    function testSessionExpiring() {
        $cookie = new Cookie("name", "value", "/path");
        $this->assertTrue($cookie->isExpired(0));
    }

    function testTimestampExpiry() {
        $cookie = new Cookie("name", "value", "/path", 456);
        $this->assertFalse($cookie->isExpired(0));
        $this->assertTrue($cookie->isExpired(457));
        $this->assertFalse($cookie->isExpired(455));
    }

    function testDateExpiry() {
        $cookie = new Cookie(
                "name",
                "value",
                "/path",
                "Mon, 18 Nov 2002 15:50:29 GMT");
        $this->assertTrue($cookie->isExpired("Mon, 18 Nov 2002 15:50:30 GMT"));
        $this->assertFalse($cookie->isExpired("Mon, 18 Nov 2002 15:50:28 GMT"));
    }

    function testAging() {
        $cookie = new Cookie("name", "value", "/path", 200);
        $cookie->agePrematurely(199);
        $this->assertFalse($cookie->isExpired(0));
        $cookie->agePrematurely(2);
        $this->assertTrue($cookie->isExpired(0));
    }
}

class TestOfCookieJar extends UnitTestCase {

    function testAddCookie() {
        $jar = new CookieJar();
        $jar->setCookie("a", "A");
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array('a=A'));
    }

    function testHostFilter() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A', 'my-host.com');
        $jar->setCookie('b', 'B', 'another-host.com');
        $jar->setCookie('c', 'C');
        $this->assertEqual(
                $jar->selectAsPairs(new Url('my-host.com')),
                array('a=A', 'c=C'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('another-host.com')),
                array('b=B', 'c=C'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('www.another-host.com')),
                array('b=B', 'c=C'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('new-host.org')),
                array('c=C'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('/')),
                array('a=A', 'b=B', 'c=C'));
    }

    function testPathFilter() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A', false, '/path/');
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array());
        $this->assertEqual($jar->selectAsPairs(new Url('/elsewhere')), array());
        $this->assertEqual($jar->selectAsPairs(new Url('/path/')), array('a=A'));
        $this->assertEqual($jar->selectAsPairs(new Url('/path')), array('a=A'));
        $this->assertEqual($jar->selectAsPairs(new Url('/pa')), array());
        $this->assertEqual($jar->selectAsPairs(new Url('/path/here')), array('a=A'));
    }

    function testPathFilterDeeply() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A', false, '/path/more_path/');
        $this->assertEqual($jar->selectAsPairs(new Url('/path/')), array());
        $this->assertEqual($jar->selectAsPairs(new Url('/path')), array());
        $this->assertEqual($jar->selectAsPairs(new Url('/pa')), array());
        $this->assertEqual($jar->selectAsPairs(new Url('/path/more_path/')), array('a=A'));
        $this->assertEqual($jar->selectAsPairs(new Url('/path/more_path/and_more')), array('a=A'));
        $this->assertEqual($jar->selectAsPairs(new Url('/path/not_here/')), array());
    }

    function testMultipleCookieWithDifferentPathsButSameName() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'abc', false, '/');
        $jar->setCookie('a', '123', false, '/path/here/');
        $this->assertEqual(
                $jar->selectAsPairs(new Url('/')),
                array('a=abc'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('my-host.com/')),
                array('a=abc'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('my-host.com/path/')),
                array('a=abc'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('my-host.com/path/here')),
                array('a=abc', 'a=123'));
        $this->assertEqual(
                $jar->selectAsPairs(new Url('my-host.com/path/here/there')),
                array('a=abc', 'a=123'));
    }

    function testOverwrite() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'abc', false, '/');
        $jar->setCookie('a', 'cde', false, '/');
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array('a=cde'));
    }
/*
    function testGettingMostSpecificCookieByPath() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'abc', false, '/');
        $jar->setCookie('a', 'ghi', false, '/path/next/');
        $jar->setCookie('a', 'def', false, '/path/');
        $url = new Url('/path/next');
        $this->assertEqual(
                $jar->getCookieValue($url->getHost(), $url->getPath(), 'a'),
                '1');
    }
/*
    function testGetCookiesReturnsMostSpecificCookies() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'xyz', false, '/');
        $jar->setCookie('b', 'abc', false, '/');
        $jar->setCookie('b', '123', false, '/path/');
        $this->assertEqual($jar->getCookies(new Url('/path')), array('a' => 'xyz', 'b' => '123'));
    }
*/

    function testClearSessionCookies() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A', false, '/');
        $jar->restartSession();
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array());
    }

    function testExpiryFilterByDate() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A', false, '/', 'Wed, 25-Dec-02 04:24:20 GMT');
        $jar->restartSession("Wed, 25-Dec-02 04:24:19 GMT");
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array('a=A'));
        $jar->restartSession("Wed, 25-Dec-02 04:24:21 GMT");
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array());
    }

    function testExpiryFilterByAgeing() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'A', false, '/', 'Wed, 25-Dec-02 04:24:20 GMT');
        $jar->restartSession("Wed, 25-Dec-02 04:24:19 GMT");
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array('a=A'));
        $jar->agePrematurely(2);
        $jar->restartSession("Wed, 25-Dec-02 04:24:19 GMT");
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array());
    }

    function testCookieClearing() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'abc', false, '/');
        $jar->setCookie('a', '', false, '/');
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array('a='));
    }

    function testCookieClearByLoweringDate() {
        $jar = new CookieJar();
        $jar->setCookie('a', 'abc', false, '/', 'Wed, 25-Dec-02 04:24:21 GMT');
        $jar->setCookie('a', 'def', false, '/', 'Wed, 25-Dec-02 04:24:19 GMT');
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array('a=def'));
        $jar->restartSession('Wed, 25-Dec-02 04:24:20 GMT');
        $this->assertEqual($jar->selectAsPairs(new Url('/')), array());
    }
}
?>