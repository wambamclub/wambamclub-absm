<?php
require_once(dirname(__FILE__) . '/../../external/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../encoding.php');
require_once(dirname(__FILE__) . '/../http.php');
require_once(dirname(__FILE__) . '/../socket.php');
require_once(dirname(__FILE__) . '/../cookies.php');

Mock::generate('Socket');
Mock::generate('CookieJar');
Mock::generate('Route');
Mock::generatePartial('Route', 'PartialRoute', array('_createSocket'));
Mock::generatePartial(
        'ProxyRoute',
        'PartialProxyRoute',
        array('_createSocket'));

class TestOfDirectRoute extends UnitTestCase {

    function testDefaultGetRequest() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET /here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: a.valid.host\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->Route(new Url('http://a.valid.host/here.html'));

        $this->assertReference($route->createConnection('GET', 15), $socket);
    }

    function testDefaultPostRequest() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("POST /here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: a.valid.host\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->Route(new Url('http://a.valid.host/here.html'));

        $route->createConnection('POST', 15);
    }

    function testGetWithPort() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET /here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: a.valid.host:81\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->Route(new Url('http://a.valid.host:81/here.html'));

        $route->createConnection('GET', 15);
    }

    function testGetWithParameters() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET /here.html?a=1&b=2 HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: a.valid.host\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->Route(new Url('http://a.valid.host/here.html?a=1&b=2'));

        $route->createConnection('GET', 15);
    }
}

class TestOfProxyRoute extends UnitTestCase {

    function testDefaultGet() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET http://a.valid.host/here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: my-proxy:8080\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialProxyRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->ProxyRoute(
                new Url('http://a.valid.host/here.html'),
                new Url('http://my-proxy'));
        $route->createConnection('GET', 15);
    }

    function testDefaultPost() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("POST http://a.valid.host/here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: my-proxy:8080\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialProxyRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->ProxyRoute(
                new Url('http://a.valid.host/here.html'),
                new Url('http://my-proxy'));
        $route->createConnection('POST', 15);
    }

    function testGetWithPort() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET http://a.valid.host:81/here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: my-proxy:8081\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialProxyRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->ProxyRoute(
                new Url('http://a.valid.host:81/here.html'),
                new Url('http://my-proxy:8081'));
        $route->createConnection('GET', 15);
    }

    function testGetWithParameters() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET http://a.valid.host/here.html?a=1&b=2 HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: my-proxy:8080\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 3);

        $route = &new PartialProxyRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->ProxyRoute(
                new Url('http://a.valid.host/here.html?a=1&b=2'),
                new Url('http://my-proxy'));
        $route->createConnection('GET', 15);
    }

    function testGetWithAuthentication() {
        $encoded = base64_encode('Me:Secret');

        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("GET http://a.valid.host/here.html HTTP/1.0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Host: my-proxy:8080\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("Proxy-Authorization: Basic $encoded\r\n"));
        $socket->expectArgumentsAt(3, 'write', array("Connection: close\r\n"));
        $socket->expectCallCount('write', 4);

        $route = &new PartialProxyRoute();
        $route->setReturnReference('_createSocket', $socket);
        $route->ProxyRoute(
                new Url('http://a.valid.host/here.html'),
                new Url('http://my-proxy'),
                'Me',
                'Secret');
        $route->createConnection('GET', 15);
    }
}

class TestOfHttpRequest extends UnitTestCase {

    function testReadingBadConnection() {
        $socket = &new MockSocket();

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);

        $request = &new HttpRequest($route, new GetEncoding());
        $reponse = &$request->fetch(15);
        $this->assertTrue($reponse->isError());
    }

    function testReadingGoodConnection() {
        $socket = &new MockSocket();
        $socket->expectOnce('write', array("\r\n"));

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);
        $route->expectArguments('createConnection', array('GET', 15));

        $request = &new HttpRequest($route, new GetEncoding());
        $this->assertIsA($request->fetch(15), 'HttpResponse');
    }

    function testWritingAdditionalHeaders() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("My: stuff\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("\r\n"));
        $socket->expectCallCount('write', 2);

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);

        $request = &new HttpRequest($route, new GetEncoding());
        $request->setHeader('My', 'stuff');
        $request->fetch(15);
    }

    function testCookieWriting() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("Cookie: a=A\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("\r\n"));
        $socket->expectCallCount('write', 2);

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);

        $jar = new CookieJar();
        $jar->setCookie('a', 'A');

        $request = &new HttpRequest($route, new GetEncoding());
        $request->readCookiesFromJar($jar, new Url('/'));
        $this->assertIsA($request->fetch(15), 'HttpResponse');
    }

    function testMultipleCookieWriting() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("Cookie: a=A;b=B\r\n"));

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);

        $jar = new CookieJar();
        $jar->setCookie('a', 'A');
        $jar->setCookie('b', 'B');

        $request = &new HttpRequest($route, new GetEncoding());
        $request->readCookiesFromJar($jar, new Url('/'));
        $request->fetch(15);
    }
}

class TestOfHttpPostRequest extends UnitTestCase {

    function testReadingBadConnectionCausesErrorBecauseOfDeadSocket() {
        $socket = &new MockSocket();

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);

        $request = &new HttpRequest($route, new PostEncoding());
        $reponse = &$request->fetch(15);
        $this->assertTrue($reponse->isError());
    }

    function testReadingGoodConnection() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("Content-Length: 0\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Content-Type: application/x-www-form-urlencoded\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("\r\n"));
        $socket->expectArgumentsAt(3, 'write', array(""));

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);
        $route->expectArguments('createConnection', array('POST', 15));

        $request = &new HttpRequest($route, new PostEncoding());
        $this->assertIsA($request->fetch(15), 'HttpResponse');
    }

    function testContentHeadersCalculated() {
        $socket = &new MockSocket();
        $socket->expectArgumentsAt(0, 'write', array("Content-Length: 3\r\n"));
        $socket->expectArgumentsAt(1, 'write', array("Content-Type: application/x-www-form-urlencoded\r\n"));
        $socket->expectArgumentsAt(2, 'write', array("\r\n"));
        $socket->expectArgumentsAt(3, 'write', array("a=A"));

        $route = &new MockRoute();
        $route->setReturnReference('createConnection', $socket);
        $route->expectArguments('createConnection', array('POST', 15));

        $request = &new HttpRequest(
                $route,
                new PostEncoding(array('a' => 'A')));
        $this->assertIsA($request->fetch(15), 'HttpResponse');
    }
}

class TestOfHttpHeaders extends UnitTestCase {

    function testParseBasicHeaders() {
        $headers = new HttpHeaders(
                "HTTP/1.1 200 OK\r\n" .
                "Date: Mon, 18 Nov 2002 15:50:29 GMT\r\n" .
                "Content-Type: text/plain\r\n" .
                "Server: Apache/1.3.24 (Win32) PHP/4.2.3\r\n" .
                "Connection: close");
        $this->assertIdentical($headers->getHttpVersion(), "1.1");
        $this->assertIdentical($headers->getResponseCode(), 200);
        $this->assertEqual($headers->getMimeType(), "text/plain");
    }

    function testNonStandardResponseHeader() {
        $headers = new HttpHeaders(
                "HTTP/1.1 302 (HTTP-Version SP Status-Code CRLF)\r\n" .
                "Connection: close");
        $this->assertIdentical($headers->getResponseCode(), 302);
    }

    function testCanParseMultipleCookies() {
        $jar = &new MockCookieJar();
        $jar->expectAt(0, 'setCookie', array('a', 'aaa', 'host', '/here/', 'Wed, 25 Dec 2002 04:24:20 GMT'));
        $jar->expectAt(1, 'setCookie', array('b', 'bbb', 'host', '/', false));

        $headers = new HttpHeaders(
                "HTTP/1.1 200 OK\r\n" .
                "Date: Mon, 18 Nov 2002 15:50:29 GMT\r\n" .
                "Content-Type: text/plain\r\n" .
                "Server: Apache/1.3.24 (Win32) PHP/4.2.3\r\n" .
                "Set-Cookie: a=aaa; expires=Wed, 25-Dec-02 04:24:20 GMT; path=/here/\r\n" .
                "Set-Cookie: b=bbb\r\n" .
                "Connection: close");
        $headers->writeCookiesToJar($jar, new Url('http://host'));
    }

    function testCanRecogniseRedirect() {
        $headers = new HttpHeaders("HTTP/1.1 301 OK\r\n" .
                "Content-Type: text/plain\r\n" .
                "Content-Length: 0\r\n" .
                "Location: http://www.somewhere-else.com/\r\n" .
                "Connection: close");
        $this->assertIdentical($headers->getResponseCode(), 301);
        $this->assertEqual($headers->getLocation(), "http://www.somewhere-else.com/");
        $this->assertTrue($headers->isRedirect());
    }

    function testCanParseChallenge() {
        $headers = new HttpHeaders("HTTP/1.1 401 Authorization required\r\n" .
                "Content-Type: text/plain\r\n" .
                "Connection: close\r\n" .
                "WWW-Authenticate: Basic realm=\"Somewhere\"");
        $this->assertEqual($headers->getAuthentication(), 'Basic');
        $this->assertEqual($headers->getRealm(), 'Somewhere');
        $this->assertTrue($headers->isChallenge());
    }
}

class TestOfHttpResponse extends UnitTestCase {

    function testBadRequest() {
        $socket = &new MockSocket();
        $socket->setReturnValue('getSent', '');

        $response = &new HttpResponse($socket, new Url('here'), new GetEncoding());
        $this->assertTrue($response->isError());
        $this->assertPattern('/Nothing fetched/', $response->getError());
        $this->assertIdentical($response->getContent(), false);
        $this->assertIdentical($response->getSent(), '');
    }

    function testBadSocketDuringResponse() {
        $socket = &new MockSocket();
        $socket->setReturnValueAt(0, "read", "HTTP/1.1 200 OK\r\n");
        $socket->setReturnValueAt(1, "read", "Date: Mon, 18 Nov 2002 15:50:29 GMT\r\n");
        $socket->setReturnValue("read", "");
        $socket->setReturnValue('getSent', 'HTTP/1.1 ...');

        $response = &new HttpResponse($socket, new Url('here'), new GetEncoding());
        $this->assertTrue($response->isError());
        $this->assertEqual($response->getContent(), '');
        $this->assertEqual($response->getSent(), 'HTTP/1.1 ...');
    }

    function testIncompleteHeader() {
        $socket = &new MockSocket();
        $socket->setReturnValueAt(0, "read", "HTTP/1.1 200 OK\r\n");
        $socket->setReturnValueAt(1, "read", "Date: Mon, 18 Nov 2002 15:50:29 GMT\r\n");
        $socket->setReturnValueAt(2, "read", "Content-Type: text/plain\r\n");
        $socket->setReturnValue("read", "");

        $response = &new HttpResponse($socket, new Url('here'), new GetEncoding());
        $this->assertTrue($response->isError());
        $this->assertEqual($response->getContent(), "");
    }

    function testParseOfResponseHeadersWhenChunked() {
        $socket = &new MockSocket();
        $socket->setReturnValueAt(0, "read", "HTTP/1.1 200 OK\r\nDate: Mon, 18 Nov 2002 15:50:29 GMT\r\n");
        $socket->setReturnValueAt(1, "read", "Content-Type: text/plain\r\n");
        $socket->setReturnValueAt(2, "read", "Server: Apache/1.3.24 (Win32) PHP/4.2.3\r\nConne");
        $socket->setReturnValueAt(3, "read", "ction: close\r\n\r\nthis is a test file\n");
        $socket->setReturnValueAt(4, "read", "with two lines in it\n");
        $socket->setReturnValue("read", "");

        $response = &new HttpResponse($socket, new Url('here'), new GetEncoding());
        $this->assertFalse($response->isError());
        $this->assertEqual(
                $response->getContent(),
                "this is a test file\nwith two lines in it\n");
        $headers = $response->getHeaders();
        $this->assertIdentical($headers->getHttpVersion(), "1.1");
        $this->assertIdentical($headers->getResponseCode(), 200);
        $this->assertEqual($headers->getMimeType(), "text/plain");
        $this->assertFalse($headers->isRedirect());
        $this->assertFalse($headers->getLocation());
    }

    function testRedirect() {
        $socket = &new MockSocket();
        $socket->setReturnValueAt(0, "read", "HTTP/1.1 301 OK\r\n");
        $socket->setReturnValueAt(1, "read", "Content-Type: text/plain\r\n");
        $socket->setReturnValueAt(2, "read", "Location: http://www.somewhere-else.com/\r\n");
        $socket->setReturnValueAt(3, "read", "Connection: close\r\n");
        $socket->setReturnValueAt(4, "read", "\r\n");
        $socket->setReturnValue("read", "");

        $response = &new HttpResponse($socket, new Url('here'), new GetEncoding());
        $headers = $response->getHeaders();
        $this->assertTrue($headers->isRedirect());
        $this->assertEqual($headers->getLocation(), "http://www.somewhere-else.com/");
    }

    function testRedirectWithPort() {
        $socket = &new MockSocket();
        $socket->setReturnValueAt(0, "read", "HTTP/1.1 301 OK\r\n");
        $socket->setReturnValueAt(1, "read", "Content-Type: text/plain\r\n");
        $socket->setReturnValueAt(2, "read", "Location: http://www.somewhere-else.com:80/\r\n");
        $socket->setReturnValueAt(3, "read", "Connection: close\r\n");
        $socket->setReturnValueAt(4, "read", "\r\n");
        $socket->setReturnValue("read", "");

        $response = &new HttpResponse($socket, new Url('here'), new GetEncoding());
        $headers = $response->getHeaders();
        $this->assertTrue($headers->isRedirect());
        $this->assertEqual($headers->getLocation(), "http://www.somewhere-else.com:80/");
    }
}
?>