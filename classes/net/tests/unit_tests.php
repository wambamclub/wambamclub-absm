<?php
require_once(dirname(__FILE__) . '/../../external/test_adapters.php');

class NetworkTests extends TestSuite {
    function __construct() {
        parent::__construct('Networking tests');
        $path = dirname(__FILE__);
        $this->addFile("$path/authentication_test.php");
        $this->addFile("$path/cookies_test.php");
        $this->addFile("$path/encoding_test.php");
        $this->addFile("$path/http_test.php");
        $this->addFile("$path/socket_test.php");
        $this->addFile("$path/url_test.php");
        $this->addFile("$path/user_agent_test.php");
    }
}

if (! defined('RUNNER')) {
    define('RUNNER', 'Net tests');
    $test = new NetworkTests();
    $test->run(new DualReporter());
}
?>