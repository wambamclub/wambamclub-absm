<?php
require_once(dirname(__FILE__) . '/../../external/simpletest/autorun.php');
require_once(dirname(__FILE__) . '/../authentication.php');
require_once(dirname(__FILE__) . '/../http.php');
Mock::generate('HttpRequest');

class TestOfRealm extends UnitTestCase {

    function testWithinSameUrl() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/hello.html'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/hello.html')));
    }

    function testInsideWithLongerUrl() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/hello.html')));
    }

    function testBelowRootIsOutside() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/more/hello.html')));
    }

    function testOldNetscapeDefinitionIsOutside() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/'));
        $this->assertFalse($realm->isWithin(
                new Url('http://www.here.com/pathmore/hello.html')));
    }

    function testInsideWithMissingTrailingSlash() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path')));
    }

    function testDifferentPageNameStillInside() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/hello.html'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/goodbye.html')));
    }

    function testNewUrlInSameDirectoryDoesNotChangeRealm() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/hello.html'));
        $realm->stretch(new Url('http://www.here.com/path/goodbye.html'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/index.html')));
        $this->assertFalse($realm->isWithin(
                new Url('http://www.here.com/index.html')));
    }

    function testNewUrlMakesRealmTheCommonPath() {
        $realm = &new Realm(
                'Basic',
                new Url('http://www.here.com/path/here/hello.html'));
        $realm->stretch(new Url('http://www.here.com/path/there/goodbye.html'));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/here/index.html')));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/there/index.html')));
        $this->assertTrue($realm->isWithin(
                new Url('http://www.here.com/path/index.html')));
        $this->assertFalse($realm->isWithin(
                new Url('http://www.here.com/index.html')));
        $this->assertFalse($realm->isWithin(
                new Url('http://www.here.com/paths/index.html')));
        $this->assertFalse($realm->isWithin(
                new Url('http://www.here.com/pathindex.html')));
    }
}

class TestOfAuthenticator extends UnitTestCase {

    function testNoRealms() {
        $request = &new MockHttpRequest();
        $request->expectNever('setHeader');
        $authenticator = &new Authenticator();
        $authenticator->setHeader($request, new Url('http://here.com/'));
    }

    function &createSingleRealm() {
        $authenticator = &new Authenticator();
        $authenticator->addRealm(
                new Url('http://www.here.com/path/hello.html'),
                'Basic',
                'Sanctuary');
        $authenticator->setIdentityForRealm('www.here.com', 'Sanctuary', 'test', 'secret');
        return $authenticator;
    }

    function testOutsideRealm() {
        $request = &new MockHttpRequest();
        $request->expectNever('setHeader');
        $authenticator = &$this->createSingleRealm();
        $authenticator->setHeader(
                $request,
                new Url('http://www.here.com/hello.html'));
    }

    function testWithinRealm() {
        $request = &new MockHttpRequest();
        $request->expectOnce('setHeader',
                             array('Authorization',
                                   'Basic dGVzdDpzZWNyZXQ='));
        $authenticator = &$this->createSingleRealm();
        $authenticator->setHeader(
                $request,
                new Url('http://www.here.com/path/more/hello.html'));
    }

    function testRestartingClearsRealm() {
        $request = &new MockHttpRequest();
        $request->expectNever('setHeader');
        $authenticator = &$this->createSingleRealm();
        $authenticator->restartSession();
        $authenticator->setHeader(
                $request,
                new Url('http://www.here.com/hello.html'));
    }

    function testDifferentHostIsOutsideRealm() {
        $request = &new MockHttpRequest();
        $request->expectNever('setHeader');
        $authenticator = &$this->createSingleRealm();
        $authenticator->setHeader(
                $request,
                new Url('http://here.com/path/hello.html'));
    }
}
?>