<?php
require_once(dirname(__FILE__) . '/cookies.php');
require_once(dirname(__FILE__) . '/http.php');
require_once(dirname(__FILE__) . '/encoding.php');
require_once(dirname(__FILE__) . '/authentication.php');

if (! defined('DEFAULT_MAX_REDIRECTS')) {
    define('DEFAULT_MAX_REDIRECTS', 3);
}

if (! defined('DEFAULT_CONNECTION_TIMEOUT')) {
    define('DEFAULT_CONNECTION_TIMEOUT', 15);
}

class UserAgent {
    var $_cookie_jar;
    var $_cookies_enabled = true;
    var $_authenticator;
    var $_max_redirects = DEFAULT_MAX_REDIRECTS;
    var $_proxy = false;
    var $_proxy_username = false;
    var $_proxy_password = false;
    var $_connection_timeout = DEFAULT_CONNECTION_TIMEOUT;
    var $_additional_headers = array();

    function UserAgent() {
        $this->_cookie_jar = &new CookieJar();
        $this->_authenticator = &new Authenticator();
    }

    function restart($date = false) {
        $this->_cookie_jar->restartSession($date);
        $this->_authenticator->restartSession();
    }

    function setHeader($header, $value) {
        $this->_additional_headers[$header] = $value;
    }

    function ageCookies($interval) {
        $this->_cookie_jar->agePrematurely($interval);
    }

    function setCookie($name, $value, $host = false, $path = '/', $expiry = false) {
        $this->_cookie_jar->setCookie($name, $value, $host, $path, $expiry);
    }

    function getCookieValue($host, $path, $name) {
        return $this->_cookie_jar->getCookieValue($host, $path, $name);
    }

    function getCookies($host, $path) {
        return $this->_cookie_jar->getCookies(new Url($host . $path));
    }

    function getBaseCookieValue($name, $base) {
        if (! $base) {
            return null;
        }
        return $this->getCookieValue($base->getHost(), $base->getPath(), $name);
    }

    function ignoreCookies() {
        $this->_cookies_enabled = false;
    }

    function useCookies() {
        $this->_cookies_enabled = true;
    }

    function setConnectionTimeout($timeout) {
        $this->_connection_timeout = $timeout;
    }

    function setMaximumRedirects($max) {
        $this->_max_redirects = $max;
    }

    function useProxy($proxy, $username, $password) {
        if (! $proxy) {
            $this->_proxy = false;
            return;
        }
        if ((strncmp($proxy, 'http://', 7) != 0) && (strncmp($proxy, 'https://', 8) != 0)) {
            $proxy = 'http://'. $proxy;
        }
        $this->_proxy = &new Url($proxy);
        $this->_proxy_username = $username;
        $this->_proxy_password = $password;
    }

    function _isTooManyRedirects($redirects) {
        return ($redirects > $this->_max_redirects);
    }

    function setIdentity($host, $realm, $username, $password) {
        $this->_authenticator->setIdentityForRealm($host, $realm, $username, $password);
    }

    function &fetchResponse($url, $encoding) {
        if ($encoding->getMethod() != 'POST') {
            $url->addRequestParameters($encoding);
            $encoding->clear();
        }
        $response = &$this->_fetchWhileRedirected($url, $encoding);
        if ($headers = $response->getHeaders()) {
            if ($headers->isChallenge()) {
                $this->_authenticator->addRealm(
                        $url,
                        $headers->getAuthentication(),
                        $headers->getRealm());
            }
        }
        return $response;
    }

    function &_fetchWhileRedirected($url, $encoding) {
        $redirects = 0;
        do {
            $response = &$this->_fetch($url, $encoding);
            if ($response->isError()) {
                return $response;
            }
            $headers = $response->getHeaders();
            $location = new Url($headers->getLocation());
            $url = $location->makeAbsolute($url);
            if ($this->_cookies_enabled) {
                $headers->writeCookiesToJar($this->_cookie_jar, $url);
            }
            if (! $headers->isRedirect()) {
                break;
            }
            $encoding = new GetEncoding();
        } while (! $this->_isTooManyRedirects(++$redirects));
        return $response;
    }

    function &_fetch($url, $encoding) {
        $request = &$this->_createRequest($url, $encoding);
        $response = &$request->fetch($this->_connection_timeout);
        return $response;
    }

    function &_createRequest($url, $encoding) {
        $request = &$this->_createHttpRequest($url, $encoding);
        $this->_setAdditionalHeaders($request);
        if ($this->_cookies_enabled) {
            $request->readCookiesFromJar($this->_cookie_jar, $url);
        }
        $this->_authenticator->setHeader($request, $url);
        return $request;
    }

    function _setAdditionalHeaders(&$request) {
        foreach ($this->_additional_headers as $header => $value) {
            $request->setHeader($header, $value);
        }
    }

    function &_createHttpRequest($url, $encoding) {
        $request = &new HttpRequest($this->_createRoute($url), $encoding);
        return $request;
    }

    function &_createRoute($url) {
        if ($this->_proxy) {
            $route = &new ProxyRoute(
                    $url,
                    $this->_proxy,
                    $this->_proxy_username,
                    $this->_proxy_password);
        } else {
            $route = &new Route($url);
        }
        return $route;
    }
}
?>