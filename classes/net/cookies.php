<?php
require_once(dirname(__FILE__) . '/url.php');

class Cookie {
	var $_host;
	var $_name;
	var $_value;
	var $_path;
	var $_expiry;
	var $_is_secure;

	function Cookie($name, $value = false, $path = false, $expiry = false, $is_secure = false) {
		$this->_host = false;
		$this->_name = $name;
		$this->_value = $value;
		$this->_path = ($path ? $this->_fixPath($path) : "/");
		$this->_expiry = false;
		if (is_string($expiry)) {
			$this->_expiry = strtotime($expiry);
		} elseif (is_integer($expiry)) {
			$this->_expiry = $expiry;
		}
		$this->_is_secure = $is_secure;
	}

	function setHost($host) {
		if ($host = $this->_truncateHost($host)) {
			$this->_host = $host;
			return true;
		}
		return false;
	}

	function getHost() {
		return $this->_host;
	}

	function isValidHost($host) {
		return ($this->_truncateHost($host) === $this->getHost());
	}

	function _truncateHost($host) {
		$tlds = Url::getAllTopLevelDomains();
		if (preg_match('/[a-z\-]+\.(' . $tlds . ')$/i', $host, $matches)) {
			return $matches[0];
		} elseif (preg_match('/[a-z\-]+\.[a-z\-]+\.[a-z\-]+$/i', $host, $matches)) {
			return $matches[0];
		}
		return false;
	}

	function getName() {
		return $this->_name;
	}

	function getValue() {
		return $this->_value;
	}

	function getPath() {
		return $this->_path;
	}

	function isValidPath($path) {
		return (strncmp(
				$this->_fixPath($path),
				$this->getPath(),
				strlen($this->getPath())) == 0);
	}

	function getExpiry() {
		if (! $this->_expiry) {
			return false;
		}
		return gmdate("D, d M Y H:i:s", $this->_expiry) . " GMT";
	}

	function isExpired($now) {
		if (! $this->_expiry) {
			return true;
		}
		if (is_string($now)) {
			$now = strtotime($now);
		}
		return ($this->_expiry < $now);
	}

	function agePrematurely($interval) {
		if ($this->_expiry) {
			$this->_expiry -= $interval;
		}
	}

	function isSecure() {
		return $this->_is_secure;
	}

	function _fixPath($path) {
		if (substr($path, 0, 1) != '/') {
			$path = '/' . $path;
		}
		if (substr($path, -1, 1) != '/') {
			$path .= '/';
		}
		return $path;
	}
}

class CookieJar {
	var $_cookies;

	function CookieJar() {
		$this->_cookies = array();
	}

	function restartSession($date = false) {
		$surviving_cookies = array();
		for ($i = 0; $i < count($this->_cookies); $i++) {
			if (! $this->_cookies[$i]->getValue()) {
				continue;
			}
			if (! $this->_cookies[$i]->getExpiry()) {
				continue;
			}
			if ($date && $this->_cookies[$i]->isExpired($date)) {
				continue;
			}
			$surviving_cookies[] = $this->_cookies[$i];
		}
		$this->_cookies = $surviving_cookies;
	}

	function agePrematurely($interval) {
		for ($i = 0; $i < count($this->_cookies); $i++) {
			$this->_cookies[$i]->agePrematurely($interval);
		}
	}

	function setCookie($name, $value, $host = false, $path = '/', $expiry = false) {
		$cookie = new Cookie($name, $value, $path, $expiry);
		if ($host) {
			$cookie->setHost($host);
		}
		$this->_cookies[$this->_findFirstMatch($cookie)] = $cookie;
	}

	function _findFirstMatch($cookie) {
		for ($i = 0; $i < count($this->_cookies); $i++) {
			$is_match = $this->_isMatch(
					$cookie,
					$this->_cookies[$i]->getHost(),
					$this->_cookies[$i]->getPath(),
					$this->_cookies[$i]->getName());
			if ($is_match) {
				return $i;
			}
		}
		return count($this->_cookies);
	}

	function getCookieValue($host, $path, $name) {
        $longest_path = '';
		foreach ($this->_cookies as $cookie) {
			if ($this->_isMatch($cookie, $host, $path, $name)) {
				if (strlen($cookie->getPath()) > strlen($longest_path)) {
					$value = $cookie->getValue();
					$longest_path = $cookie->getPath();
				}
			}
		}
		return (isset($value) ? $value : false);
	}

	function _isMatch($cookie, $host, $path, $name) {
		if ($cookie->getName() != $name) {
			return false;
		}
		if ($host && $cookie->getHost() && ! $cookie->isValidHost($host)) {
			return false;
		}
		if (! $cookie->isValidPath($path)) {
			return false;
		}
		return true;
	}

	function selectAsPairs($url) {
		$pairs = array();
		foreach ($this->_cookies as $cookie) {
			if ($this->_isMatch($cookie, $url->getHost(), $url->getPath(), $cookie->getName())) {
				$pairs[] = $cookie->getName() . '=' . $cookie->getValue();
			}
		}
		return $pairs;
	}

    function getCookies($url) {
        $cookies = array();
        foreach ($this->_cookies as $cookie) {
            $name = $cookie->getName();
            $value = $this->getCookieValue($url->getHost(), $url->getPath(), $name);
            if ($value) {
                $cookies[$name] = $value;
            }
        }
        return $cookies;
    }
}
?>