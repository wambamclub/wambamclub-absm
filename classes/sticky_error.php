<?php
class StickyError {
    var $_error = "Error not initialised";

    function __construct() {
        $this->_error = "";
    }

    function getError() {
        return $this->_error;
    }

    function isError() {
        return ($this->_error !== '');
    }

    function _setError($error) {
        $this->_error = $error;
    }

    function _clearError() {
        $this->_error = "";
    }
}
?>