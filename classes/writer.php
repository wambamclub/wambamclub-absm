<?php
class StdoutWriter  {
    function write($output) {
        print $output;
    }
}

class FileWriter  {
    var $_filename;
    var $_open_flags;
    var $_stream;

    function FileWriter($filename) {
        $this->_filename = $filename;
        $this->_open_flags = 'a';
        $this->_stream = false;
    }

    function write($output) {
        $this->_ensureOpen();
        if (! $this->_stream) {
            return;
        }
        fwrite($this->_stream, $output);
        fflush($this->_stream);
    }

    function close() {
        @fclose($this->_stream);
    }

    function _ensureOpen() {
        if (! $this->_stream) {
            if (! file_exists($this->_filename)) {
                touch($this->_filename);
                chmod($this->_filename, 0664);
            }
            $this->_stream = fopen($this->_filename, $this->_open_flags);
        }
    }
}

class StringWriter  {
    var $_contents = '';

    function write($output) {
        $this->_contents .= $output;
    }

    function getValue() {
        return $this->_contents;
    }
}
?>