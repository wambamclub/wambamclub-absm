<?php
require_once(dirname(__FILE__) . '/mysql_transaction_class.php');
require_once(dirname(__FILE__) . '/net/user_agent.php');

class AweberQueuer {
    var $environment;
    var $list;
    
    public function __construct($environment, $list) {
        $this->environment = $environment;
        $this->list = $list;
    }

    public function add($name, $email) {
        if ($this->exists($email)) {
            return false;
        }
        
        $connection = $this->createMysqlConnection($this->environment);
        $connection->execute(
                "INSERT INTO queue (id, name, email, list, last) VALUES " .
                        "('', '" .
                        $connection->escape($name) . "', '" .
                        $connection->escape($email) . "', '" .
                        $connection->escape($this->list) . "', " .
                        "NOW())");
        $connection->commit();
    }
    
    public function count($email = null) {
        $connection = $this->createMysqlConnection($this->environment);
        $iterator = $connection->select(
                "SELECT count(*) AS count FROM queue " .
                    (($email) ? 
                        "WHERE email = '" . $connection->escape($email) . "' " . 
                        "AND LIST = '" . $connection->escape($this->list) . "'" 
                    : "")
                );
        $row = $iterator->next();
        return $row['count'];
    }
    
    public function exists($email) {
        return (boolean)($this->count($email) >= 1) ? true : false;
    }
    
    public function next() {
        $connection = $this->createMysqlConnection($this->environment);
        $iterator = $connection->select(
                "SELECT name, email, list FROM queue " .
                    "ORDER BY last " .
                    "ASC " .
                    "LIMIT 1");
        $row = $iterator->next();
        if (! $row) {
            return;
        }
        $this->remove($row['email'], $row['list']);
        return array(
                'name' => $row['name'],
                'email' => $row['email'],
                'list' => $row['list']);
    }
    
    public function remove($email, $list) {
        $connection = $this->createMysqlConnection($this->environment);
        $connection->execute($sql =
                "DELETE FROM queue WHERE email = '" . $connection->escape($email) . "' " .
                 "AND LIST = '" . $connection->escape($list) . "'");
        $connection->commit();
    }
    
    private function createMysqlConnection($configuration) {
        return new MysqlTransaction(
                $configuration->getValue('mysql_host'),
                $configuration->getValue('mysql_username'),
                $configuration->getValue('mysql_password'),
                $configuration->getValue('mysql_database'));
    }
}

class AweberPoster {
    private $url;
    
    public function __construct($url = 'http://www.aweber.com/scripts/addlead.pl') {
        $this->url = $url;
    }
    
    public function post($name, $email, $list, $referer = null, $redirect = null) {
        $url = new Url($this->url);
        $url->addRequestParameter('listname', $list);
        $url->addRequestParameter('redirect', $redirect);
        $url->addRequestParameter('meta_message', 1);
        $url->addRequestParameter('name', $name);
        $url->addRequestParameter('from', $email);
        
        $agent = new UserAgent();
        $agent->setHeader('Referer', $referer);
        return $agent->fetchResponse(
            $url,
            new GetEncoding());
    }
}
?>
