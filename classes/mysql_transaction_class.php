<?php
require_once(dirname(__FILE__) . '/../classes/sticky_error.php');


class MysqlTransaction {
    var $_connection = false;
    var $_error_raised = false;
    var $_database;

    function MysqlTransaction($host, $username, $password, $database) {
        $this->_database = $database;
        if (! ($this->_connection = mysql_connect($host, $username, $password, true))) {
            trigger_error(mysql_error(), E_USER_WARNING);
            $this->_error_raised = true;
            return;
        }
        if (! mysql_select_db($database, $this->_connection)) {
            $this->_abandon();
            return;
        }
        $this->execute('set autocommit = 0');
        $this->execute('set transaction isolation level repeatable read');
        $this->execute('begin');
    }

    function commit() {
        if ($this->_error_raised) {
            return false;
        }
        $result = $this->execute('commit');
        $this->_close();
        return $result !== false;
    }

    function rollback() {
        @mysql_query('rollback', $this->_connection);
        $this->_close();
    }

    function _close() {
        @mysql_close($this->_connection);
        $this->_connection = false;
    }

    function select($sql) {
        $result = $this->execute($sql);
        return $result ? new MysqlRecordSet($result) : false;
    }

    function selectRow($sql) {
        $result = $this->select($sql);
        if (! $result) {
            return false;
        }
        return $result->next();
    }

    function execute($sql) {
        if (! $this->_connection && $this->_error_raised) {
            return false;
        }
        $result = @mysql_query($sql, $this->_connection);
        if (mysql_error($this->_connection)) {
            $this->_abandon();
            return false;
        }
        return $result;
    }

    function getLastAutoId() {
        $id = @mysql_insert_id($this->_connection);
        return $id > 0 ? $id : false;
    }

    function getDatabase() {
        return $this->_database;
    }

    function _abandon() {
        if (! $this->_error_raised ) {
            throw new Exception(mysql_error($this->_connection));
            $this->_error_raised = true;
        }
        $this->rollback();
    }

    function escape($text) {
        return mysql_real_escape_string($text, $this->_connection);
    }
    
    function isError() {
        return (boolean)($this->_error_raised);
    }
}

class MysqlRecordSet {
    var $_result;
    var $_next;
    var $_error;

    function MysqlRecordSet($result) {
        $this->_result = $result;
        $this->_error = new StickyError();
    }

    function next() {
        $this->_next = mysql_fetch_assoc($this->_result);
        return $this->_next;
    }

    function count() {
        return mysql_num_rows($this->_result);
    }
    
    function isEnd() {
        return (boolean)($this->_next === false);
    }
    
    function isError() {
        return $this->_error->isError();
    }
    
    function getError() {
        return $this->_error->getError();
    }
}
?>